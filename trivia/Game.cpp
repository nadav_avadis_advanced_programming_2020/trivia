#include "Game.h"

mutex m_question_mutex;
mutex m_Ansers_mutex;
mutex m_players_mutex;

Game::Game(int timeForQ , int id , vector<Question> questions, map<LoggedUser, GameData> players) : m_question(questions),m_players(players)
{
	this->timeForQ = timeForQ;
	this->id = id;
	this->currentQ = 0;
	this->gameFinish = false;
	this->m_Ansers.resize(questions.size());
	for (int i = 0; i < m_Ansers.size(); i++)
	{
		m_Ansers[i] = 0;
	}
}

Game::~Game()
{

}

bool Game::isEveibadyEnserd(LoggedUser user)
{
	lock_guard<mutex> G(m_Ansers_mutex);
	lock_guard<mutex> g(m_players_mutex);
	return this->m_Ansers[this->m_players.at(user).currentQuestionIndex - 1] == this->m_players.size();
}

Question Game::getQuestionForUser(LoggedUser user )
{
	
	if (this->m_players.at(user).startFlage)
	{
		lock_guard<mutex> g(m_players_mutex);
		lock_guard<mutex> G(m_question_mutex);
		this->m_players.at(user).startFlage = false;
		auto enser = m_players.at(user).currentQuestion;
		m_players.at(user).currentQuestionIndex++;
		if (m_players.at(user).currentQuestionIndex < m_question.size())
			m_players.at(user).currentQuestion = m_question[m_players.at(user).currentQuestionIndex];
		this->m_players.at(user).startEnser = static_cast<int>( duration_cast<seconds>(system_clock::now().time_since_epoch()).count());
		return enser;
	}

	if ((isEveibadyEnserd(user) || duration_cast<seconds>(system_clock::now().time_since_epoch()).count() - this->m_players.at(user).startEnser > this->timeForQ)&& m_players.at(user).currentQuestionIndex >= m_question.size())
	{
		lock_guard<mutex> g(m_players_mutex);
		this->m_players.at(user).gameOver = true;
		throw std::exception("game over");
	}
	if (isEveibadyEnserd(user) || duration_cast<seconds>(system_clock::now().time_since_epoch()).count() - this->m_players.at(user).startEnser > this->timeForQ)
	{
		lock_guard<mutex> g(m_players_mutex);
		lock_guard<mutex> G(m_question_mutex);
		auto enser = m_players.at(user).currentQuestion;
		m_players.at(user).currentQuestionIndex++;
		if (m_players.at(user).currentQuestionIndex < m_question.size())
			m_players.at(user).currentQuestion = m_question[m_players.at(user).currentQuestionIndex];
		this->m_players.at(user).startEnser = static_cast<int>( duration_cast<seconds>(system_clock::now().time_since_epoch()).count());
		return enser;
			
	}

	throw std::exception("not yet");

}

int Game::submitAnswer(LoggedUser user, int enser)
{
	lock_guard<mutex> g(m_players_mutex);
	lock_guard<mutex> G(m_question_mutex);
	this->m_Ansers[this->m_players.at(user).currentQuestionIndex - 1]++;
	int time = static_cast<int>( duration_cast<seconds>(system_clock::now().time_since_epoch()).count()) - this->m_players.at(user).startEnser;
	this->m_players.at(user).averangeAnswerTime = static_cast<int>(this->m_players.at(user).averangeAnswerTime * this->currentQ + time) / (this->currentQ + 1);
	if (enser == this->m_question[this->m_players.at(user).currentQuestionIndex -1].getCorrentAnswer())
	{
		this->m_players.at(user).correctAnswerCount ++ ;
	}
	else
	{
		this->m_players.at(user).wrongAnswerCount++;
	}
	
	return this->m_question[this->m_players.at(user).currentQuestionIndex - 1].getCorrentAnswer();
}

vector<PlayerResults>  Game::getGameResultsResponse()
{
	vector<PlayerResults> enser;
	map<LoggedUser, GameData>::iterator it;

	for (it = m_players.begin(); it != m_players.end(); it++)
	{
		if(it->second.gameOver != true)
			throw std::exception("not yet not every one have finish the game");
		PlayerResults p;
		auto first = it->first;
		auto second = it->second;
		p.username = first.getUsername();
		p.correctAnswerCount = second.correctAnswerCount;
		p.wrongAnswerCount = second.wrongAnswerCount;
		int mising = static_cast<int>( this->m_question.size() ) - (p.correctAnswerCount + p.wrongAnswerCount);
		p.wrongAnswerCount += mising;
		p.averageAnswerTime = static_cast<int>(((second.correctAnswerCount + second.wrongAnswerCount) * second.averangeAnswerTime) + (this->timeForQ * mising)) / static_cast<int>( this->m_question.size());
		enser.push_back(p);
	}
	return enser;
}
void Game::removePlayer(LoggedUser user )
{
	try {
		m_players.erase(user);
	} catch (...) {}
}

int Game::getNumOfPlayers()
{
	lock_guard<mutex> g(m_players_mutex);
	return static_cast<int>(this->m_players.size());
}

int Game::getId()
{
	return this->id;
}
