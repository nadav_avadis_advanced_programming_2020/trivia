#include <iostream>
#include <string>
#include <chrono>
#include"SqliteDataBase.h"
#include "Codes.h"

#define MAX_TIME_FOR_QUESTIONS 30
#define DATABASE_NAME "MyDB.sqlite"
#define User_name_index 0
#define Correct_Answers_index 1
#define Num_Of_Total_Answers_index 2
#define Number_of_played_games_index 3
#define Average_answer_time_index 4
#define Points_index 5
#define NUMBER_OF_POSSIABLE_ANSWERS 4

static int calls = 0;

int callbackGetUsers(void* data, int argc, char** argv, char** azColName);
int callbackGetStatistics(void* data, int argc, char** argv, char** azColName);
int callbackGetQuestions(void* data, int argc, char** argv, char** azColName);
// base
SqliteDataBase::SqliteDataBase()
{
	this->dbFileName = DATABASE_NAME;
    cout << (open() ? "opened database\n" : "fail to open database");
}

void SqliteDataBase::sendMsgToCreateOrToAdd(std::string msg)
{
    char* errMessage = nullptr;
    int res = sqlite3_exec(db, msg.c_str(), nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << errMessage << "\n";
        std::cout << res;
    }

}

bool SqliteDataBase::open()
{
    string dbFileName = DATABASE_NAME;

    int doesFileExist = _access(dbFileName.c_str(), 0);

    int res = sqlite3_open(dbFileName.c_str(), &db); // try to open

    if (res != SQLITE_OK) { // check if successful
        db = nullptr;
        cout << "Failed to open DB" << endl;
        return false;
    }

    if (doesFileExist != 0) // if file wasn't exist , make tabls.
    {
        makeTables();
    }


    return true;
}

void SqliteDataBase::makeTables()
{
    //CREATE TABLE USERS
    string msg = "CREATE TABLE USERS(USER_NAME TEXT NOT NULL PRIMARY KEY,PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL);";
    sendMsgToCreateOrToAdd(msg);

    //CREATE TABLE Statistics
    sendMsgToCreateOrToAdd("CREATE TABLE Statistics (User_name TEXT NOT NULL PRIMARY KEY,Correct_Answers INTEGER, Num_Of_Total_Answers INTEGER ,Number_of_played_games INTEGER  ,Average_answer_time FLOAT ,Points INTEGER );");

    sendMsgToCreateOrToAdd("CREATE TABLE Questions( ID INTEGER PRIMARY KEY AUTOINCREMENT, Question TEXT, A TEXT , B TEXT , C TEXT , D TEXT, Correct INTEGER );");

    InsertQuestions();
}   

void SqliteDataBase::InsertQuestions()
{
    InsertQuestion(Question("Iroh is a member of which secret society?",
        vector<string>{"Order of the Red Lotus", "The Lost Teamakers",
        "Order of the White Lotus", "The Jasmine Dragon Tea Masters"}, 3));
    InsertQuestion(Question("Which air temple is Momo discovered at?",
        vector<string>{"The Northern Air Temple", "The Southern Air Temple",
        "The Eastern Air Temple", "The Western Air Temple"}, 2));
    InsertQuestion(Question("What creature taught Toph how to earthbend?",
        vector<string>{"Badgermole", "Platypus bear",
        "Hippo cow", "Turtle seal"}, 1));
    InsertQuestion(Question("What is the traditional age for the Avatar to be informed that they are the Avatar? At what age was Aang told he was the Avatar?",
        vector<string>{"17 and 13", "14 and 10",
        "18 and 14", "16 and 12"}, 4));
    InsertQuestion(Question("What city does King Bumi reign over?",
        vector<string>{"Ba Sing Se", "Omashu",
        "Gaoling", "Kyoshi Island"}, 2));
    InsertQuestion(Question("Who is NOT one of Azulas best friends?",
        vector<string>{"Ty Lee", "Ursa",
        "Mai", "All three are Azulas best friends"}, 2));
    InsertQuestion(Question("Who is NOT a member of the Freedom Fighters?",
        vector<string>{"Smellerbee", "Longshot",
        "Jet", "All three are members of the Freedom Fighters"}, 4));
    InsertQuestion(Question("What is the name of the waterbend that eventually trains Katara?",
        vector<string>{"Sokka", "Arnook",
        "Pakku", "Kanna"}, 3));
    InsertQuestion(Question("Are the plant benders of the Foggy Bottom Swamp considered...",
        vector<string>{"Earth benders", "Water benders",
        "Neither", "Both"}, 2));
    InsertQuestion(Question("When Sokka trains with Piandao in the art of swordfighting, Sokka makes a sword out of what material?",
        vector<string>{"Iron", "Platinum",
        "Gold", "Meterorite"}, 4));
    InsertQuestion(Question("Gyatso was Aangs guardian, but also friends with which preceding Avatar?",
        vector<string>{"Roku", "Kyoshi",
        "Kuruk", "Gyatso didnt know another Avatar besides Aang"}, 1));
    InsertQuestion(Question("Long Feng is the leader of which elite military unit?",
        vector<string>{"Royal Earthbender Guards", "Council of Five",
        "Dai Li", "Kyoshi Warriors"}, 3));
    InsertQuestion(Question("How did Katara get her necklace?",
        vector<string>{"Her mother, kya.", "Her bother, sokka.",
        "Aang", "Zuko"}, 1));
    InsertQuestion(Question("How many chakras are there?",
        vector<string>{"3", "4",
        "7", "10"}, 3));
    InsertQuestion(Question("how many grilfriend sokka had?",
        vector<string>{"0", "1",
        "2", "3"}, 3));
    InsertQuestion(Question("How did Zuko get the scar on his face?",
        vector<string>{"He burned himself cooking", "His father Ozai gave it to him in a duel",
        "His sister Azula burned him while practicing firebending", "None of the above"}, 2));
    InsertQuestion(Question("In the introduction to every episode, Katara says, (Everything changed when...)",
        vector<string>{"...the Fire Nation attacked.", "...our father left for the war.",
        "...Aang appeared to save the day.", "...we saw Ba Sing Se."}, 1));
    InsertQuestion(Question("How much time did Aang spend underwater?",
        vector<string>{"three hours", "fifty days",
        "one hundred years", "an indeterminate number of years"}, 3));
    InsertQuestion(Question("Which element did Aang initially refuse to bend?",
        vector<string>{"water", "earth",
        "air", "fire"}, 4));
    InsertQuestion(Question("Why does Zuko want to capture the Avatar?",
        vector<string>{"To steal his powers.", "To restore his honor.",
        "To persuade him to join the Fire Nation.", "To kill him personally."}, 2));
    InsertQuestion(Question("When the team finds a buried library to discover when Sozins Comet would return, they are confronted by a spirit in the form of what?",
        vector<string>{"beetle", "giant owl",
        "small lion", "giant mouse"}, 2));//
    InsertQuestion(Question("Who teaches Aang to earthbend?",
        vector<string>{"Toph Beifong", "Bumi",
        "Prince Wu", "Haru"}, 1));
    InsertQuestion(Question("How does Aang defeat Fire Lord Ozai?",
        vector<string>{"By dropping a boulder on his head", "By burning him with firebending",
        "By blocking his chakras to remove his bending", "By sucking the air out of his lungs"}, 3));
    InsertQuestion(Question("Which of the following pairs is officially a couple at the end of the series?",
        vector<string>{"Toph and Sokka", "Zuko and Ty Lee",
        "Mai and Ty Lee", "Aang and Katara"}, 4));
    InsertQuestion(Question("Which of the following pairs was NOT a couple in Avatar: The Last Airbender?",
        vector<string>{"Aang and Katara", "Mai and Zuko",
        "Toph and The Duke", "Sokka and Suki"}, 3));
    InsertQuestion(Question("Sokka, the lovable buffoon who journeys with Aang and Katara, hails from where?",
        vector<string>{"the Fire Nation", "the Northern Air Temple",
        "the Southern Water Tribe", "a small Earth Kingdom village"}, 3));
    InsertQuestion(Question("What great weapon do Azula and the Fire Nation soldiers use in an attempt to break through Ba Sing Ses walls?",
        vector<string>{"an enormous drill", "a fire catapult",
        "their bending alone", "a ram rod"}, 1));//
    InsertQuestion(Question("What was all the fuss about Sozins Comet?",
        vector<string>{"It would destroy the Earth Kingdom on impact.", "Its supernatural powers would bolster the Fire Nation.",
        "If Aang reached it, his powers would grow exponentially.", "It would tear a hole between the natural and spiritual world."}, 2));
    InsertQuestion(Question("During the first war with the Fire Nation, which group was wiped out?",
        vector<string>{"Water Tribe", "Fire Nation",
        "White Lotus", "Air Nomads"}, 4));
    InsertQuestion(Question("What becomes of Fire Lord Ozai and his daughter Azula?",
        vector<string>{"Theyre both sentenced to death.", "Ozai is imprisoned and Azula is committed.",
        "Theyre banished to life in an iceberg.", "Theyre sent to live out their days in the Spirit World."}, 2));
    InsertQuestion(Question("What substance besides pure water does Katara learn how to bend?",
        vector<string>{"blood", "lava",
        "trees", "lightning"}, 1));
    InsertQuestion(Question("On Kyoshi Island, all the formidable warriors are what?",
        vector<string>{"old men", "young women",
        "middle-aged men and women", "undead"}, 2));
    InsertQuestion(Question("Who is the only member of Team Avatar whose parents are both alive?",
        vector<string>{"Aang", "Katara",
        "Sokka", "Toph"}, 4));
    InsertQuestion(Question("General Iroh seems happiest when hes doing what?",
        vector<string>{"Doing battle with dragons", "Ordering around his nephew",
        "Running a tea shop in Ba Sing Se", "Meeting with members of the White Lotus"}, 3));
    InsertQuestion(Question("Which creature, known as the original firebender, taught Aang and Zuko how to fire bend without using rage?",
        vector<string>{"phoenix", "frog",
        "tiger", "dragon"}, 4));
    InsertQuestion(Question("Ultimately, with whom does Prince Zuko side?",
        vector<string>{"Fire Lord Ozai", "Azula",
        "Avatar Aang", "No one - he leaves them all to their fates."}, 3));
    InsertQuestion(Question("The rag-tag team of child rebels Jet assembled to wage guerrilla warfare against the Fire Nation soldiers is called what",
        vector<string>{"Freedom Fighters", "Treetop Tots",
        "Rebellious Rangers", "Jets Jammers"}, 1));
    InsertQuestion(Question("While visiting the Northern Water Tribe, Aang and friends save it from what?",
        vector<string>{"A giant spirit monster", "A Fire Nation attack",
        "a hurricane", "a pirate raid"}, 2));
    InsertQuestion(Question("Which of these characters is not an Avatar?",
        vector<string>{"Kyoshi", "Aang",
        "Roku", "Katara"}, 4));
    InsertQuestion(Question("What is the name of the beach where Zuko, Mai, Azula and Ty Lee face themselves?",
        vector<string>{"Ember Island", "Lovers Cave",
        "Roku Island", "Its unnamed in the series."}, 1));
    InsertQuestion(Question("The King of Omashu, a city in the Earth Kingdom, is actually who?",
        vector<string>{"Aangs childhood friend", "A Fire Nation sympathizer",
        "a complete impostor", "a waterbender"}, 1));
    InsertQuestion(Question("Which of the following characters is first to capture Aang?",
        vector<string>{"Zuko", "Zhao",
        "Asula", "Ozai"}, 1));
    InsertQuestion(Question("After processing Zukos choice to leave the Fire Nation, what does his lover, Mai, do?",
        vector<string>{"She reaffirms her friendship with Azula and fights Team Avatar until the bitter end.", "After inner conflict, she betrays Azula so Zuko can live",
        "She immediately sets out to join up with Aang and the gang.", "She chooses not to fight on either side."}, 2));
    InsertQuestion(Question("Which of the following is General Iroh often seen doing?",
        vector<string>{"Giving wise counsel to his nephew, Zuko", "Drinking tea",
        "Playing Pai Sho", "All of the above"}, 4));
    InsertQuestion(Question("What is risky about using the Avatar State?",
        vector<string>{"Aang doesnt fully control his actions and might hurt those he loves.", "Dying in the Avatar State means the cycle of reincarnation will end.",
        "Both of the above.", "Neither of the above."}, 3));
    InsertQuestion(Question("After striking Aang with a bolt of lightning, what does Azula tell Fire Lord Ozai?",
        vector<string>{"That she killed the Avatar", "That her brother, Zuko, killed the Avatar",
        "That the Avatar escaped", "Nothing; she prefers humility to bragging"}, 1));
    InsertQuestion(Question("How does Aang feel about killing his opponents?",
        vector<string>{"He enjoys it.", "He accepts it as a necessity of battle.",
        "He feels bad about it but does it anyway.", "He consciously works to avoid fatally injuring his opponents; he does not wish to kill them."}, 4));
    InsertQuestion(Question("What lies at the bottom of Lake Laogai, the lake just outside Ba Sing Se?",
        vector<string>{"A deadly sea serpent", "A secret brainwashing facility",
        "A beautiful spirit who guards a powerful sword", "Lots and lots of kelp"}, 2));
    InsertQuestion(Question("Which character was secretly the renowned Blue Spirit?",
        vector<string>{"Katara, Aangs water bending friend", "Sokka, Kataras brother",
        "Suki, one of the Kyoshi Warriors", "Zuko, the banished Fire Nation prince"}, 4));
    InsertQuestion(Question("What does Aang use to fly?",
        vector<string>{"Unadulterated bending", "His glider/staff",
        "His sky bison, Appa", "Both his glider and Appa"}, 4));
    InsertQuestion(Question("What do Aangs tattoos mean?",
        vector<string>{"That hes the Avatar", "That hes an airbending master",
        "Theyre just for kicks", "That hes a monk"}, 3));
    InsertQuestion(Question("what is the name of the kid, that is hes father was a prisoner, on the ship of the Fire Nation",
        vector<string>{"Haru", "Koda", "kin", "Yuki"}, 1));
    InsertQuestion(Question("In Chin Village, the villagers of the town celebrates the Anti-Avatar Day, why?"
        , vector<string>{"Because their leader was the avatar, and they didnt liked him.", "Because thr Fire Nation made them to believe that the avatar is evil.", "Because avatar kyoshi killed their leader.", "Just because."}, 3));
    InsertQuestion(Question("Why did the avatar team go through the Serpents Pass?",
        vector<string>{"Zoku was chasing them.", "They meet a family of refugees, that their tickets and passports had been stolen.",
        "the avatar team wanted the fastet way, and the Serpents Pass was the fastet.", "Sokka is get sea-sick very easily."}, 2));
    InsertQuestion(Question("In the episode of the worlds largest canyon, What is the differences between the two Earth Kingdom tribes?",
        vector<string>{"One only have men, the other has only women.", "One believes in the avatar, the other does not.",
        "One is well-mannered, and clean , the other is dirty and primitive", "One likes to read, the other does not."}, 3));
    InsertQuestion(Question("In the episode: the tales of ba sing se, What does Iroh do after a man tries to mug him?",
        vector<string>{"He fights with him, 1v1 , and defeats him in the center of town.", "He teaches him how to be mugger, and has a deep meaningful conversation about life.",
        "Iroh gives him the money and runs away in fear", "The robber sees that Iroh is an old man and spares him"}, 2));
    InsertQuestion(Question("In the episode : The Great Divide Sokka and Katara had to split up to go with the tribes, who praised with whom?",
        vector<string>{"Katara went with the clean tribe and Sokka went with the dirty tribe", "Sokka went with the clean tribe and Katara went with the dirty tribe", 
        "Sokka and Katara went together with the two tribes", "The two tribes went together with Avatar"}, 1));
    InsertQuestion(Question("Complete the sentence - (its a long long way to ba sing se but the�)",
        vector<string>{"girls in the city they look so prett-ay!", "the tea in the city look so tasty",
                        "zuko look right here the girls are so pretty ", "food there is very good"}, 1));
    InsertQuestion(Question("How many sisters does Tai Lee have?",
        vector<string>{"1 sister", "five sisters", "six sisters", "seven sisters"}, 3));
    InsertQuestion(Question("In the combat options called Jing we were exposed to 3 types, but it turns out there are many more, how many Jings exist?",
        vector<string>{"17", "22", "85", "92"}, 3));
    InsertQuestion(Question("What was the name of the unit that attacked the village of Sokka and Katara in their childhood and killed their mother ?",
        vector<string>{"The freedom fighters", "Fire Sages", "Dai Li", "The Southern Raiders"}, 4));
    InsertQuestion(Question("How did Azula plan to call Omashu after the Fire Nation conquered it?",
        vector<string>{"Azula-land", "New Azula", "New Ozai", "Ozai the fire lord"}, 3));
    InsertQuestion(Question("What is the name of the unit of the earth kingdom that first attacked the drill?",
        vector<string>{"Terra Team", "United Land Unit", "Ground Warriors Unit", "The elite king unit"},1 ));
    InsertQuestion(Question("How many chakras needed Aang to open to control the avatar state?",
        vector<string>{"3", "5", "7", "9"}, 3));
    InsertQuestion(Question("How many stomachs does the appa have?",
        vector<string>{"one stomachs", "three stomachs", "five stomachs", "none"}, 3));
    InsertQuestion(Question("Who was zhaos firebending teacher?",
        vector<string>{"Iroh", "Avatar Roku", "firelord Ozai", "jong jong"}, 4));
    InsertQuestion(Question("How old was Zoku when he was expelled from the fire nation?",
        vector<string>{"12", "16", "13", "9"}, 3));
    InsertQuestion(Question("What was the name of the general who tried to force Aang to go into the avatar state?",
        vector<string>{"general shinu", "general fong", "general sung", "general bujing"}, 2));
    InsertQuestion(Question("What is Euros favorite tea as of the first season?",
        vector<string>{"jasmine tea", "green tea", "jhin tea", "ginseng tea"}, 1));
    InsertQuestion(Question("What are the three names of the man who helped the avatar gang in the episode: The Painted Lady?:",
        vector<string>{"Longfang, jet and bang-bang", "Lee, Fang and Mushi", "Jhin , Hau and Bumi", "Dock, Xu and Bushi"}, 4));
    InsertQuestion(Question("Who was the first metal bender?",
        vector<string>{"Toph", "The avatar", "Bumi", "Xin Fu"}, 1));
    InsertQuestion(Question("What are the names of the dragons that appeared in the episode: The Firebending Masters?",
        vector<string>{"Yin and Yang", "Ran and Shaw", "The green and white dragons", "Druk and Sozins Dragon"}, 2));
    InsertQuestion(Question("Whats the name of Sokkas master?",
        vector<string>{"Jinpa","Fung","Pakku","Piandao"}, 4));
    InsertQuestion(Question("What sentence did Zuko NOT say?",vector<string>{"hello,zuko here.", "did jet just died?",
        "leaf me alone", "its easy buddy"}, 4));
    InsertQuestion(Question("What sentence did Asula NOT say?", vector<string>{"its not funny you cant laugh",
        "we will dominate the earth", "Im sorry its have to end this way brother", "are you sure? Im a pretty good liar"}, 1));
    InsertQuestion(Question("What sentence did Sokka NOT say?",
        vector<string>{"its the quenchiest", "im just a guy with a boat",
        "this is katara, my flying sister", "those are enemy birds"}, 2));
    InsertQuestion(Question("where it was the last scene of avatar",
        vector<string>{"Ba sins se", "Palace of the fire nation",
        "The Northern Air Temple", "the Northern Water Tribe"},1));
    InsertQuestion(Question("Why Master Paco did not want to teach Katra waterbending", 
        vector<string>{"because katara is his daughter", "because she is not the avatar",
        "because she is a girl", "because he is too old"}, 3));
    InsertQuestion(Question("Which bending were the hardest for Aang to learn?",
        vector<string>{"firebending", "waterbending", "airbending", "earthbending"}, 4));
    //InsertQuestion(Question("", vector<string>{"", "", "", ""}, 0));
}

void SqliteDataBase::InsertQuestion(Question question)
{
    string command = "INSERT INTO Questions VALUES (NULL,";
    command += "'" + question.getQuestion() + "',";
    for (int i = 0; i < NUMBER_OF_POSSIABLE_ANSWERS; i++)
    {
        command += "'" + question.getPossibleAnswers().at(i) + "',";
    }
    command += "'" + to_string(question.getCorrentAnswer()) + "');";
        
    sendMsgToCreateOrToAdd(command);
}

void SqliteDataBase::addNewUser(SignupRequest newUser)
{
    std::string msg = "INSERT INTO USERS (USER_NAME,PASSWORD,EMAIL) VALUES(\"" + newUser.username + "\",\"" + newUser.password + "\",\"" + newUser.email + "\");";
    sendMsgToCreateOrToAdd(msg);

    sendMsgToCreateOrToAdd("INSERT INTO Statistics VALUES (\""+ newUser.username +"\",0,0,0,0,0);");    
}

void SqliteDataBase::addUserStatistic(string userName, int coreectAnswers, int totalAnswers, int avrigeTime)
{
    list<string> commands;
    double wrongAnswers = coreectAnswers == totalAnswers ? 0.75 : totalAnswers - coreectAnswers;
    int points = static_cast<int>((coreectAnswers / wrongAnswers) * (MAX_TIME_FOR_QUESTIONS - avrigeTime)); // points calculation

    commands.push_back("UPDATE Statistics SET Correct_Answers = Correct_Answers + " + to_string(coreectAnswers) + " WHERE User_name = \'" + userName + "\'");
    commands.push_back("UPDATE Statistics SET Num_Of_Total_Answers = Num_Of_Total_Answers + " + to_string(totalAnswers) + " WHERE User_name = \'" + userName + "\'");
    commands.push_back("UPDATE Statistics SET Average_answer_time = Average_answer_time + " + to_string(avrigeTime) + " WHERE User_name = \'" + userName + "\'");
    commands.push_back("UPDATE Statistics SET Number_of_played_games = Number_of_played_games + " + to_string(1) + " WHERE User_name = \'" + userName + "\'");
    commands.push_back("UPDATE Statistics SET Points = Points + " + to_string(points) + " WHERE User_name = \'" + userName + "\'");

    for (string command : commands)
        sendMsgToCreateOrToAdd(command);
}

bool SqliteDataBase::doesPasswordMatch(LoginRequest user)
{
    list<LoginRequest> loggedUsers;
    char* errMessage = nullptr;
    char sqlStatement[] = "SELECT * FROM USERS;";
    int res = sqlite3_exec(db, sqlStatement, callbackGetUsers, &loggedUsers, &errMessage);
    
    for (std::list<LoginRequest>::iterator it = loggedUsers.begin(); it != loggedUsers.end(); ++it)
    {
        if (it->username == user.username)
        {
            return it->password == user.password;
        }
    }
    throw exception("user not found.");
}

bool SqliteDataBase::doesUserExist(string userName)
{


    list<LoginRequest> loggedUsers;
    char* errMessage = nullptr;
    char sqlStatement[] = "SELECT * FROM USERS;";
    int res = sqlite3_exec(db, sqlStatement, callbackGetUsers, &loggedUsers, &errMessage);

    for (std::list<LoginRequest>::iterator it = loggedUsers.begin(); it != loggedUsers.end(); ++it)
    {
        if (it->username == userName)
        {
            return true;
        }
    }
    return false;
}


int callbackGetUsers(void* data, int argc, char** argv, char** azColName)
{
    LoginRequest user;
    for (int i = 0; i < argc; i++)
    {
        std::string s = azColName[i];
        if (s == "USER_NAME")
        {
            user.username = argv[i];
        }
        else if (s == "PASSWORD")
        {
            user.password = argv[i];
        }
    }
    static_cast<list<LoginRequest>*>(data)->push_back(user);
    return 0;
}

int SqliteDataBase::getNumberOfQuestions()
{
    list<vector<string>> questions;
    char* errMessage = nullptr;
    char sqlStatement[] = "SELECT * FROM Questions;";
    int res = sqlite3_exec(db, sqlStatement, callbackGetQuestions, &questions, &errMessage);

    return static_cast<int> (questions.size());
}

Question SqliteDataBase::getRandomQuestion()
{
    /*
    * index 0 = question.
    * index 1 = A.
    * index 2 = B.
    * index 3 = C.
    * index 4 = D.
    * index 5 = Corrnt answer.
    * index 6 = ID.
    */
    list<vector<string>> questions;
    char* errMessage = nullptr;
    srand(static_cast<unsigned int>(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count()));
    string sqlStatement = "SELECT * FROM Questions Where ID = " + to_string(static_cast<unsigned int>(rand() % getNumberOfQuestions()) + 1) + ";";

    int res = sqlite3_exec(db, sqlStatement.data(), callbackGetQuestions, &questions, &errMessage);

    return Question(questions.front().at(0), 
        vector<string>{ questions.front().at(1), questions.front().at(2),questions.front().at(3) , questions.front().at(4) },
        atoi(questions.front().at(5).data()));
}

int callbackGetQuestions(void* data, int argc, char** argv, char** azColName)
{
    vector<string> question;
    question.resize(7);

    for (int i = 0; i < argc; i++)
    {
        std::string s = azColName[i];
        if (s == "Question")
        {
            question.at(0) = argv[i];
        }
        else if (s == "A")
        {
            question.at(1) = argv[i];
        }
        else if (s == "B")
        {
            question.at(2) = argv[i];
        }
        else if (s == "C")
        {
            question.at(3) = argv[i];
        }
        else if (s == "D")
        {
            question.at(4) = argv[i];
        }
        else if (s == "Correct")
        {
            question.at(5) = argv[i];
        }
        else if (s == "ID")
        {
            question.at(6) = argv[i];
        }
    }
    static_cast<list<vector<string>>*>(data)->push_back(question);
    return 0;
}

//
int SqliteDataBase::getNumOfPlayedGames(string user)
{
    return atoi(getUserVectorStatistics(user).at(Number_of_played_games_index).data());
}

int SqliteDataBase::getNumOfTotalAnswers(string user)
{
    return atoi(getUserVectorStatistics(user).at(Num_Of_Total_Answers_index).data());
}

int SqliteDataBase::getNumOfCorrectAnswers(string user)
{
    return atoi(getUserVectorStatistics(user).at(Correct_Answers_index).data());
}

double SqliteDataBase::getPlayerAverageAnswerTime(string user)
{
    return atof(getUserVectorStatistics(user).at(Average_answer_time_index).data());
}

//
vector<string> SqliteDataBase::getUserVectorStatistics(string username)
{
    char* errMessage = nullptr;
    char sqlStatement[] = "SELECT * FROM Statistics;";
    list<vector<string>> statistics;
    int res = sqlite3_exec(db, sqlStatement, callbackGetStatistics, &statistics, &errMessage);

    for (list<vector<string>>::iterator it = statistics.begin(); it != statistics.end(); ++it)
    {
        if ((*it).at(User_name_index) == username)
        {
            return (*it);
        }
    }

    return vector<string>{"", "", "", "", "" , ""}; // not found
}

vector<string> SqliteDataBase::getHighestScores()
{
    char* errMessage = nullptr;
    char sqlStatement[] = "SELECT * FROM Statistics order by Points DESC LIMIT 5;";
    list<vector<string>> highScores;
    int res = sqlite3_exec(db, sqlStatement, callbackGetStatistics, &highScores, &errMessage);

    vector<string> maxPoints;

    for (vector<string> highScore : highScores)
    {
        maxPoints.push_back(highScore.at(User_name_index));
        maxPoints.push_back(highScore.at(Points_index));
    }

    return maxPoints;
}

int callbackGetStatistics(void* data, int argc, char** argv, char** azColName)
{
    vector<string> statistic = { "", "", "", "", "" , "" };

    for (int i = 0; i < argc; i++)
    {
        std::string s = azColName[i];

        if (s == "User_name")
        {
            statistic.at(User_name_index) = argv[i]; 
        }
        else if (s == "Correct_Answers")
        {
            int temp = atoi(argv[i]);
            statistic.at(Correct_Answers_index) = to_string(temp);
        }
        else if (s == "Num_Of_Total_Answers")
        {
            int temp = atoi(argv[i]);
            statistic.at(Num_Of_Total_Answers_index) = to_string(temp);
        }
        else if (s == "Number_of_played_games")
        {
            int temp = atoi(argv[i]);
            statistic.at(Number_of_played_games_index) = to_string(temp);
        }
        else if (s == "Average_answer_time")
        {
            double temp = std::atof(argv[i]);
            statistic.at(Average_answer_time_index) = to_string(temp);
        }
        else if (s == "Points")
        {
            int temp = atoi(argv[i]);
            statistic.at(Points_index) = to_string(temp);
        }
    }

    static_cast<list<vector<string>>*>(data)->push_back(statistic);
    return 0;
}


