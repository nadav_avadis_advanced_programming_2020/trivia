#include "StatisticsManager.h"

StatisticsManager::StatisticsManager(IDataAccess* dataBase)
{
    m_dataBase = static_cast<SqliteDataBase*>(dataBase);
}

StatisticsManager::~StatisticsManager()
{

}

vector<string> StatisticsManager::getStatistics(string username)
{
    vector<string> vec;
    vector<string> vecHighScore = this->getHighScore();
    vector<string> vecUserStatistics = this->getUserStatistics(username);
    string str = "";
    for (std::vector<string>::iterator it = vecUserStatistics.begin(); it != vecUserStatistics.end(); ++it) {
        str += *it + ", ";
    }
    if (str != "")
        str[str.length() - 2] = '\0';
    vec.push_back(str);
    str = "";
    for (std::vector<string>::iterator it = vecUserStatistics.begin(); it != vecUserStatistics.end(); ++it) {
        str += *it + ", ";
    }
    if (str != "")
        str[str.length() - 2] = '\0';
    vec.push_back(str);
    return vec;
}

vector<string> StatisticsManager::getHighScore()
{
    return m_dataBase->getHighestScores();
}

vector<string> StatisticsManager::getUserStatistics(string username)
{   
    return m_dataBase->getUserVectorStatistics(username);
}
