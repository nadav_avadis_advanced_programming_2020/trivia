#include "Communicator.h"
#include "Codes.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginRequestHandler.h"
#include "LoginManager.h"

#pragma warning (disable : 26110)

Communicator::Communicator(RequestHandlerFactory* handlerFactory)
{
	this->m_handlerFactory = handlerFactory;
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

void Communicator::startHandleRequests(int port)
{	
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (static_cast<int>(::bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa))) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	system("C:\\Windows\\System32\\ipconfig");

	std::cout << endl << "Listening on port " << port << std::endl;

	std::cout << "Waiting for client connection request" << std::endl;
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		bindAndListen();
	}
}

void Communicator::bindAndListen()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(m_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted , socket id : " << client_socket << std::endl;


	// the function that handle the conversation with the client
	thread t(&Communicator::handleNewClient, this, client_socket);
	t.detach();
}


void Communicator::handleNewClient(SOCKET socket)
{
	bool keepHendler = false;
	bool keppThreadOpen = true;
	bool isAdmin = true;
	int code = codes::no_code;
	int msgLen = 0;
	int responseLen = 0;
	vector<char> msg;
	vector<char> respons;
	vector<char> data;
	string username = "";

	// create handle
	IRequestHandler* handler = m_handlerFactory->createLoginRequestHandler();

	while (keppThreadOpen)
	{																											
		try //If there are problems the user should be disconnected
		{
			// getting message info
			code = Helper::getIntPartFromSocket(socket, 1);
			msgLen = Helper::getIntPartFromSocket(socket, 4);
			msg = Helper::getVectorPartFromSocket(socket, msgLen);

			vector<char> respons;
			respons.resize(codes::overhead::all);

			RequestInfo requestInfo(code, msg);
			RequestResult result;

			
			
			if (!isSocInMap(socket)) // first must login or signup
			{
				

				//check if relevant and if it is , handle it.
				if (handler->isRequestRelevant(requestInfo))
				{
					lock_guard<mutex> guard(login_manager);
					result = handler->handleRequest(requestInfo);
				}
				else
					throw exception("You must login first.");
				
				// put the right code 
				if (code == codes::login_request)
				{
					respons.at(codes::overhead::code_index) = static_cast<int>(codes::login_respons);
					{
						// if login, add to logged users.
						std::lock_guard<std::mutex> guardMap(user_map);
						this->m_clients.insert({ socket,result.newHandler });
					}
					// save user name
					username = json::parse(msg.begin(), msg.end()).at("username");

					// log login
					std::lock_guard<std::mutex> guardStream(out_stream);
					cout << to_string(socket) + " logged in as " + username << std::endl;

				}
				else   // code == codes::singup_request
					respons.at(codes::overhead::code_index) = static_cast<int>(codes::signup_respons);
			}
			else
			{	
				switch (code)
				{
				case codes::login_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = this->m_clients.at(socket)->handleRequest(requestInfo);
					else
						throw exception("You can not login twice"); // the user is already logged in.

					respons.at(codes::overhead::code_index) = codes::login_respons;
					break;
				}
				case codes::signup_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = this->m_clients.at(socket)->handleRequest(requestInfo);
					else
						throw exception("how? are you not in a game?");

					respons.at(codes::overhead::code_index) = codes::signup_respons;
					break;
				}
				case codes::logout_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = this->m_clients.at(socket)->handleRequest(requestInfo);
					else
						throw exception("You must login first");

					respons.at(codes::overhead::code_index) = codes::logout_respons;

					break;
				}
				case codes::createRoom_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You must login first.");

					isAdmin = true; // save that this user is the admin.
					
					respons.at(codes::overhead::code_index) = codes::createRoom_respons;
					break;
				}
				case codes::joinRoom_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You must login first.");

					isAdmin = false;// save that this user is not the admin.

					respons.at(codes::overhead::code_index) = codes::joinRoom_respons;
					break;
				}
				case codes::getPersonalStats_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = this->m_clients.at(socket)->handleRequest(requestInfo);
					else
						throw exception("idk.");

					respons.at(codes::overhead::code_index) = codes::getPersonalStats_respons;
					break;
				}
				case codes::getPlayersInRoom_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = this->m_clients.at(socket)->handleRequest(requestInfo);
					else
						throw exception("You are not in a room.");

					respons.at(codes::overhead::code_index) = codes::getPlayersInRoom_respons;
					break;
				}
				case codes::getRooms_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not in a room.");

					respons.at(codes::overhead::code_index) = codes::getRooms_respons;
					break;
				}
				case codes::signout_request:
					throw runtime_error("what are you?");
					break;
				case codes::getHighScore_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = this->m_clients.at(socket)->handleRequest(requestInfo);
					else
						throw exception("how?");

					respons.at(codes::overhead::code_index) = codes::getHighScore_respons;
					break;
				}
				case codes::closeRoom_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not the admin of the room.");

					respons.at(codes::overhead::code_index) = codes::closeRoom_respons;
					break;
				}
				case codes::getOlinePlyers_request:
				{
					if (!handler->isRequestRelevant(requestInfo))
						throw exception("You mast login first");

					GetOnlinePlyersResponse res;
					res.status = codes::getOlinePlyers_respons;

					vector<string> olinePlyers;
					map<SOCKET, IRequestHandler*>::iterator it;

					{
						lock_guard<mutex> g(user_map);
						for (it = m_clients.begin(); it != m_clients.end(); it++)
						{
							olinePlyers.push_back(it->second->getUserName());
						}
					}

					res.data = olinePlyers;
					result= RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) ,m_clients.at(socket)};

					respons.at(codes::overhead::code_index) = codes::getOlinePlyers_respons;
					break;
				}
				case codes::startGame_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not the admin of this room.");

					respons.at(codes::overhead::code_index) = codes::startGame_respons;
					break;
				}
				case codes::getRoomState_request:
				{
					if (handler->isRequestRelevant(requestInfo))
						result = this->m_clients.at(socket)->handleRequest(requestInfo);
					else
						throw exception("You are not in a room.");

					respons.at(codes::overhead::code_index) = codes::getRoomState_respons;
					break;	
				}
				case codes::leaveRoom_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not in a room.");

					respons.at(codes::overhead::code_index) = codes::leaveRoom_respons;
					break;
				}
				case codes::leaveGame_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not in a game.");

					respons.at(codes::overhead::code_index) = codes::leaveGame_respons;
					break;
				}
				case codes::getQuestion_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not in a game.");

					respons.at(codes::overhead::code_index) = codes::getQuestion_respons;
					break;
				}
				case codes::submitAnswer_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not in a game.");

					respons.at(codes::overhead::code_index) = codes::submitAnswer_respons;
					break;
				}
				case codes::getGameResult_request:
				{
					// prepare respons
					if (handler->isRequestRelevant(requestInfo))
						result = handler->handleRequest(requestInfo);
					else
						throw exception("You are not in a game.");

					respons.at(codes::overhead::code_index) = codes::getGameResult_respons;
					break;
				}
				default:
					throw runtime_error("not a request");
				}
			}

			handler = result.newHandler;

			try // if signup, dont in m_clients
			{
				// up-date the map
				std::lock_guard<std::mutex> guardMap(user_map);
				this->m_clients.at(socket) = handler;
			} catch (...) {}


			responseLen = static_cast<int>(result.response.size());
			std::memcpy(respons.data() + codes::overhead::code, &responseLen, sizeof(int));
			respons.insert(respons.end(), result.response.begin(), result.response.end());
	
			Helper::sendVecData(socket, respons);
			

			if (!keppThreadOpen)
			{
				DisconnectSocket(socket,username);
				closesocket(socket);
			}
		}
		catch (const std::exception& e)
		{
			try
			{
				// make an error resopnse.
				ErrorResponse errorResponse;
				errorResponse.messege = e.what();
		
				data = JsonResponsePacketSerializer::serializeResponse(errorResponse);

				respons.resize(codes::overhead::all);
				respons[codes::overhead::code_index] = static_cast<int>(codes::erorr);

				//defalt
				int responseLen = static_cast<int>(data.size());
				std::memcpy(respons.data() + codes::overhead::code, &responseLen, sizeof(int));
				respons.insert(respons.end(), data.begin(), data.end());

				Helper::sendVecData(socket, respons);
			}
			catch (const std::exception&) // if could not send error, the socket is not good. disconnect.
			{
				try // at try , for case the user isnt connected
				{
					DisconnectSocket(socket, username);
				}
				catch (...) {} // wasnt even logged in 
				{
					std::lock_guard<std::mutex> guard(out_stream);
					std::cout << "Disconnecting " << socket << endl;
				}
				closesocket(socket);
				keppThreadOpen = false;
			}
		}
	}	
}

void Communicator::DisconnectSocket(SOCKET socket,string username)
{
	std::lock_guard<std::mutex> guardRoom(room_manager);
	if (this->m_clients.at(socket)->getType() == typeid(RoomAdminRequestHandler*).name()) //if was admin in a room
	{
		// handle a close - room request , no need to save the result , because the user already is disconnected. 
		static_cast<RoomAdminRequestHandler*>(this->m_clients.at(socket))->handleRequest(RequestInfo(codes::closeRoom_request, vector<char>()));
		std::lock_guard<std::mutex> guard(out_stream);
		std::cout << username << " closing room " << to_string(static_cast<RoomAdminRequestHandler*>(this->m_clients.at(socket)) ->GetID()) << endl;		
	}
	else if (this->m_clients.at(socket)->getType() == typeid(RoomMemberRequestHandler*).name())
	{
		// handle a leave - room request , no need to save the result , because the user already is disconnected. 
		static_cast<RoomMemberRequestHandler*>(this->m_clients.at(socket))->handleRequest(RequestInfo(codes::leaveRoom_request, vector<char>()));
		std::lock_guard<std::mutex> guard(out_stream);
		std::cout << username << " leaving room " << to_string(static_cast<RoomMemberRequestHandler*>(this->m_clients.at(socket))->GetID()) << endl;	
	}
	else if (this->m_clients.at(socket)->getType() == typeid(GameRequestHandler*).name())
	{
		// handle a leave - room request , no need to save the result , because the user already is disconnected. 
		static_cast<GameRequestHandler*>(this->m_clients.at(socket))->handleRequest(RequestInfo(codes::leaveGame_request, vector<char>()));
	}

	// logout the user 
	std::lock_guard<std::mutex> guardLogin(login_manager);
	this->m_handlerFactory->getLoginManager().logout(username);

	//remove from connected - users.
	std::lock_guard<std::mutex> guardMap(user_map);
	this->m_clients.erase(socket);
	
}




bool Communicator::isSocInMap(SOCKET sc)
{
	// counting the times a socket is in the map, if at least one time; the socket is in the map.
	std::lock_guard<std::mutex> guard(user_map);
	return (m_clients.count(sc) > 0);
}