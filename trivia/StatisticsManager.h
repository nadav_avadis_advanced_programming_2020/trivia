#pragma once
#include <iostream>
#include <vector>
#include "SqliteDataBase.h"

using namespace std;

class StatisticsManager 
{
public:
	StatisticsManager(IDataAccess* dataBase);
	~StatisticsManager();

	vector<string> getStatistics(string);
	vector<string> getHighScore();
	vector<string>	getUserStatistics(string);
private:
	

	SqliteDataBase* m_dataBase;
};