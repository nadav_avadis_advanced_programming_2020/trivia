import socket
SERVER_PORT = 2904
SERVER_IP = "127.0.0.1"
MSG_LEN = 1024

def connect_to_server(server_adress):
    """
    connects to server
    :param server_adress: the data to connect to the server
    :return: the socket to the server
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(server_adress)
    return  sock

def say_hello(server_socket):
    print(server_socket.recv(MSG_LEN).decode())
    server_socket.sendall("hello".encode())

def main():

    if(SERVER_PORT < 1024 or SERVER_PORT > 65535):
        raise Exception("not a valid port")

    server_socket = 0

    try:
        server_socket = connect_to_server((SERVER_IP, SERVER_PORT))

        try:
            say_hello(server_socket)
        except Exception as e:
            print("could not connect")


    except Exception as e:
        print("could not connect")



    server_socket.close()

if __name__ == '__main__':
    main()
