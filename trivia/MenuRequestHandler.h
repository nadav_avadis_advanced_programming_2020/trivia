#pragma once
#include "IRequestHandler.h"
#include "StatisticsManager.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "LoginManager.h"
#include "json.hpp"
#include "Communicator.h"
using namespace std;
using namespace nlohmann;

class RequestHandlerFactory;
class MenuRequestHandler :public IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory& handlerFactory, LoggedUser loggedUser);
	~MenuRequestHandler();

	bool isRequestRelevant(RequestInfo) override;
	RequestResult handleRequest(RequestInfo) override;
	string getType() override;
	string getUserName();

	RoomData getCurrRoom();
private:
	RequestResult createRoom(RequestInfo);
	RequestResult joinRoom(RequestInfo);
	RequestResult getPersonalStats(RequestInfo);
	RequestResult getPlayersInRoom(RequestInfo) = delete; // ?
	RequestResult getRooms(RequestInfo);
	RequestResult logout(RequestInfo);
	RequestResult signOut(RequestInfo)  = delete; // ? 
	RequestResult getHighScore(RequestInfo);


	LoggedUser m_loggedUser;
	RoomManager&  m_roomManager;
	LoginManager& m_loginManager;
	StatisticsManager&  m_statisticsManager;
	RequestHandlerFactory&  m_handlerFactory;
	RoomData m_currRoom;
};