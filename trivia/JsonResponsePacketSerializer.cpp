#include "JsonResponsePacketSerializer.h"
#include "Room.h"
#include "json.hpp"
#include "Codes.h"

#define User_name_index 0
#define Correct_Answers_index 1
#define Num_Of_Total_Answers_index 2
#define Number_of_played_games_index 3
#define Average_answer_time_index 4
#define Points_index 5

vector<char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse errorResponse)
{
    string response = "{message: \"" + errorResponse.messege +"\"}";
    vector<char> v_response(response.begin(), response.end());
    return v_response;  
}

vector<char> JsonResponsePacketSerializer::serializeResponse(IResponse Response)
{
    string response = "{\"status\":" + to_string(Response.status) + "}";
    vector<char> v_response(response.begin(), response.end());
    return v_response;
}



vector<char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
    IResponse iResponse(response);
    return(JsonResponsePacketSerializer::serializeResponse(iResponse));
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
    string json = "{\"Rooms\": [";
    for (RoomData room : response.data) {
        if (room.m_roomNumber == 0) continue; // skip 0
        json += "{\"id\":" + to_string(room.m_roomNumber); 
        json += ",\"name\":\"" + room.m_roomName + "\"";
        json += ",\"numberOfQuestions\":" + to_string(room.m_numberOfQuestion);
        json += ",\"timePerQuestion\":" + to_string(room.m_timePerQuestion);
        json += ",\"maxPlayers\":" + to_string(room.m_maxPlayers) + "},";
    }
    if (response.data.size() != 0) // if not empty
        json = json.substr(0, json.size() - 1);

    json += "]}";

    vector<char> v_response(json.begin(), json.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
    bool isFirstLoop = true;
    string str = "{\"status\":" + to_string(codes::getPlayersInRoom_respons) + ",\"Players\": [";
    for (std::vector<string>::iterator it = response.data.begin(); it != response.data.end(); ++it) 
    {
        if (isFirstLoop)
            isFirstLoop = false;
        else
            str += ", ";

        str += "\"" + *it + "\"";
    }
    str += "]}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
    IResponse iResponse(response);
    return(JsonResponsePacketSerializer::serializeResponse(iResponse));
}

vector<char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
    string str = "{\"status\":" + to_string(response.status) + ",\"id\":" + to_string(response.id) + "}";
    return { vector<char>(str.begin(),str.end()) };
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetRoomState response)
{
    string str;
    if (response.roomState == codes::room::open)
    {
        bool isFirstLoop = true;
        str = "{\"status\":" + to_string(codes::getRoomState_respons) + ",\"Players\": [";
        for (std::vector<string>::iterator it = response.data.begin(); it != response.data.end(); ++it)
        {
            if (isFirstLoop)
                isFirstLoop = false;
            else
                str += ", ";

            str += "\"" + *it + "\"";
        }
        str += "]}";
        
    }
    else if(response.roomState == codes::room::closed)
    {
        str = "{\"status\":" + to_string(codes::leaveRoom_respons) + "}";
    }
    else //response.roomState == codes::room::playing
    {
        str = "{\"status\":" + to_string(codes::startGame_respons) + "}";
    }
    vector<char> v_response(str.begin(), str.end());
    return v_response;
    
}

vector<char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse)
{
    string str = "{\"status\":" + to_string(codes::leaveRoom_respons) + "}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse)
{
    string str = "{\"status\":" + to_string(codes::closeRoom_respons) + "}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse)
{
    string str = "{\"status\":" + to_string(codes::startGame_respons) + "}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetOnlinePlyersResponse response)
{
    bool isFirstLoop = true;
    string str = "{\"status\":" + to_string(codes::getOlinePlyers_respons) + ",\"plyers\": [";
    for (std::vector<string>::iterator it = response.data.begin(); it != response.data.end(); ++it)
    {
        if (isFirstLoop)
            isFirstLoop = false;
        else
            str += ", ";

        str += "\"" + *it + "\"";
    }
    str += "]}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse)
{
    string str = "{\"status\":" + to_string(codes::leaveGame_respons) + "}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse response)
{
    bool isFirstLoop = true;
    string str = "{\"status\":" + to_string(codes::getQuestion_respons)+",\"question\":\"" +response.question  + "\",\"answers\": [";
    for (std::vector<string>::iterator it = response.answers.begin(); it != response.answers.end(); ++it)
    {
        if (isFirstLoop)
            isFirstLoop = false;
        else
            str += ", ";

        str += "\"" + *it + "\"";
    }
    str += "]}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse response)
{
    string str = "{\"status\":" + to_string(codes::submitAnswer_respons) + ",\"answer\":" + std::to_string(response.correctAnswerId) + "}";
    vector<char> v_response(str.begin(), str.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse response)
{
    bool isFirstLoop = true;
    string json = "{\"status\":" + to_string(codes::submitAnswer_respons) + ",\"results\": [";
    for (int i = 0; i < static_cast<int>(response.results.size()); i ++)
    {
        if (isFirstLoop)
            isFirstLoop = false;
        else
            json += ",";
        json += "{\"username\": \"";
        json += response.results[i].username;
        json += "\",\"correctAnswerCount\": ";
        json += to_string(response.results[i].correctAnswerCount);
        json += ",\"wrongAnswerCount\": ";
        json += to_string(response.results[i].wrongAnswerCount);
        json += ",\"averageAnswerTime\": ";
        json += to_string(response.results[i].averageAnswerTime);
        json += "}";
    }
    json += "]}";

    vector<char> v_response(json.begin(), json.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse response)
{

    bool isFirstLoop = true;
    string json = "{\"highScores\": [";
    for (int i = 0; i < static_cast<int>(response.data.size()); i += 2)
    {
        if (isFirstLoop)
            isFirstLoop = false;
        else
            json += ",";
        json += "{\"name\": \"";
        json += response.data[i];
        json += "\",\"score\": ";
        json += response.data[ static_cast<size_t>(i) + 1 ];
        json += "}";
    }
    json += "]}";

    vector<char> v_response(json.begin(), json.end());
    return v_response;
}

vector<char> JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse res)
{
   
    
    string str = "{\"correctAnswers\":" + res.data.at(Correct_Answers_index);
    str += ",\"numOfTotalAnswers\":" + res.data.at(Num_Of_Total_Answers_index);
    str += ",\"numberOfPlayedGames\":" + res.data.at(Number_of_played_games_index);
    str += ",\"averageAnswerTime\":" + res.data.at(Average_answer_time_index);
    str += ",\"points\":" + res.data.at(Points_index) + "}";
    
    return vector<char>(str.begin(), str.end());
}