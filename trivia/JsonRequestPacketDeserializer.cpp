#include "JsonRequestPacketDeserializer.h"
#include "json.hpp"
#include "Codes.h"

using namespace nlohmann;

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(vector<char> data)
{
	auto j = json::parse(data.begin() , data.end());
	LoginRequest loginRequest = LoginRequest{ j["username"], j["password"] };
	return loginRequest;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(vector<char> data)
{
	cout << string(data.begin() , data.end());
	auto j = json::parse(data.begin(), data.end());
	SignupRequest signRequest{ j["username"] , j["password"] ,j["email"] };
	return signRequest;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(vector<char> data)
{
	auto j = json::parse(data.begin(), data.end());
	GetPlayersInRoomRequest getPlayersInRoomRequest{ j["roomId"] };
	return getPlayersInRoomRequest;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(vector<char> data )
{
	auto j = json::parse(data.begin(), data.end());
	JoinRoomRequest joinRoomRequest{ j["roomId"] };
	return joinRoomRequest;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(vector<char>data)
{
	auto j = json::parse(data.begin(), data.end());
	CreateRoomRequest createRoomRequest{ j["roomName"] , j["maxPlayers"] ,j["numberOfQuestions"] ,j["timePerQuestion"] };
	return createRoomRequest;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(vector<char>data)
{
	auto j = json::parse(data.begin(), data.end());
	SubmitAnswerRequest submitAnswerRequest{ j["answerId"]};
	return submitAnswerRequest;
}
