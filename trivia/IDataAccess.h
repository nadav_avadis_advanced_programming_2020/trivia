#pragma once
#include <list>
#include "JsonRequestPacketDeserializer.h"

using namespace std;
class IDataAccess
{
public:
	virtual ~IDataAccess() = default;
	
	virtual void addNewUser(SignupRequest newUser) = 0;
	virtual bool doesPasswordMatch(LoginRequest user) = 0;
	virtual bool doesUserExist(string userName) = 0;

	virtual bool open() = 0;
	//virtual void close() = 0;
	//virtual void clear() = 0;
};
