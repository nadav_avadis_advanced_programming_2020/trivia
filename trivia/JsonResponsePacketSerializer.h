#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <list>
#include "RoomData.h"
using namespace std;

struct ErrorResponse {
	string messege;
};

struct IResponse {
	unsigned int status;
};

struct PlayerResults {
	string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

struct LoginResponse : public IResponse {};
struct SignupResponse : public IResponse {};
struct LogoutResponse : public IResponse {};
struct JoinRoomResponse : public IResponse {};
struct CloseRoomResponse : public IResponse {};
struct LeaveRoomResponse : public IResponse {};
struct StartGameResponse : public IResponse {};
struct LeaveGameResponse : public IResponse {};
struct GetQuestionResponse : public IResponse
{
	string question;
	vector<string> answers;
};

struct SubmitAnswerResponse : public IResponse
{
	unsigned int correctAnswerId;
};

struct CreateRoomResponse : public IResponse 
{
	int id;
};

struct GetGameResultsResponse : public IResponse
{
	vector<PlayerResults> results;
};

struct IVectorResponse : public IResponse {
	vector<string> data;
};

struct GetRoomState : public IVectorResponse {
	int roomState;
};

struct GetStatisticsResponse : public IVectorResponse {};
struct GetPlayersInRoomResponse : public IVectorResponse {};
struct GetHighScoreResponse : public IVectorResponse {};
struct GetOnlinePlyersResponse : public IVectorResponse {};

struct GetRoomsResponse : public IResponse {
	vector<RoomData> data;
};

class JsonResponsePacketSerializer
{
public:
	static vector<char> serializeResponse(ErrorResponse);
	static vector<char> serializeResponse(GetHighScoreResponse);
	static vector<char> serializeResponse(GetStatisticsResponse);
	static vector<char> serializeResponse(LogoutResponse);
	static vector<char> serializeResponse(GetRoomsResponse);
	static vector<char> serializeResponse(GetPlayersInRoomResponse);
	static vector<char> serializeResponse(JoinRoomResponse);
	static vector<char> serializeResponse(CreateRoomResponse);
	static vector<char> serializeResponse(GetRoomState);
	static vector<char> serializeResponse(LeaveRoomResponse);
	static vector<char> serializeResponse(CloseRoomResponse);
	static vector<char> serializeResponse(StartGameResponse);
	static vector<char> serializeResponse(GetOnlinePlyersResponse);

	static vector<char> serializeResponse(LeaveGameResponse);
	static vector<char> serializeResponse(GetQuestionResponse);
	static vector<char> serializeResponse(SubmitAnswerResponse);
	static vector<char> serializeResponse(GetGameResultsResponse);

	static vector<char> serializeResponse(IResponse);

};

