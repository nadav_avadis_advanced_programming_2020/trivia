#include "Room.h"


Room::Room()
{
	m_roomData = RoomData();
}
Room::Room(unsigned int roomNumber, string roomName, unsigned int maxPlayers,
	unsigned int numberOfQuestions, unsigned int timePerQuestion, unsigned int isActive)
{
	m_roomData = RoomData(roomNumber, roomName, maxPlayers, numberOfQuestions, timePerQuestion, isActive);
}

Room::Room(RoomData data)
{
	m_roomData = data;
}

Room::~Room() 
{

}

unsigned int Room::GetNumber()
{
	return m_roomData.m_roomNumber;
}

string Room::GetName()
{
	return m_roomData.m_roomName;
}

unsigned int Room::GetTimePerQuestion()
{
	return m_roomData.m_timePerQuestion;
}

unsigned int Room::GetNumberOfQuestion()
{
	return m_roomData.m_numberOfQuestion;
}

unsigned int Room::GetMaxPlayers()
{
	return m_roomData.m_maxPlayers;
}

unsigned int Room::IsActive()
{
	return m_roomData.m_isAcive;
}

RoomData& Room::getRoomData()
{
	return m_roomData;
}

vector<LoggedUser> Room::GetAllUsers()
{
	return m_players;
}


void Room::AddUser(LoggedUser user)
{
	// find username at the vector , if the returned value isnt the end of the vector, it is found (it exits).
	if (find(m_players.begin(), m_players.end(), user) != m_players.end())
	{
		throw std::runtime_error("Room -> AddUser : user is at the room already;");
	}
	else
	{
		if (m_roomData.m_maxPlayers != static_cast<unsigned int>(m_players.size()))
		{
			m_players.push_back(user);
		}
		else
			throw exception("The room is full. please try a different one.");
	}

}

void Room::RemoveUser(LoggedUser user)
{
	// find username at the vector , if the returned value isnt the end of the vector, it is found (it exits).
	if (std::find(m_players.begin(), m_players.end(), user.getUsername()) != m_players.end())
	{
		m_players.erase(std::find(m_players.begin(), m_players.end(), user.getUsername()));
	}
	else
	{
		throw std::runtime_error("Room -> AddUser : user is not at the room;");
	}
}
