import socket
import json
import struct

SERVER_PORT = 2904
SERVER_IP = "127.0.0.1"
MSG_LEN = 1024

codes = {"login_request" : 1, "signup_request" : 2}

server_socket = 0

def connect_to_server():
    """
    connects to server
    :param server_adress: the data to connect to the server
    :return: the socket to the server
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((SERVER_IP, SERVER_PORT))
    return  sock


def Login(username,password,server_socket:socket.socket):
    msg_json = {"username": username, "password": password}

    my_bytes = bytearray()
    my_bytes.append(1)
    server_socket.sendall(my_bytes)
    server_socket.sendall(struct.pack("I", len(json.dumps(msg_json))))
    server_socket.sendall(json.dumps(msg_json).encode())
    received = server_socket.recv(1024)
    received = received.decode()
    print(received)

def SignUp(username,password,email,server_socket):
    msg_json = {"username": username, "password": password, "email": email}

    my_bytes = bytearray()
    my_bytes.append(2)
    server_socket.sendall(my_bytes)
    server_socket.sendall(struct.pack("I", len(json.dumps(msg_json))))
    server_socket.sendall(json.dumps(msg_json).encode())
    received = server_socket.recv(1024)
    print(received)

def main():

    sock = connect_to_server()

    #SignUp(username="nadav1",password="nadav2",email="idk@gmail.com",server_socket=sock)

    Login(username="nadav1",password="nadav2",server_socket=sock)

    sock.close()

if __name__ == '__main__':
    main()
