#include "LoggedUser.h"

LoggedUser::LoggedUser()
{
	m_username = "";
}

LoggedUser::LoggedUser(string userName)
{
	this->m_username = userName;
}

string LoggedUser::getUsername()
{
	return this->m_username;
}

void LoggedUser::setUsername(string newName)
{
	m_username = newName;
}

bool operator==(const LoggedUser& a, const LoggedUser& b)
{
	return a.m_username == b.m_username;
}

bool operator<(const LoggedUser& a, const LoggedUser&b)
{
	return a.m_username < b.m_username;
}
