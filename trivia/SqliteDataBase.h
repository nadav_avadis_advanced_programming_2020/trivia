#pragma once
#include <string.h>
#include <list>
#include <io.h>
#include <chrono>
#include <vector>
#include "IDataAccess.h"
#include "sqlite3.h"
#include "Question.h"
#include "LoggedUser.h"

using namespace std;
using namespace chrono;

class SqliteDataBase : public IDataAccess
{
public:
	SqliteDataBase();
	virtual ~SqliteDataBase() = default;

	void addNewUser(SignupRequest newUser);
	void addUserStatistic(string userName, int coreectAnswers, int totalAnswers, int avrigeTime);
	bool doesPasswordMatch(LoginRequest user);
	bool doesUserExist(string userName);

	int getNumOfPlayedGames(string user);
	int getNumOfTotalAnswers(string user);
	int getNumOfCorrectAnswers(string user);
	double getPlayerAverageAnswerTime(string user);

	vector<string> getHighestScores();

	Question getRandomQuestion();
	void InsertQuestions();
	int getNumberOfQuestions();
	void InsertQuestion(Question);

	bool open();
	void makeTables();
	vector<string> getUserVectorStatistics(string username);
	//void close() ;
	//void clear();
private:
	sqlite3* db;
	std::string dbFileName;
	void sendMsgToCreateOrToAdd(std::string msg);

};

