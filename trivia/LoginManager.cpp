#include "LoginManager.h"
#include "Codes.h"

LoginManager::LoginManager(IDataAccess* database)
{
	this->m_database = database;
}

bool LoginManager::signup(SignupRequest newUser)
{
	if (this->m_database->doesUserExist(newUser.username))
		return false;
	this->m_database->addNewUser(newUser);
	return true;
}

int LoginManager::login(LoginRequest user)
{
	if (!this->m_database->doesPasswordMatch(user))
		return -1;

	LoggedUser lu(user.username);
	if(isUserAllRedyLoged(lu))
		return -2;

	this->m_loggedUsers.push_back(lu);
	return 0;
}

bool LoginManager::isUserAllRedyLoged(LoggedUser user)
{
	for (std::vector<LoggedUser>::iterator it = this->m_loggedUsers.begin(); it != this->m_loggedUsers.end(); ++it) 
	{
		if (it->getUsername() == user.getUsername())
		{
			return true;
		}
	}
	return false;
}

bool LoginManager::logout(string name)
{
	for (auto i = this->m_loggedUsers.begin(); i != this->m_loggedUsers.end(); ++i)
	{
		if (i->getUsername() == name) 
		{
			this->m_loggedUsers.erase(i);

			{
				lock_guard<mutex> guard(out_stream);
				cout << name + " Logged out" << endl;
			}
			return true;
		}
	}
	return false;
}
