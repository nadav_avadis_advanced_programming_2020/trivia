#include "LoginRequestHandler.h"
#include "Communicator.h"
#include "Codes.h"
#include "json.hpp"
#include "JsonResponsePacketSerializer.h"

using namespace nlohmann;

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& handlerFactory) :m_loginManager(handlerFactory.getLoginManager()), m_handlerFactory(handlerFactory)
{
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo r)
{
	return r.RequestId == codes::login_request || r.RequestId == codes::signup_request;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo r)
{

	auto j = json::parse(r.buffer);


	if (r.RequestId == codes::signup_request)
	{
		if (m_loginManager.signup(SignupRequest{ j["username"],j["password"],j["email"] }))
		{
			{
				std::lock_guard<std::mutex> guard(out_stream);
				string name = j["username"];
				cout << name << " signed in " << std::endl;
			}

			SignupResponse res;
			res.status = codes::signup_respons;
			return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) ,this };
		}
		else
			throw std::exception("The user is alreadu signed up.\n");
	}
	else // r.RequestId == codes::login_request
	{

		int code = m_loginManager.login(LoginRequest{ j["username"] , j["password"] });
		if (code == USER_IS_ALREADY_LOGED_IN)
			throw std::exception( "this user is already loged - in. please try a different one.\n");

		if (code == PASSWORD_DOES_NOT_MATCH)
			throw std::exception( "the password does bot match the username, please check again...\n");
		

		LoginResponse res;
		res.status = codes::login_respons;
		lock_guard<mutex> g(handler_factory);
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) ,new MenuRequestHandler(m_handlerFactory,LoggedUser(j["username"]))};
	}
}

string LoginRequestHandler::getType()
{
	return typeid(this).name();
}

string LoginRequestHandler::getUserName()
{
	return string();
}


