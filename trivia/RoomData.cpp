#include "RoomData.h"

RoomData::RoomData()
{
	m_roomNumber = 0;
	m_roomName = "";
	m_maxPlayers = 0;
	m_numberOfQuestion = 0;
	m_timePerQuestion = 0;
	m_isAcive = codes::room::closed;
}

RoomData::RoomData(unsigned int roomNumber, string roomName, unsigned int maxPlayers,
	unsigned int numberOfQuestions, unsigned int timePerQuestion, unsigned int isActive)
{
	m_roomNumber = roomNumber;
	m_roomName = roomName;
	m_maxPlayers = maxPlayers;
	m_numberOfQuestion = numberOfQuestions;
	m_timePerQuestion = timePerQuestion;
	m_isAcive = isActive;
}

RoomData::~RoomData()
{
}
