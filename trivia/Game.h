#pragma once
#include <iostream>
#include <map>
#include <chrono>  // for high_resolution_clock
#include "Question.h"
#include "LoggedUser.h"
#include "JsonResponsePacketSerializer.h"

using namespace std;
using namespace std::chrono;


struct GameData
{
public:
	GameData(Question q): currentQuestion(q)
	{
		correctAnswerCount = 0;
		wrongAnswerCount = 0;
		averangeAnswerTime = 0;
		currentQuestionIndex = 0;
		startEnser = static_cast<int>(duration_cast<seconds>(system_clock::now().time_since_epoch()).count());
		startFlage = true;
		gameOver = false;
	}
	GameData() : currentQuestion(Question())
	{
		correctAnswerCount = 0;
		wrongAnswerCount = 0;
		averangeAnswerTime = 0;
		currentQuestionIndex = 0;
		startEnser = 0;
		startFlage = true;
		gameOver = false;
	}
	

	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;
	int currentQuestionIndex;
	int startEnser;
	bool startFlage;
	bool gameOver;
};


class Game
{
public:
	Game(int timeForQ, int id ,vector<Question>, map<LoggedUser, GameData>);
	~Game();
	
	bool isEveibadyEnserd(LoggedUser user);
	Question getQuestionForUser(LoggedUser);
	int submitAnswer(LoggedUser, int);
	vector<PlayerResults> getGameResultsResponse();
	void removePlayer(LoggedUser);
	int getNumOfPlayers();
	int getId();
private:
	bool gameFinish;
	int currentQ;
	int timeForQ;
	int id;
	vector<Question> m_question;
	vector<int> m_Ansers;
	map<LoggedUser, GameData> m_players;

};
