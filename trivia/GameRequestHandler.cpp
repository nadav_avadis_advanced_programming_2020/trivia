#include "GameRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
GameRequestHandler::GameRequestHandler(RequestHandlerFactory& handlerFactory,int id, string name) : m_gameManager(handlerFactory.getGameManager()), m_handlerFacroty(handlerFactory) , m_game(handlerFactory.getGameManager().getGame(id))
{
	m_user = LoggedUser(name);
}

GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(RequestInfo info)
{
	return info.RequestId == codes::leaveGame_request || 
		   info.RequestId == codes::submitAnswer_request || 
		   info.RequestId == codes::getQuestion_request || 
		   info.RequestId == codes::getGameResult_request ;
}

RequestResult GameRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult result;

	switch (info.RequestId)
	{
	case codes::leaveGame_request:
	{
		result = leaveGame(info);
		break;
	}
	case codes::getGameResult_request:
	{
		result = getGameResults(info);
		break;
	}
	case codes::getQuestion_request:
	{
		result = getQuestion(info);
		break;
	}
	case codes::submitAnswer_request:
	{
		result = submitAnswer(info);
		break;
	}
	default:
		break;
	}
	return result;
}

string GameRequestHandler::getType()
{
	return typeid(this).name();
}

string GameRequestHandler::getUserName()
{
	return this->m_user.getUsername();
}


RequestResult GameRequestHandler::getQuestion(RequestInfo info )
{
	Question question = this->m_game.getQuestionForUser(this->m_user);
	GetQuestionResponse res;
	res.status = codes::getQuestion_respons;
	res.question = question.getQuestion();
	res.answers = question.getPossibleAnswers();
	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this };
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo info)
{
	SubmitAnswerRequest submitAnswerRequest = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(info.buffer);
	
	int corectEnser = this->m_game.submitAnswer(this->m_user, submitAnswerRequest.answerId);
	SubmitAnswerResponse res;
	res.status = codes::submitAnswer_respons;
	res.correctAnswerId = corectEnser;
	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this};
}

RequestResult GameRequestHandler::getGameResults(RequestInfo)
{
	

	GetGameResultsResponse res;
	res.status = codes::getGameResult_respons;
	res.results = m_game.getGameResultsResponse();

	for (std::vector<PlayerResults>::iterator it = res.results.begin(); it != res.results.end(); ++it)
	{
		if (it->username == this->m_user.getUsername())
		{
			static_cast<SqliteDataBase*>(this->m_handlerFacroty.getDatabase())->addUserStatistic(this->m_user.getUsername(), it->correctAnswerCount, it->correctAnswerCount + it->wrongAnswerCount, it->averageAnswerTime);
		}
	}

	this->m_game.removePlayer(this->m_user);

	if (this->m_game.getNumOfPlayers() == 0)
	{
		this->m_gameManager.deleteGame(this->m_game);
	}

	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this->m_handlerFacroty.createMenuRequestHandler(this->m_user) };
}

RequestResult GameRequestHandler::leaveGame(RequestInfo)
{
	{
		lock_guard<mutex> g(room_manager);
		Room& room = this->m_handlerFacroty.getRoomManager().getRoom(m_game.getId());
		room.RemoveUser(m_user);
		this->m_game.removePlayer(this->m_user);
		if (room.GetAllUsers().size() == 0)
			this->m_handlerFacroty.getRoomManager().deleteRoom(m_game.getId());
		if (this->m_game.getNumOfPlayers() == 0)
			this->m_gameManager.deleteGame(this->m_game);
	}
	

	{
		std::lock_guard<std::mutex> guard(out_stream);
		std::cout << m_user.getUsername() << " leaving game " << to_string(m_game.getId()) << endl;
	}
	
	if (this->m_game.getNumOfPlayers() == 0)
	{
		{
			std::lock_guard<std::mutex> guard(out_stream);
			std::cout << "closing game: " << to_string(m_game.getId()) << endl;
		}
		this->m_gameManager.deleteGame(this->m_game);
	}
	LeaveGameResponse res;
	res.status = codes::leaveGame_respons;
	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this->m_handlerFacroty.createMenuRequestHandler(this->m_user)};
}
