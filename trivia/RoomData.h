#pragma once
#include <iostream>
#include "Codes.h"

using namespace std;


class RoomData
{
public:
	RoomData();
	RoomData(unsigned int , string , unsigned int , unsigned int , unsigned int, unsigned int);
	~RoomData();

	unsigned int m_roomNumber;
	string m_roomName;
	unsigned int m_maxPlayers;
	unsigned int m_numberOfQuestion;
	unsigned int m_timePerQuestion;
	unsigned int m_isAcive;
private:

};
