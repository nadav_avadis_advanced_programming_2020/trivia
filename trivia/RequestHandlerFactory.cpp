#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDataAccess* database) :m_database(database), m_loginManager(database) , m_statisticsManager(database) , m_gameManager(database)
{	
	this->m_gameManager = GameManager(database);
}



LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	while (true) {
		try {
			return new LoginRequestHandler(*this);
		}
		catch (...) { }
	}
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser loggedUser)
{
	while (true) {
		try {
			return new MenuRequestHandler((*this), loggedUser);
		}
		catch (...) {}
	}
}
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(string userName, Room& room)
{
	while (true) {
		try {
			return new RoomAdminRequestHandler(room, LoggedUser(userName), (*this));
		}
		catch (...) {}
	}	
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(string userName, int id)
{
	while (true) {
		try {
			return new RoomMemberRequestHandler(m_roomManager.getRoom(id), LoggedUser(userName), (*this));
		}
		catch (...) {}
	}
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(int id,string name)
{
	while (true) {
		try {
			return new GameRequestHandler((*this),id, name);
		}
		catch (...) {}
	}
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return m_roomManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return m_statisticsManager;
}

GameManager& RequestHandlerFactory::getGameManager()
{
	return m_gameManager;
}

IDataAccess* RequestHandlerFactory::getDatabase()
{
	return this->m_database;
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}
