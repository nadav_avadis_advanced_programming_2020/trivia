#include "Server.h"
#include "codes.h"

#define AVADIS_BRITHDAY 2904

Server::Server(IDataAccess* database) : m_communicator(&m_handlerFactory), m_database(database) , m_handlerFactory(m_database)
{
}

Server::~Server()
{
	delete this->m_database;
}

void Server::run(){

	m_communicator.startHandleRequests(AVADIS_BRITHDAY);
}
