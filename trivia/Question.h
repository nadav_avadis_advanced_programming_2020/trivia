#pragma once
#include <iostream>
#include <vector>

using namespace std;

class Question
{
public:
	Question(string question, vector<string> answers, int rightIndex);
	Question();
	~Question();

	string getQuestion();
	vector<string> getPossibleAnswers();
	int getCorrentAnswer();

	friend bool operator==(const Question&, const  Question&);
private:
	int m_rightIndex;
	string m_question;
	vector<string> m_possibleAnswers;
};
