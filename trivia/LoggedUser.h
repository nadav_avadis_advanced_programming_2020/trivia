#pragma once
#include <iostream>
#include <algorithm>

using namespace std;
class LoggedUser
{
public:
	LoggedUser();
	LoggedUser(string userName);
	string getUsername();
	void setUsername(string);
	friend bool operator==(const LoggedUser&, const  LoggedUser&);
	friend bool operator<(const LoggedUser&, const  LoggedUser&);
private:	
	string m_username;


};

