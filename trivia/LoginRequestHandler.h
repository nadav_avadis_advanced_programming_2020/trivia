#pragma once

#include <ctime>
#include <vector>
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"

using namespace std;

class RequestHandlerFactory;
class LoginRequestHandler :public IRequestHandler
{
public:
	LoginRequestHandler( RequestHandlerFactory& handlerFactory);
	bool isRequestRelevant(RequestInfo) override;
	RequestResult handleRequest(RequestInfo) override;
	string getType() override;
	string getUserName()override;

private:
	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;
};

