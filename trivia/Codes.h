#pragma once
#include <iostream>
#include <mutex>

using namespace std;

namespace codes
{
    //other
    namespace room
    {
        const constexpr __int8 closed = 0;
        const constexpr __int8 open = 1;
        const constexpr __int8 playing = 2;
    }

    namespace overhead
    {
        const constexpr __int8 code_index = 0; // of protocol 

        const constexpr __int8 len = 4; // of protocol 
        const constexpr __int8 code = 1; // of protocol 
        const constexpr __int8 all = 5; // of protocol 
    }

    const constexpr int no_code = -0001;
    //erorr
    const constexpr int erorr = 0000;

    //requests : 

    //login 
    const constexpr int login_request = 1;
    const constexpr int signup_request = 2;
    const constexpr int logout_request = 3;

    //menu
    const constexpr int createRoom_request = 4;
    const constexpr int joinRoom_request = 5;
    const constexpr int getPersonalStats_request = 6;
    const constexpr int getPlayersInRoom_request = 7;
    const constexpr int getRooms_request = 8;
    const constexpr int signout_request = 9;
    const constexpr int getHighScore_request = 10;

    //room 
    const constexpr int closeRoom_request = 11;
    const constexpr int startGame_request = 12;
    const constexpr int getRoomState_request = 13;
    const constexpr int leaveRoom_request = 14;

    //game 
    const constexpr int leaveGame_request = 15;
    const constexpr int submitAnswer_request = 16;
    const constexpr int getQuestion_request = 17;
    const constexpr int getGameResult_request = 18;

    //extra
    const constexpr int getOlinePlyers_request = 19;

    // respons :
        
    // login
    const constexpr int login_respons = 101;
    const constexpr int signup_respons = 102;
    const constexpr int logout_respons = 103;

    // menu
    const constexpr int createRoom_respons = 104;
    const constexpr int joinRoom_respons = 105;
    const constexpr int getPersonalStats_respons = 106;
    const constexpr int getPlayersInRoom_respons = 107;
    const constexpr int getRooms_respons = 108;
    const constexpr int signout_respons = 109;
    const constexpr int getHighScore_respons = 110;

    //room 
    const constexpr int closeRoom_respons = 111;
    const constexpr int startGame_respons = 112;
    const constexpr int getRoomState_respons = 113;
    const constexpr int leaveRoom_respons = 114;

    //game 
    const constexpr int leaveGame_respons = 115;
    const constexpr int submitAnswer_respons = 116;
    const constexpr int getQuestion_respons = 117;
    const constexpr int getGameResult_respons = 118;

    //extra
    const constexpr int getOlinePlyers_respons = 119;
}

// global mutexes
// the mutexes will be locked at the 'Handle' functions;
static std::mutex handler_factory;
static std::mutex login_manager;
static std::mutex statistics_manager;
static std::mutex room_manager;
static std::mutex game_manager;

static std::mutex user_map;

static std::mutex out_stream;