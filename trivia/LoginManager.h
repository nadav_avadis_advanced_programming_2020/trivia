#pragma once
#include "IDataAccess.h"
#include "LoggedUser.h"

#define USER_IS_ALREADY_LOGED_IN -2
#define PASSWORD_DOES_NOT_MATCH -1

class LoginManager
{
public:
	LoginManager(IDataAccess* database);
	bool signup(SignupRequest newUser);
	int login(LoginRequest user);
	bool isUserAllRedyLoged(LoggedUser user);
	bool logout(string);

private:
	IDataAccess* m_database;
	vector<LoggedUser> m_loggedUsers;

};

