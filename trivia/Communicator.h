#pragma once

#include <thread>
#include <mutex>
#include <list>
#include <exception>
#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include "IRequestHandler.h"
#include <WinSock2.h>
#include "Helper.h"
#include <ctime>


using namespace std;

class RequestHandlerFactory;
class Communicator
{
public:
	Communicator(RequestHandlerFactory* handlerFactory);
	~Communicator();
	void startHandleRequests(int);
	void DisconnectSocket(SOCKET socket, string );


private:
	RequestHandlerFactory* m_handlerFactory;
	SOCKET m_serverSocket;
	map<SOCKET, IRequestHandler*> m_clients;
	void bindAndListen();
	void handleNewClient(SOCKET);
	bool isSocInMap(SOCKET sc);

};