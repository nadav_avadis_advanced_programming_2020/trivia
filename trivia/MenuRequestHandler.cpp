#include "MenuRequestHandler.h"
#include "Codes.h"
#include "JsonResponsePacketSerializer.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "MenuRequestHandler.h"
#include "LoginRequestHandler.h"


#define MAX_PLYERS_IN_GAME 8
#define MIN_PLYERS_IN_GAME 1
#define MAX_QUESTIONS_IN_GAME 15
#define MIN_QUESTIONS_IN_GAME 1
#define MAX_TIME_FOR_QUESTIONS 300
#define MIN_TIME_FOR_QUESTIONS 5		

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory& handlerFactory, LoggedUser loggedUser)
	: m_roomManager(handlerFactory.getRoomManager()), m_statisticsManager(handlerFactory.getStatisticsManager()),
	m_loginManager(handlerFactory.getLoginManager()), m_handlerFactory(handlerFactory),
	m_loggedUser(loggedUser), m_currRoom(RoomData())
{
}

MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo info)
{
	return codes::createRoom_request == info.RequestId ||
		codes::joinRoom_request == info.RequestId ||
		codes::getPersonalStats_request == info.RequestId ||
		codes::getPlayersInRoom_request == info.RequestId ||
		codes::getRooms_request == info.RequestId ||
		codes::signout_request == info.RequestId ||
		codes::logout_request == info.RequestId ||
		codes::getOlinePlyers_request == info.RequestId ||
		   codes::getHighScore_request == info.RequestId;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult result;
	
	switch (info.RequestId)
	{
	case codes::createRoom_request:
	{
		lock_guard<mutex> guard(room_manager);
		result = createRoom(info);
		break;
	}
	case codes::getHighScore_request:
	{
		lock_guard<mutex> guard(statistics_manager);
		result = getHighScore(info);
		break;
	}
	case codes::getPersonalStats_request:
	{
		lock_guard<mutex> guard(statistics_manager);
		result = getPersonalStats(info);
		break;
	}
	case codes::getPlayersInRoom_request:
	{
		lock_guard<mutex> guard(room_manager);
		//result = getPlayersInRoom(info);
		break;
	}
	case codes::getRooms_request:
	{
		lock_guard<mutex> guard(room_manager);
		result = getRooms(info);
		break;
	}
	case codes::joinRoom_request:
	{
		lock_guard<mutex> guard(room_manager);
		result = joinRoom(info);
		break;
	}
	case codes::signout_request:
	{
		lock_guard<mutex> guard(room_manager);
		//result = signOut(info);
		break;
	}

	case codes::logout_request: // future inpliment 
	{
		lock_guard<mutex> guard(login_manager);
		result = logout(info);
		break;
	}
	
	default:
		cerr << "MenuRequestHandler::handleRequest - not in cases" << endl;
		break;
	}
	return result;
}

string MenuRequestHandler::getType()
{
	return typeid(this).name();
}

string MenuRequestHandler::getUserName()
{
	return this->m_loggedUser.getUsername();
}

RoomData MenuRequestHandler::getCurrRoom()
{
	return m_currRoom;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo info)
{
	//getting new , un-used ID.
	unsigned int newID = static_cast<unsigned int>(rand());

	while (m_roomManager.getRoom(newID).GetName() != "")
	{
		newID = static_cast<unsigned int>(rand());
	}

	CreateRoomRequest request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);

	if (request.roomName == "")
		throw std::exception("name can not be empty");
	
	if(request.maxUsers > MAX_PLYERS_IN_GAME)
		throw std::exception("only up to 8 plyers");

	if (request.maxUsers < MIN_PLYERS_IN_GAME)
		throw std::exception("must to have at list one plyer");

	if (request.questionCount > MAX_QUESTIONS_IN_GAME)
		throw std::exception("only up to 15 questions");

	if (request.questionCount < MIN_QUESTIONS_IN_GAME)
		throw std::exception("must to have at list one questions");

	if (request.answerTimeout > MAX_TIME_FOR_QUESTIONS)
		throw std::exception("only up to 30 sec");

	if (request.answerTimeout < MIN_TIME_FOR_QUESTIONS)
		throw std::exception("must to have at list 5 sec");

	m_currRoom = RoomData(newID, request.roomName, request.maxUsers,
		request.questionCount, request.answerTimeout, false);

	{
		std::lock_guard<std::mutex> guard(out_stream);
		std::cout << "Creating room " + to_string(m_currRoom.m_roomNumber) << " , name : " + m_currRoom.m_roomName << endl;
		std::cout << m_loggedUser.getUsername() + " has joined room \"" + m_currRoom.m_roomName + "\" , id : " + to_string(m_currRoom.m_roomNumber) << endl;
	}
	

	m_roomManager.createRoom(m_currRoom, m_loggedUser); 

	CreateRoomResponse res;
	res.status = codes::createRoom_respons;
	res.id = m_currRoom.m_roomNumber;
	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) ,
		m_handlerFactory.createRoomAdminRequestHandler(m_loggedUser.getUsername() , m_roomManager.getRoom(m_currRoom.m_roomNumber)) };
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo info)
{
	auto j = json::parse(info.buffer.begin(), info.buffer.end());

	Room& room = m_roomManager.getRoom(j.at("id").get<int>());

	if (room.GetName() == "") // no such room
		throw exception("not a room id");

	

	room.AddUser(m_loggedUser);

	m_currRoom = room.getRoomData();

	{
		std::lock_guard<std::mutex> guard(out_stream);
		std::cout << m_loggedUser.getUsername() + " has joined room \"" + m_currRoom.m_roomName + "\" , id : " + to_string(m_currRoom.m_roomNumber) << endl;
	}

	JoinRoomResponse res;
	res.status = codes::joinRoom_respons;
	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) ,
		m_handlerFactory.createRoomMemberRequestHandler(m_loggedUser.getUsername(),m_currRoom.m_roomNumber) };
}

RequestResult MenuRequestHandler::getPersonalStats(RequestInfo info)
{
	auto j = json::parse(info.buffer.begin(), info.buffer.end());

	GetStatisticsResponse res;
	res.status = codes::getPersonalStats_respons;
	res.data = m_statisticsManager.getUserStatistics(m_loggedUser.getUsername());
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) ,this };
}

RequestResult MenuRequestHandler::getRooms(RequestInfo info )
{
	GetRoomsResponse res;
	vector<Room> rooms = m_roomManager.getRooms();
	for (Room room : rooms)
		if(room.IsActive() == codes::room::open)
			res.data.push_back(room.getRoomData());

	res.status = codes::getRooms_respons;
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this };
}

RequestResult MenuRequestHandler::logout(RequestInfo info)
{
	m_loginManager.logout(m_loggedUser.getUsername());

	LogoutResponse res;
	res.status = codes::logout_respons;
	lock_guard<mutex> guard(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) ,
		m_handlerFactory.createLoginRequestHandler()};
}


RequestResult MenuRequestHandler::getHighScore(RequestInfo info)
{
	GetHighScoreResponse res;
	res.status = codes::getHighScore_respons;
	res.data = m_statisticsManager.getHighScore();
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this };
}
