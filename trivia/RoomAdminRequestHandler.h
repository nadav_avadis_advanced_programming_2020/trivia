#pragma once
#include "IRequestHandler.h"
#include "StatisticsManager.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "LoginManager.h"
#include "json.hpp"
#include "Codes.h"

using namespace std;
using namespace nlohmann;

class RequestHandlerFactory;
class RoomAdminRequestHandler :public IRequestHandler
{
public:
	RoomAdminRequestHandler(Room& room, LoggedUser user,  RequestHandlerFactory& handlerFactory);
	bool isRequestRelevant(RequestInfo);
	RequestResult handleRequest(RequestInfo);

	string getType();
	int GetID();
	string getUserName();

private:
	Room& m_room;
	LoggedUser  m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult closeRoom(RequestInfo);
	RequestResult startGame(RequestInfo);
	RequestResult getRoomState(RequestInfo);

};

