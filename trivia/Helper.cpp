#include "Helper.h"
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;


// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}



int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	
	if (bytesNum == 1) // code -> 1 byte 
	{
		int number = (int)s[0];
		delete s;
		return number;
	}
	if (bytesNum == 4) // len -> 4 byte
	{		
		unsigned int number;
		unsigned int* pointer = &number;
		memcpy(pointer, ((unsigned int*)(s)), sizeof(int));
		delete s;
		return number;
	}
	return -1;
}

// recieve data from socket according byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

std::vector<char> Helper::getVectorPartFromSocket(SOCKET sc, int bytesNum)
{
	std::vector<char> vec;
	char* s = getPartFromSocket(sc, bytesNum, 0);
	int i = 0;
	while (bytesNum > i)
	{
		vec.push_back(s[i]);
		i++;
	}
	delete s;
	return vec;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}

// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	size_t t = bytesNum;
	char* data = new char[ t + 1 ];
	int res = recv(sc, data, bytesNum, flags);
	if (res == SOCKET_ERROR)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[t] = 0;
	return data;
}

// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, static_cast<int> (message.size()) , 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

void Helper::sendVecData(SOCKET sc, std::vector<char> message)
{
	const char* data = message.data();

	if (send(sc, data, static_cast<int>( message.size() ), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

