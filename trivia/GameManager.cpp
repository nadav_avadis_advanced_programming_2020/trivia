#include "GameManager.h"
#include <map>
GameManager::GameManager(IDataAccess* database)
{
	m_database = database;
}

GameManager::~GameManager()
{

}

Game& GameManager::getGame(int id)
{
	return m_games.at(id);
}

Game& GameManager::createGame(Room room)
{
	vector<Question> questions;
	map<LoggedUser, GameData> gameMap;
	Question q;

	for (int i = 0; i < static_cast<int>(room.GetNumberOfQuestion()); i++)
	{	
		do q = static_cast<SqliteDataBase*>(m_database)->getRandomQuestion();
		while (count(questions.begin(), questions.end(), q) != 0); // question found.
		questions.push_back(q);
	}
	for (LoggedUser user : room.GetAllUsers())	
		gameMap.insert(make_pair(user, GameData(questions.front())));
	

	m_games.insert({ room.GetNumber(),Game(room.GetTimePerQuestion(), room.GetNumber() , questions,gameMap) });
	return m_games.at(room.GetNumber());
}

void GameManager::deleteGame(Game game)
{
	for (auto& mapGame : this->m_games)
	{
		if (mapGame.second.getId() == game.getId())
		{
			m_games.erase(m_games.find(game.getId()));
			return;
		}
	}
}
