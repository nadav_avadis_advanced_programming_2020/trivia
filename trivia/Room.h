#pragma once
#include <iostream>
#include <vector>
#include <list>
#include "LoggedUser.h"
#include "RoomData.h"

using namespace std;

class Room
{
public:
	// constractor and destractor
	Room(unsigned int, string, unsigned int, unsigned int, unsigned int, unsigned int);
	Room(RoomData);
	Room();
	~Room();
	
	// gets
	unsigned int GetNumber();
	string GetName();
	unsigned int GetTimePerQuestion();
	unsigned int GetNumberOfQuestion();
	unsigned int GetMaxPlayers();
	unsigned int IsActive();
	RoomData& getRoomData();
	vector< LoggedUser > GetAllUsers();

	// methods
	void AddUser(LoggedUser);
	void RemoveUser(LoggedUser);

private:
	vector<LoggedUser> m_players;

	RoomData m_roomData;
};