#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "json.hpp"
#include "Codes.h"
using namespace std;
using namespace nlohmann;

class RequestHandlerFactory;
class RoomMemberRequestHandler :public IRequestHandler
{
public:
	RoomMemberRequestHandler(Room& room, LoggedUser user, RequestHandlerFactory& handlerFactory);
	bool isRequestRelevant(RequestInfo);
	RequestResult handleRequest(RequestInfo);
	string getType();
	int GetID();
	string getUserName();

private:
	Room& m_room;
	LoggedUser  m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult leaveRoom(RequestInfo);
	RequestResult getRoomState(RequestInfo);
};


