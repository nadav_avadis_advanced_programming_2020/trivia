#pragma once
#include "SqliteDataBase.h"

#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"

#include "RoomManager.h"
#include "LoginManager.h"
#include "StatisticsManager.h"
#include "GameManager.h"

class MenuRequestHandler;
class LoginRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDataAccess* database);
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(string userName, Room& room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(string userName, int id);
	GameRequestHandler* createGameRequestHandler(int id,string name);
	LoginManager&  getLoginManager();
	RoomManager& getRoomManager();
	StatisticsManager& getStatisticsManager();
	GameManager& getGameManager();
	IDataAccess* getDatabase();
	

private:
	GameManager m_gameManager;
	StatisticsManager m_statisticsManager;
	LoginManager m_loginManager;
	IDataAccess*  m_database ;
	RoomManager m_roomManager;
};

