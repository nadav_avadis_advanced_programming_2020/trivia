#pragma once
#include <map>
#include "Game.h"
#include "Room.h"
#include "SqliteDataBase.h"
#include <iostream>

class GameManager
{
public:
	GameManager(IDataAccess*);
	~GameManager();

	Game& getGame(int id);

	Game& createGame(Room);
	void deleteGame(Game);
private:
	IDataAccess* m_database;
	map<int,Game> m_games;
};