#pragma once
#pragma comment (lib, "ws2_32.lib")

#include "Server.h"
#include <iostream>
#include <exception>
#include "Communicator.h"
#include "SqliteDataBase.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	Server(IDataAccess* database);
	~Server();
	void run();
private:
	Communicator m_communicator;
	IDataAccess* m_database;
	RequestHandlerFactory m_handlerFactory;
};

