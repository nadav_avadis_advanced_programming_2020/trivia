#include "RoomManager.h"

RoomManager::RoomManager()
{
	m_rooms.insert({ 0,Room() });
}

RoomManager::~RoomManager()
{
}

void RoomManager::createRoom(RoomData data, LoggedUser user)
{
	m_rooms.insert({ data.m_roomNumber, Room(data) });
	m_rooms.at(data.m_roomNumber).AddUser(user);
	m_rooms.at(data.m_roomNumber).getRoomData().m_isAcive = codes::room::open;
}

void RoomManager::deleteRoom(unsigned int id)
{
	lock_guard<mutex> out(out_stream);
	string roomName = m_rooms.at(id).GetName();
	for (LoggedUser user : m_rooms.at(id).GetAllUsers())
	{
		cout <<  user.getUsername() << " has kicked out of room " << roomName << endl;
	}
	m_rooms.erase(id);
}

unsigned int RoomManager::getRoomState(unsigned int id)
{
	return m_rooms.at(id).IsActive();
}

vector<Room> RoomManager::getRooms()
{
	vector<Room> vec;

	for (unordered_map<unsigned int, Room>::iterator it = m_rooms.begin(); it != m_rooms.end(); it++)
		vec.push_back(it->second);

	return vec;
}

Room& RoomManager::getRoom(unsigned int id)
{
	if(m_rooms.find(id) != m_rooms.end())
		return m_rooms.at(id);
	return m_rooms.at(0);
}