#pragma once
#include <unordered_map>
#include <iostream>
#include "Codes.h"
#include "Room.h"
#include "LoggedUser.h"

using namespace std;

class RoomManager
{
public:
	RoomManager();
	~RoomManager();

	void createRoom(RoomData, LoggedUser);
	void deleteRoom(unsigned int id);
	unsigned int getRoomState(unsigned int id);
	vector<Room> getRooms();

	Room& getRoom(unsigned int id);
private:
	unordered_map<unsigned int, Room> m_rooms;
};
