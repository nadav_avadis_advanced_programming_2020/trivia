#include "RoomMemberRequestHandler.h"
#include "JsonResponsePacketSerializer.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room& room, LoggedUser user, RequestHandlerFactory& handlerFactory) :m_room(room), m_roomManager(handlerFactory.getRoomManager()), m_handlerFactory(handlerFactory)
{
	this->m_user = user;
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo info)
{
	if (info.RequestId != codes::getRoomState_request && info.RequestId != codes::leaveRoom_request)
		return false;
	return true;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult result;

	switch (info.RequestId)
	{
	case codes::leaveRoom_request:
	{
		lock_guard<mutex> guard(room_manager);
		result = this->leaveRoom(info);
		break;
	}
	case codes::getRoomState_request:
	{
		lock_guard<mutex> guard(statistics_manager);
		result = getRoomState(info);
		break;
	}
	
	default:
		cerr << "MenuRequestHandler::handleRequest - not in cases" << endl;
		break;
	}
	return result;
}

string RoomMemberRequestHandler::getType()
{
	return typeid(this).name();
}

int RoomMemberRequestHandler::GetID()
{
	return this->m_room.GetNumber();
}

string RoomMemberRequestHandler::getUserName()
{
	return this->m_user.getUsername();
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo info)
{
	m_room.RemoveUser(this->m_user);
	
	{
		lock_guard<mutex> out(out_stream);
		cout << m_user.getUsername() << " has left the room room " << m_room.GetName() << endl;
	}

	LeaveRoomResponse res;
	res.status = codes::leaveRoom_request;
	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , m_handlerFactory.createMenuRequestHandler(m_user)};
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo info)
{
	GetRoomState res;
	vector<LoggedUser> users = m_roomManager.getRoom(m_room.GetNumber()).GetAllUsers();
	if (users.empty())
	{
		res.roomState = codes::room::closed;
	}
	else // if the room is open or if he is plating. the response if accordingly
		res.roomState = m_room.IsActive();
	
	for (LoggedUser loggedUser : users)
		res.data.push_back(loggedUser.getUsername());

	lock_guard<mutex> g(handler_factory);
	switch (res.roomState)
	{
	case codes::room::closed:
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , m_handlerFactory.createMenuRequestHandler(m_user) };
	case codes::room::open:
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this };
	case codes::room::playing:
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , m_handlerFactory.createGameRequestHandler(m_room.GetNumber(),m_user.getUsername()) };
	default:
		throw exception("getRoomState - not in swich");
	}
}
