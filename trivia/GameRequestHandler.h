#pragma once
#include <iostream>
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"

#include "RoomManager.h"
#include "LoginManager.h"
#include "StatisticsManager.h"
#include "GameManager.h"

using namespace std;

class RequestHandlerFactory;
class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(RequestHandlerFactory&,int,string);
	~GameRequestHandler();

	bool isRequestRelevant(RequestInfo);
	RequestResult handleRequest(RequestInfo);
	string getType();
	string getUserName();
	
private:
	RequestResult getQuestion(RequestInfo);
	RequestResult submitAnswer(RequestInfo);
	RequestResult getGameResults(RequestInfo);
	RequestResult leaveGame(RequestInfo);

	Game& m_game;
	LoggedUser m_user;
	GameManager& m_gameManager; 
	RequestHandlerFactory& m_handlerFacroty;
};