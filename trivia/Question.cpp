#include "Question.h"

Question::Question(string question , vector<string> answers , int rightIndex)
{
	this->m_possibleAnswers = answers;
	this->m_question = question;
	this->m_rightIndex = rightIndex;
}

Question::Question()
{
	m_rightIndex = 0;
}

Question::~Question()
{

}

string Question::getQuestion()
{
	return m_question;
}

vector<string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

int Question::getCorrentAnswer()
{
	return m_rightIndex;
}

bool operator==(const Question& a, const Question& b)
{
	return a.m_question == b.m_question;
}
