#include "RoomAdminRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"

//

class RequestHandlerFactory;

RoomAdminRequestHandler::RoomAdminRequestHandler(Room& room, LoggedUser user, RequestHandlerFactory& handlerFactory): m_room(room), m_roomManager(handlerFactory.getRoomManager()), m_handlerFactory(handlerFactory)
{
	this->m_user = user;
	this->m_room = room;
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo info)
{
	if (info.RequestId != codes::closeRoom_request && info.RequestId != codes::startGame_request && info.RequestId != codes::getRoomState_request)
		return false;
	return true;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult result;

	switch (info.RequestId)
	{
	case codes::closeRoom_request:
	{
		lock_guard<mutex> guard(room_manager);
		result = this->closeRoom(info);
		break;
	}
	case codes::getRoomState_request:
	{
		lock_guard<mutex> guard(statistics_manager);
		result = getRoomState(info);
		break;
	}
	case codes::startGame_request:
	{
		lock_guard<mutex> guard(room_manager);
		result = this->startGame(info);
		break;
	}
	default:
		cerr << "MenuRequestHandler::handleRequest - not in cases" << endl;
		break;
	}
	return result;
}

string RoomAdminRequestHandler::getType()
{
	return typeid(this).name();
}

string RoomAdminRequestHandler::getUserName()
{
	return this->m_user.getUsername();
}

int RoomAdminRequestHandler::GetID()
{
	return this->m_room.GetNumber();
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo)
{
	{
		lock_guard<mutex> out(out_stream);
		cout << m_user.getUsername() << " closing room " << m_room.GetName() << endl;
	}

	this->m_roomManager.deleteRoom(m_room.GetNumber());
	CloseRoomResponse res;
	res.status = codes::closeRoom_respons;

	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , m_handlerFactory.createMenuRequestHandler(m_user)};
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo)
{
	m_roomManager.getRoom(m_room.GetNumber()).getRoomData().m_isAcive = codes::room::playing;

	m_handlerFactory.getGameManager().createGame(m_roomManager.getRoom(m_room.GetNumber()));

	StartGameResponse res;
	res.status = codes::startGame_respons;
	lock_guard<mutex> g(handler_factory);
	return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , m_handlerFactory.createGameRequestHandler(m_room.GetNumber(),m_user.getUsername()) };
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo)
{
	GetRoomState res;
	res.roomState = m_room.IsActive();
	vector<LoggedUser> users = m_roomManager.getRoom(m_room.GetNumber()).GetAllUsers();
	for (LoggedUser loggedUser : users)
		res.data.push_back(loggedUser.getUsername());
	
	lock_guard<mutex> g(handler_factory);
	switch (res.roomState)
	{
	case codes::room::closed:
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , m_handlerFactory.createMenuRequestHandler(m_user)};
	case codes::room::open:
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , this };
	case codes::room::playing:
		return RequestResult{ JsonResponsePacketSerializer::serializeResponse(res) , m_handlerFactory.createGameRequestHandler(m_room.GetNumber(),m_user.getUsername())};
	default:
		throw exception("getRoomState - not in swich");
	}	
}
