#pragma once
#include <iostream>
#include <ctime>
#include <vector>

#pragma warning (disable : 26495)

using namespace std;

struct RequestInfo
{
	int RequestId;
	time_t receivalTime;
	std::vector<char> buffer;
	RequestInfo(int id, std::vector<char> buf)
	{
		RequestId = id;
		time(&receivalTime);
		buffer = buf;
	}
}typedef RequestInfo;

class IRequestHandler;

struct RequestResult
{
	std::vector<char> response;
	IRequestHandler* newHandler;
}typedef RequestResult;


class IRequestHandler
{
public:
	virtual RequestResult handleRequest(RequestInfo) = 0;	
	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual string getType() = 0;
	virtual string getUserName() = 0;
};



