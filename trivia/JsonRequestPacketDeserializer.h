#pragma once

#include <vector>
#include <iostream>

using namespace std;

struct LoginRequest
{
	string username;
	string password;
}typedef LoginRequest;

struct SignupRequest
{
	string username;
	string password;
	string email;
}typedef SignupRequest;

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
}typedef GetPlayersInRoomRequest;

struct JoinRoomRequest
{
	unsigned int roomId;
}typedef JoinRoomRequest;

struct CreateRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}typedef CreateRoomRequest;

struct SubmitAnswerRequest
{
	unsigned int answerId;
}typedef SubmitAnswerRequest;

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(vector<char>);
	static SignupRequest deserializeSignupRequest(vector<char>);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(vector<char>);
	static JoinRoomRequest deserializeJoinRoomRequest(vector<char>);
	static CreateRoomRequest deserializeCreateRoomRequest(vector<char>);

	static SubmitAnswerRequest deserializeSubmitAnswerRequest(vector<char>);
};


