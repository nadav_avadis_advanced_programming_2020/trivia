﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.request
{
    public class CreateRoomRequest
    {
        public string roomName { get; set; }
        public int maxPlayers { get; set; }
        public int numberOfQuestions { get; set; }
        public int timePerQuestion { get; set; }
    }
}
