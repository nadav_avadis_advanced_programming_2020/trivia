﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.respons
{
    public class PlayerResults
    {
        public string username { get; set; }
        public int correctAnswerCount { get; set; }
        public int wrongAnswerCount { get; set; }
        public int averageAnswerTime { get; set; }

    }
    public class GetGameResultsResponse
    {
        public int status { get; set; }
        public List<PlayerResults> results { get; set; }
}
}
