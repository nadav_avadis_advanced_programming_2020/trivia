﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.respons
{
    public class GetPlayersInRoomsRespons
    {
        public int status { get; set; }
        public List<string> Players { get; set; }
    }
}
