﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.respons
{
    public class SubmitAnswerResponse
    {
        public int status { get; set; }
        public int answer { get; set; }
    }
}
