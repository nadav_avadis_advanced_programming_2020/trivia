﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.respons
{
    class GetQuestionResponse
    {
        public int status { get; set; }
        public string question { get; set; }
        public List<string> answers { get; set; }

    }
}
