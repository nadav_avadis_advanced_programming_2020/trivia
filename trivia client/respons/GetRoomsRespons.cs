﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.respons
{
    public class RoomData
    {
        public int id { get; set; }
        public string name { get; set; }
        public int numberOfQuestions{ get; set; }
        public int timePerQuestion{ get; set; }
        public int maxPlayers{ get; set; }
    }
    public class GetRoomsRespons
    {
        public List<RoomData> Rooms { get; set; }
    }
}
