﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.respons
{
    public class GetStatisticsResponse
    {
        public int correctAnswers { get; set; }
        public int numOfTotalAnswers { get; set; }
        public int numberOfPlayedGames { get; set; }
        public float averageAnswerTime { get; set; }
        public int points { get; set; }

        public override string ToString()
        {
            return "correctAnswers: "+ this.correctAnswers + "\nnumOfTotalAnswers: " + this.numOfTotalAnswers +"\nnumberOfPlayedGames: " + numberOfPlayedGames + "\naverageAnswerTime: " + this.averageAnswerTime + "\npoints: " + this.points;
        }
    }
}
