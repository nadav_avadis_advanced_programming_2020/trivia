﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client.respons
{
    public class HighestScore
    {
        public string name { get; set; }
        public int score { get; set; }

        public override string ToString()
        {
            return " " + name + "( " + score + " points)";
        }
    }
    public class GetHighestScoreStatisticsRespons
    {
        public List<HighestScore> highScores { get; set; }
    }
}
