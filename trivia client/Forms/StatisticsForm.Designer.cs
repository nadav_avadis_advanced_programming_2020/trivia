﻿namespace trivia_client.Forms
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StatisticsLable = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.PersonalTab = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.PersonalLable = new System.Windows.Forms.Label();
            this.TopTab = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.PersonalTab.SuspendLayout();
            this.TopTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatisticsLable
            // 
            this.StatisticsLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatisticsLable.AutoSize = true;
            this.StatisticsLable.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.StatisticsLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.StatisticsLable.Location = new System.Drawing.Point(60, 45);
            this.StatisticsLable.Name = "StatisticsLable";
            this.StatisticsLable.Size = new System.Drawing.Size(125, 31);
            this.StatisticsLable.TabIndex = 24;
            this.StatisticsLable.Text = "Statistics";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.PersonalTab);
            this.tabControl.Controls.Add(this.TopTab);
            this.tabControl.ItemSize = new System.Drawing.Size(50, 20);
            this.tabControl.Location = new System.Drawing.Point(66, 95);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(700, 300);
            this.tabControl.TabIndex = 25;
            this.tabControl.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl_Selecting);
            // 
            // PersonalTab
            // 
            this.PersonalTab.Controls.Add(this.label6);
            this.PersonalTab.Controls.Add(this.PersonalLable);
            this.PersonalTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.PersonalTab.Location = new System.Drawing.Point(4, 24);
            this.PersonalTab.Name = "PersonalTab";
            this.PersonalTab.Padding = new System.Windows.Forms.Padding(3);
            this.PersonalTab.Size = new System.Drawing.Size(692, 272);
            this.PersonalTab.TabIndex = 0;
            this.PersonalTab.Text = "Personal Info";
            this.PersonalTab.UseVisualStyleBackColor = true;
            this.PersonalTab.Click += new System.EventHandler(this.PersonalTab_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.Location = new System.Drawing.Point(27, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 31);
            this.label6.TabIndex = 1;
            this.label6.Text = "conect first";
            // 
            // PersonalLable
            // 
            this.PersonalLable.AutoSize = true;
            this.PersonalLable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PersonalLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.PersonalLable.Location = new System.Drawing.Point(3, 3);
            this.PersonalLable.Name = "PersonalLable";
            this.PersonalLable.Size = new System.Drawing.Size(0, 33);
            this.PersonalLable.TabIndex = 0;
            // 
            // TopTab
            // 
            this.TopTab.Controls.Add(this.label5);
            this.TopTab.Controls.Add(this.label4);
            this.TopTab.Controls.Add(this.label3);
            this.TopTab.Controls.Add(this.label2);
            this.TopTab.Controls.Add(this.label1);
            this.TopTab.Location = new System.Drawing.Point(4, 24);
            this.TopTab.Name = "TopTab";
            this.TopTab.Padding = new System.Windows.Forms.Padding(3);
            this.TopTab.Size = new System.Drawing.Size(692, 272);
            this.TopTab.TabIndex = 1;
            this.TopTab.Text = "Highest Soccers";
            this.TopTab.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Fifth place: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "fourth place: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Third place: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "second place: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Place: ";
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.StatisticsLable);
            this.Name = "StatisticsForm";
            this.Text = "StatisticsForm";
            this.Load += new System.EventHandler(this.StatisticsForm_Load);
            this.tabControl.ResumeLayout(false);
            this.PersonalTab.ResumeLayout(false);
            this.PersonalTab.PerformLayout();
            this.TopTab.ResumeLayout(false);
            this.TopTab.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label StatisticsLable;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage TopTab;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage PersonalTab;
        private System.Windows.Forms.Label PersonalLable;
        private System.Windows.Forms.Label label6;
    }
}