﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using Newtonsoft.Json;
using trivia_client.request;
using trivia_client.respons;

namespace trivia_client.Forms
{
    public partial class JoinForm : Form
    {
        MaterialSkinManager skinManager;
        private List<Room> roomList = new List<Room>();
        private MainForm main;
        public JoinForm(MainForm main)
        {
            
            InitializeComponent();
            this.main = main;
            skinManager = MaterialSkinManager.Instance;
            skinManager.Theme = MaterialSkinManager.Themes.DARK;
        }

        

        private void JoinForm_Load(object sender, EventArgs e)
        {
            foreach (Control btns in this.Controls)
            {
                if (btns.GetType() == typeof(Button))
                {
                    Button btn = (Button)btns;
                    btn.BackColor = MainForm.PrimaryColor;
                    btn.ForeColor = Color.White;
                    btn.FlatAppearance.BorderColor = MainForm.SecondaryColor;
                }
            }

            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor, 0.7f);

            materialListView.Items.Clear();

            GetRoomsRequest getRoomsRequest = new GetRoomsRequest();
            string respons = MainForm.soc.SocketSendReceive(getRoomsRequest);
            if (respons == "waiting for server to come up...")
            { }
            else if ((char)Codes.getRooms_respons == respons[0])
            {
                respons = respons.Substring(5, respons.Length - 5);
                GetRoomsRespons getRoomsRespons = JsonConvert.DeserializeObject<GetRoomsRespons>(respons);
                for (var i = 0; i < getRoomsRespons.Rooms.Count; i++)
                {
                    roomList.Add(new Room(getRoomsRespons.Rooms[i].id, getRoomsRespons.Rooms[i].name, getRoomsRespons.Rooms[i].maxPlayers, getRoomsRespons.Rooms[i].numberOfQuestions, getRoomsRespons.Rooms[i].timePerQuestion));
                }
            }
            else
            {
                //respons = respons.Substring(5, respons.Length - 5);
                //ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
            }

            if (roomList == null)
                return;

            if(roomList.Count > 4) // need more space for scroll bar
                materialListView.Size = new System.Drawing.Size(572, 190);
            else
                materialListView.Size = new System.Drawing.Size(555, 190);

            foreach (Room room in this.roomList)
            {
                
                ListViewItem item = new ListViewItem(room.name);
                item.SubItems.Add(room.maxPlayers.ToString());
                item.SubItems.Add(room.numberOfQuestion.ToString());
                item.SubItems.Add(room.timePerQuestion.ToString());
                materialListView.Items.Add(item);
            }
            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor, 0.7f);

            
     
        }

        private int GetRoomIndex(string name)
        {
            int count = 0;
            foreach(Room room in roomList)
            {
                if (room.name == name)
                    return count;
                count++;
            }
            return -1;
        }
        private void materialListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (materialListView.SelectedItems.Count > 0)
            {
                ListViewItem item = materialListView.SelectedItems[0];
                int index = GetRoomIndex(item.Text);

                JoinRoomRequest joinRoomRequest = new JoinRoomRequest()
                {
                    id = roomList[index].ID
                };

                string respons = MainForm.soc.SocketSendReceive(joinRoomRequest);
                if ((char)Codes.joinRoom_respons == respons[0])
                {
                    this.main.click();
                    this.main.OpenForm(new LobbyForm(this.main, this.GetType(), roomList[index].ID, roomList[index].timePerQuestion), this);
                }
                else
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                    joinLabel.Text = errorRespons.message;
                }
                
            }
        }

        private void JoinRoomLable_Click(object sender, EventArgs e)
        {

        }
        /////////////////////// Graphics /////////////////////////
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            GraphicsPath GraphPath = new GraphicsPath();
            int radius = 250;
            float r2 = radius / 2f;
            float w2 = 0;
            Rectangle Rect = new Rectangle(0, 0, this.Width, this.Height);

            GraphPath.AddLine(Rect.X + w2, Rect.Y + w2, Rect.X, Rect.Y);
            GraphPath.AddArc(Rect.X + Rect.Width - radius - w2, Rect.Y + w2, radius, radius, 270, 90);
            GraphPath.AddLine(Rect.X + Rect.Width - w2, Rect.Y + Rect.Height, Rect.X + Rect.Width, Rect.Y + Rect.Height - w2);
            GraphPath.AddLine(Rect.X + w2, Rect.Y - w2 + Rect.Height, Rect.X - w2, radius);
            GraphPath.AddLine(Rect.X + w2, Rect.Y + Rect.Height + r2 + w2 + Rect.Y, Rect.X + w2, Rect.Y + Rect.Y);
            this.Region = new Region(GraphPath);
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor, 0.7f);

            materialListView.Items.Clear();
            roomList.Clear();

            GetRoomsRequest getRoomsRequest = new GetRoomsRequest();
            string respons = MainForm.soc.SocketSendReceive(getRoomsRequest);
            if (respons == "waiting for server to come up...")
            { }
            else if ((char)Codes.getRooms_respons == respons[0])
            {
                respons = respons.Substring(5, respons.Length - 5);
                GetRoomsRespons getRoomsRespons = JsonConvert.DeserializeObject<GetRoomsRespons>(respons);
                for (var i = 0; i < getRoomsRespons.Rooms.Count; i++)
                {
                    roomList.Add(new Room(getRoomsRespons.Rooms[i].id, getRoomsRespons.Rooms[i].name, getRoomsRespons.Rooms[i].maxPlayers, getRoomsRespons.Rooms[i].numberOfQuestions, getRoomsRespons.Rooms[i].timePerQuestion));
                }
            }
            else
            {
                //respons = respons.Substring(5, respons.Length - 5);
                //ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
            }

            if (roomList == null)
                return;

            if (roomList.Count > 4) // need more space for scroll bars
                materialListView.Size = new System.Drawing.Size(572, 190);
            else
                materialListView.Size = new System.Drawing.Size(555, 190);

            foreach (Room room in this.roomList)
            {

                ListViewItem item = new ListViewItem(room.name);
                item.SubItems.Add(room.maxPlayers.ToString());
                item.SubItems.Add(room.numberOfQuestion.ToString());
                item.SubItems.Add(room.timePerQuestion.ToString());
                materialListView.Items.Add(item);
            }
        }
    }
}
