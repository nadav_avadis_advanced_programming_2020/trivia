﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using trivia_client.request;
using trivia_client.respons;

namespace trivia_client.Forms
{
    public partial class OnlinePlyersForm : Form
    {
        private List<string> playersList;
        private MainForm main;
        public OnlinePlyersForm(MainForm main)
        {
            InitializeComponent();
            this.main = main;
            playersList = new List<string>();
            GetOnlinePlyers();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            GetOnlinePlyers();
        }

        private void GetOnlinePlyers()
        {
            GetOnlinePlyersRequest getOnlinePlyersRequest = new GetOnlinePlyersRequest();
            string respons = MainForm.soc.SocketSendReceive(getOnlinePlyersRequest);
            if (respons == "waiting for server to come up...")
            { }
            else if ((char)Codes.getOlinePlyers_respons == respons[0])
            {
                respons = respons.Substring(5, respons.Length - 5);
                GetOnlinePlyersRespons getOnlinePlyersRespons = JsonConvert.DeserializeObject<GetOnlinePlyersRespons>(respons);
                this.onlinePlyersListView.Items.Clear();

                for (var i = 0; i < getOnlinePlyersRespons.plyers.Count; i++)
                {
                    this.onlinePlyersListView.Items.Add(new ListViewItem(getOnlinePlyersRespons.plyers[i]));
                }
            }
            else
            {
                //respons = respons.Substring(5, respons.Length - 5);
                //ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
            }
        }

        private void OnlinePlyersForm_Load(object sender, EventArgs e)
        {
            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor, 0.7f);

            foreach (Control btns in this.Controls)
            {
                if (btns.GetType() == typeof(Button))
                {
                    Button btn = (Button)btns;
                    btn.BackColor = MainForm.PrimaryColor;
                    btn.ForeColor = Color.White;
                    btn.FlatAppearance.BorderColor = MainForm.SecondaryColor;
                }
            }
            
        }
    }
}
