﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trivia_client.Forms
{
    public partial class ExitForm : Form
    {
        private MainForm main;
        public ExitForm(MainForm main)
        {
            InitializeComponent();
            this.main = main;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            GraphicsPath GraphPath = new GraphicsPath();
            int radius = 250;
            float r2 = radius / 2f;
            float w2 = 0;
            Rectangle Rect = new Rectangle(0, 0, this.Width, this.Height);

            GraphPath.AddLine(Rect.X + w2, Rect.Y + w2, Rect.X, Rect.Y);
            GraphPath.AddArc(Rect.X + Rect.Width - radius - w2, Rect.Y + w2, radius, radius, 270, 90);
            GraphPath.AddLine(Rect.X + Rect.Width - w2, Rect.Y + Rect.Height, Rect.X + Rect.Width, Rect.Y + Rect.Height - w2);
            GraphPath.AddLine(Rect.X + w2, Rect.Y - w2 + Rect.Height, Rect.X - w2, radius);
            GraphPath.AddLine(Rect.X + w2, Rect.Y + Rect.Height + r2 + w2 + Rect.Y, Rect.X + w2, Rect.Y + Rect.Y);
            this.Region = new Region(GraphPath);
        }

        private void ExitForm_Load(object sender, EventArgs e)
        {
            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor, 0.7f);
            foreach (Control btns in this.Controls)
            {
                if (btns.GetType() == typeof(Button))
                {
                    Button btn = (Button)btns;
                    btn.BackColor = MainForm.PrimaryColor;
                    btn.ForeColor = Color.White;
                    btn.FlatAppearance.BorderColor = MainForm.SecondaryColor;
                }
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {

        }

        private void exitButton_Click_1(object sender, EventArgs e)
        {
            this.main.click();
            MainForm.soc.close();
            Environment.Exit(0);
        }
    }
}
