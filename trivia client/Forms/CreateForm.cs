﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using trivia_client.request;
using trivia_client.respons;

namespace trivia_client.Forms
{
    public partial class CreateForm : Form
    {
        private MainForm main;
        public CreateForm(MainForm main)
        {
            InitializeComponent();
            this.main = main;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            GraphicsPath GraphPath = new GraphicsPath();
            int radius = 250;
            float r2 = radius / 2f;
            float w2 = 0;
            Rectangle Rect = new Rectangle(0, 0, this.Width, this.Height);

            GraphPath.AddLine(Rect.X + w2, Rect.Y + w2, Rect.X, Rect.Y);
            GraphPath.AddArc(Rect.X + Rect.Width - radius - w2, Rect.Y + w2, radius, radius, 270, 90);
            GraphPath.AddLine(Rect.X + Rect.Width - w2, Rect.Y + Rect.Height, Rect.X + Rect.Width, Rect.Y + Rect.Height - w2);
            GraphPath.AddLine(Rect.X + w2, Rect.Y - w2 + Rect.Height, Rect.X - w2, radius);
            GraphPath.AddLine(Rect.X + w2, Rect.Y + Rect.Height + r2 + w2 + Rect.Y, Rect.X + w2, Rect.Y + Rect.Y);
            this.Region = new Region(GraphPath);
        }

        private void CreateForm_Load(object sender, EventArgs e)
        {
            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor  , 0.7f);
            foreach (Control btns in this.Controls)
            {
                if (btns.GetType() == typeof(Button))
                {
                    Button btn = (Button)btns;
                    btn.BackColor = MainForm.PrimaryColor;
                    btn.ForeColor = Color.White;
                    btn.FlatAppearance.BorderColor = MainForm.SecondaryColor;
                }
            }
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            this.main.click();
            if (this.NameTextBox.Text == "")
            {
                createInfoLable.Text = "name not empty";
            }
            else if (!this.MaxPlayerTextBox.Text.All(char.IsDigit) || this.MaxPlayerTextBox.Text == "")
            {
                createInfoLable.Text = "in MaxPlayer only numbers";
            }
            else if (!this.NumberOfQuestionsTextBox.Text.All(char.IsDigit) || this.NumberOfQuestionsTextBox.Text == "")
            {
                createInfoLable.Text = "in NumberOfQuestions only numbers";
            }
            else if (!this.TimePerQuestionTextBox.Text.All(char.IsDigit) || this.TimePerQuestionTextBox.Text == "")
            {
                createInfoLable.Text = "in TimePerQuestion only numbers";
            }
            else
            {
                CreateRoomRequest createRoomRequest = new CreateRoomRequest()
                {
                    roomName = this.NameTextBox.Text,
                    maxPlayers = int.Parse(this.MaxPlayerTextBox.Text),
                    numberOfQuestions = int.Parse(this.NumberOfQuestionsTextBox.Text),
                    timePerQuestion = int.Parse(this.TimePerQuestionTextBox.Text)
                };
                string respons = MainForm.soc.SocketSendReceive(createRoomRequest);
                if (respons == "waiting for server to come up...")
                    createInfoLable.Text = respons;
                else if ((char)Codes.createRoom_respons == respons[0])
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    CreateRoomRespons createRoomRespons = JsonConvert.DeserializeObject<CreateRoomRespons>(respons);
                    this.main.OpenForm(new LobbyForm(this.main, this.GetType(), createRoomRespons.id, Int32.Parse(this.TimePerQuestionTextBox.Text)), this);
                }
                else
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                    createInfoLable.Text = errorRespons.message;
                }


                
            }
            
        }

        
    }
}
