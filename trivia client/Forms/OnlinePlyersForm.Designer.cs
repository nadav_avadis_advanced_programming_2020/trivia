﻿namespace trivia_client.Forms
{
    partial class OnlinePlyersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.onlinePlyersListView = new MaterialSkin.Controls.MaterialListView();
            this.Players = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.refreshButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // onlinePlyersListView
            // 
            this.onlinePlyersListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.onlinePlyersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Players});
            this.onlinePlyersListView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.onlinePlyersListView.Depth = 0;
            this.onlinePlyersListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.onlinePlyersListView.FullRowSelect = true;
            this.onlinePlyersListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.onlinePlyersListView.HideSelection = false;
            this.onlinePlyersListView.Location = new System.Drawing.Point(224, 12);
            this.onlinePlyersListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.onlinePlyersListView.MouseState = MaterialSkin.MouseState.OUT;
            this.onlinePlyersListView.Name = "onlinePlyersListView";
            this.onlinePlyersListView.OwnerDraw = true;
            this.onlinePlyersListView.Size = new System.Drawing.Size(350, 480);
            this.onlinePlyersListView.TabIndex = 5;
            this.onlinePlyersListView.UseCompatibleStateImageBehavior = false;
            this.onlinePlyersListView.View = System.Windows.Forms.View.Details;
            // 
            // Players
            // 
            this.Players.Text = "Players";
            this.Players.Width = 350;
            // 
            // refreshButton
            // 
            this.refreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.refreshButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refreshButton.FlatAppearance.BorderSize = 0;
            this.refreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refreshButton.Location = new System.Drawing.Point(657, 52);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(88, 32);
            this.refreshButton.TabIndex = 6;
            this.refreshButton.Text = "refresh";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // OnlinePlyersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.onlinePlyersListView);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "OnlinePlyersForm";
            this.Text = "OnlinePlyers";
            this.Load += new System.EventHandler(this.OnlinePlyersForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialListView onlinePlyersListView;
        public System.Windows.Forms.ColumnHeader Players;
        private System.Windows.Forms.Button refreshButton;
    }
}