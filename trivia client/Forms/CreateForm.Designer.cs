﻿using System.Drawing;

namespace trivia_client.Forms
{
    partial class CreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumberOfQuestionsTextBox = new System.Windows.Forms.TextBox();
            this.NumberOfQuestionsLable = new System.Windows.Forms.Label();
            this.CreateRoomLable = new System.Windows.Forms.Label();
            this.createButton = new System.Windows.Forms.Button();
            this.MaxPlayerTextBox = new System.Windows.Forms.TextBox();
            this.MaxPlayerLable = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.RoomNameLable = new System.Windows.Forms.Label();
            this.TimePerQuestionTextBox = new System.Windows.Forms.TextBox();
            this.TimePerQuestionLable = new System.Windows.Forms.Label();
            this.createInfoLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // NumberOfQuestionsTextBox
            // 
            this.NumberOfQuestionsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumberOfQuestionsTextBox.Location = new System.Drawing.Point(348, 241);
            this.NumberOfQuestionsTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.NumberOfQuestionsTextBox.Name = "NumberOfQuestionsTextBox";
            this.NumberOfQuestionsTextBox.Size = new System.Drawing.Size(125, 20);
            this.NumberOfQuestionsTextBox.TabIndex = 25;
            // 
            // NumberOfQuestionsLable
            // 
            this.NumberOfQuestionsLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumberOfQuestionsLable.AutoSize = true;
            this.NumberOfQuestionsLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.NumberOfQuestionsLable.Location = new System.Drawing.Point(153, 241);
            this.NumberOfQuestionsLable.Name = "NumberOfQuestionsLable";
            this.NumberOfQuestionsLable.Size = new System.Drawing.Size(180, 20);
            this.NumberOfQuestionsLable.TabIndex = 24;
            this.NumberOfQuestionsLable.Text = "Number of questions:";
            // 
            // CreateRoomLable
            // 
            this.CreateRoomLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateRoomLable.AutoSize = true;
            this.CreateRoomLable.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.CreateRoomLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CreateRoomLable.Location = new System.Drawing.Point(60, 45);
            this.CreateRoomLable.Name = "CreateRoomLable";
            this.CreateRoomLable.Size = new System.Drawing.Size(175, 31);
            this.CreateRoomLable.TabIndex = 23;
            this.CreateRoomLable.Text = "Create Room";
            // 
            // createButton
            // 
            this.createButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.createButton.Location = new System.Drawing.Point(490, 363);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(157, 49);
            this.createButton.TabIndex = 28;
            this.createButton.Text = "Create Room";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // MaxPlayerTextBox
            // 
            this.MaxPlayerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MaxPlayerTextBox.Location = new System.Drawing.Point(348, 183);
            this.MaxPlayerTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.MaxPlayerTextBox.Name = "MaxPlayerTextBox";
            this.MaxPlayerTextBox.Size = new System.Drawing.Size(125, 20);
            this.MaxPlayerTextBox.TabIndex = 21;
            // 
            // MaxPlayerLable
            // 
            this.MaxPlayerLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MaxPlayerLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.MaxPlayerLable.Location = new System.Drawing.Point(153, 183);
            this.MaxPlayerLable.Name = "MaxPlayerLable";
            this.MaxPlayerLable.Size = new System.Drawing.Size(157, 20);
            this.MaxPlayerLable.TabIndex = 20;
            this.MaxPlayerLable.Text = "Max Players:";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.NameTextBox.Location = new System.Drawing.Point(348, 128);
            this.NameTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(125, 20);
            this.NameTextBox.TabIndex = 19;
            // 
            // RoomNameLable
            // 
            this.RoomNameLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RoomNameLable.AutoSize = true;
            this.RoomNameLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RoomNameLable.Location = new System.Drawing.Point(153, 128);
            this.RoomNameLable.Name = "RoomNameLable";
            this.RoomNameLable.Size = new System.Drawing.Size(112, 20);
            this.RoomNameLable.TabIndex = 18;
            this.RoomNameLable.Text = "Room Name:";
            // 
            // TimePerQuestionTextBox
            // 
            this.TimePerQuestionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TimePerQuestionTextBox.Location = new System.Drawing.Point(348, 296);
            this.TimePerQuestionTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.TimePerQuestionTextBox.Name = "TimePerQuestionTextBox";
            this.TimePerQuestionTextBox.Size = new System.Drawing.Size(125, 20);
            this.TimePerQuestionTextBox.TabIndex = 27;
            // 
            // TimePerQuestionLable
            // 
            this.TimePerQuestionLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TimePerQuestionLable.AutoSize = true;
            this.TimePerQuestionLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.TimePerQuestionLable.Location = new System.Drawing.Point(153, 296);
            this.TimePerQuestionLable.Name = "TimePerQuestionLable";
            this.TimePerQuestionLable.Size = new System.Drawing.Size(157, 20);
            this.TimePerQuestionLable.TabIndex = 26;
            this.TimePerQuestionLable.Text = "Time per question:";
            // 
            // createInfoLable
            // 
            this.createInfoLable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createInfoLable.AutoSize = true;
            this.createInfoLable.Location = new System.Drawing.Point(-1, 438);
            this.createInfoLable.Name = "createInfoLable";
            this.createInfoLable.Size = new System.Drawing.Size(0, 13);
            this.createInfoLable.TabIndex = 28;
            // 
            // CreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.createInfoLable);
            this.Controls.Add(this.TimePerQuestionTextBox);
            this.Controls.Add(this.TimePerQuestionLable);
            this.Controls.Add(this.NumberOfQuestionsTextBox);
            this.Controls.Add(this.NumberOfQuestionsLable);
            this.Controls.Add(this.CreateRoomLable);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.MaxPlayerTextBox);
            this.Controls.Add(this.MaxPlayerLable);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.RoomNameLable);
            this.Name = "CreateForm";
            this.Text = "CreateForm";
            this.Load += new System.EventHandler(this.CreateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NumberOfQuestionsTextBox;
        private System.Windows.Forms.Label NumberOfQuestionsLable;
        private System.Windows.Forms.Label CreateRoomLable;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.TextBox MaxPlayerTextBox;
        private System.Windows.Forms.Label MaxPlayerLable;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label RoomNameLable;
        private System.Windows.Forms.TextBox TimePerQuestionTextBox;
        private System.Windows.Forms.Label TimePerQuestionLable;
        private System.Windows.Forms.Label createInfoLable;
    }
}