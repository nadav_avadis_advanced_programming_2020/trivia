﻿namespace trivia_client.Forms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginUsernameLable = new System.Windows.Forms.Label();
            this.LoginUsernameTextBox = new System.Windows.Forms.TextBox();
            this.LoginPasswordTextBox = new System.Windows.Forms.TextBox();
            this.loginPasswordLable = new System.Windows.Forms.Label();
            this.loginButton = new System.Windows.Forms.Button();
            this.loginInfoLable = new System.Windows.Forms.Label();
            this.LoginLable = new System.Windows.Forms.Label();
            this.SignupLable = new System.Windows.Forms.Label();
            this.signupButton = new System.Windows.Forms.Button();
            this.signupPasswordTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.signupUsernameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.signupEmailTextBox = new System.Windows.Forms.TextBox();
            this.EmailLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loginUsernameLable
            // 
            this.loginUsernameLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loginUsernameLable.AutoSize = true;
            this.loginUsernameLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.loginUsernameLable.Location = new System.Drawing.Point(37, 110);
            this.loginUsernameLable.Name = "loginUsernameLable";
            this.loginUsernameLable.Size = new System.Drawing.Size(96, 20);
            this.loginUsernameLable.TabIndex = 0;
            this.loginUsernameLable.Text = "Username:";
            // 
            // LoginUsernameTextBox
            // 
            this.LoginUsernameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginUsernameTextBox.Location = new System.Drawing.Point(176, 110);
            this.LoginUsernameTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.LoginUsernameTextBox.Name = "LoginUsernameTextBox";
            this.LoginUsernameTextBox.Size = new System.Drawing.Size(125, 20);
            this.LoginUsernameTextBox.TabIndex = 1;
            this.LoginUsernameTextBox.TextChanged += new System.EventHandler(this.LoginUsernameTextBox_TextChanged);
            // 
            // LoginPasswordTextBox
            // 
            this.LoginPasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginPasswordTextBox.Location = new System.Drawing.Point(176, 169);
            this.LoginPasswordTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.LoginPasswordTextBox.Name = "LoginPasswordTextBox";
            this.LoginPasswordTextBox.Size = new System.Drawing.Size(125, 20);
            this.LoginPasswordTextBox.TabIndex = 3;
            this.LoginPasswordTextBox.TextChanged += new System.EventHandler(this.LoginPasswordTextBox_TextChanged);
            // 
            // loginPasswordLable
            // 
            this.loginPasswordLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loginPasswordLable.AutoSize = true;
            this.loginPasswordLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.loginPasswordLable.Location = new System.Drawing.Point(42, 169);
            this.loginPasswordLable.Name = "loginPasswordLable";
            this.loginPasswordLable.Size = new System.Drawing.Size(91, 20);
            this.loginPasswordLable.TabIndex = 2;
            this.loginPasswordLable.Text = "Password:";
            // 
            // loginButton
            // 
            this.loginButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loginButton.FlatAppearance.BorderSize = 0;
            this.loginButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.loginButton.Location = new System.Drawing.Point(176, 316);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(136, 49);
            this.loginButton.TabIndex = 6;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // loginInfoLable
            // 
            this.loginInfoLable.AutoSize = true;
            this.loginInfoLable.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.loginInfoLable.Location = new System.Drawing.Point(0, 437);
            this.loginInfoLable.Name = "loginInfoLable";
            this.loginInfoLable.Size = new System.Drawing.Size(0, 13);
            this.loginInfoLable.TabIndex = 8;
            // 
            // LoginLable
            // 
            this.LoginLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginLable.AutoSize = true;
            this.LoginLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LoginLable.Location = new System.Drawing.Point(60, 45);
            this.LoginLable.Name = "LoginLable";
            this.LoginLable.Size = new System.Drawing.Size(80, 31);
            this.LoginLable.TabIndex = 9;
            this.LoginLable.Text = "Login";
            // 
            // SignupLable
            // 
            this.SignupLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SignupLable.AutoSize = true;
            this.SignupLable.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.SignupLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.SignupLable.Location = new System.Drawing.Point(470, 45);
            this.SignupLable.Name = "SignupLable";
            this.SignupLable.Size = new System.Drawing.Size(98, 31);
            this.SignupLable.TabIndex = 15;
            this.SignupLable.Text = "Signup";
            this.SignupLable.Click += new System.EventHandler(this.SignupLable_Click);
            // 
            // signupButton
            // 
            this.signupButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.signupButton.FlatAppearance.BorderSize = 0;
            this.signupButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.signupButton.Location = new System.Drawing.Point(613, 316);
            this.signupButton.Name = "signupButton";
            this.signupButton.Size = new System.Drawing.Size(136, 49);
            this.signupButton.TabIndex = 18;
            this.signupButton.Text = "Signup";
            this.signupButton.UseVisualStyleBackColor = true;
            this.signupButton.Click += new System.EventHandler(this.signupButton_Click);
            // 
            // signupPasswordTextBox
            // 
            this.signupPasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.signupPasswordTextBox.Location = new System.Drawing.Point(613, 169);
            this.signupPasswordTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.signupPasswordTextBox.Name = "signupPasswordTextBox";
            this.signupPasswordTextBox.Size = new System.Drawing.Size(125, 20);
            this.signupPasswordTextBox.TabIndex = 13;
            this.signupPasswordTextBox.TextChanged += new System.EventHandler(this.signupPasswordTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(479, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "Password:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // signupUsernameTextBox
            // 
            this.signupUsernameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.signupUsernameTextBox.Location = new System.Drawing.Point(613, 110);
            this.signupUsernameTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.signupUsernameTextBox.Name = "signupUsernameTextBox";
            this.signupUsernameTextBox.Size = new System.Drawing.Size(125, 20);
            this.signupUsernameTextBox.TabIndex = 11;
            this.signupUsernameTextBox.TextChanged += new System.EventHandler(this.signupUsernameTextBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(474, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Username:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // signupEmailTextBox
            // 
            this.signupEmailTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.signupEmailTextBox.Location = new System.Drawing.Point(613, 226);
            this.signupEmailTextBox.MaximumSize = new System.Drawing.Size(270, 125);
            this.signupEmailTextBox.Name = "signupEmailTextBox";
            this.signupEmailTextBox.Size = new System.Drawing.Size(125, 20);
            this.signupEmailTextBox.TabIndex = 17;
            this.signupEmailTextBox.TextChanged += new System.EventHandler(this.signupEmailTextBox_TextChanged);
            // 
            // EmailLable
            // 
            this.EmailLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EmailLable.AutoSize = true;
            this.EmailLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.EmailLable.Location = new System.Drawing.Point(479, 226);
            this.EmailLable.Name = "EmailLable";
            this.EmailLable.Size = new System.Drawing.Size(63, 20);
            this.EmailLable.TabIndex = 16;
            this.EmailLable.Text = "Email :";
            this.EmailLable.Click += new System.EventHandler(this.EmailLable_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.signupEmailTextBox);
            this.Controls.Add(this.EmailLable);
            this.Controls.Add(this.SignupLable);
            this.Controls.Add(this.signupButton);
            this.Controls.Add(this.signupPasswordTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.signupUsernameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LoginLable);
            this.Controls.Add(this.loginInfoLable);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.LoginPasswordTextBox);
            this.Controls.Add(this.loginPasswordLable);
            this.Controls.Add(this.LoginUsernameTextBox);
            this.Controls.Add(this.loginUsernameLable);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "LoginForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label loginUsernameLable;
        private System.Windows.Forms.TextBox LoginUsernameTextBox;
        private System.Windows.Forms.TextBox LoginPasswordTextBox;
        private System.Windows.Forms.Label loginPasswordLable;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.Label loginInfoLable;
        private System.Windows.Forms.Label LoginLable;
        private System.Windows.Forms.Label SignupLable;
        private System.Windows.Forms.Button signupButton;
        private System.Windows.Forms.TextBox signupPasswordTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox signupUsernameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox signupEmailTextBox;
        private System.Windows.Forms.Label EmailLable;
    }
}