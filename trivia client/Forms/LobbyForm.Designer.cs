﻿namespace trivia_client.Forms
{
    partial class LobbyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.materialListView = new MaterialSkin.Controls.MaterialListView();
            this.Players = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.backButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.backButton.Location = new System.Drawing.Point(740, 475);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(216, 62);
            this.backButton.TabIndex = 2;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.startButton.Location = new System.Drawing.Point(740, 57);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(216, 62);
            this.startButton.TabIndex = 3;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // materialListView
            // 
            this.materialListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Players});
            this.materialListView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.materialListView.Depth = 0;
            this.materialListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.materialListView.FullRowSelect = true;
            this.materialListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.materialListView.HideSelection = false;
            this.materialListView.Location = new System.Drawing.Point(232, 57);
            this.materialListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialListView.MouseState = MaterialSkin.MouseState.OUT;
            this.materialListView.Name = "materialListView";
            this.materialListView.OwnerDraw = true;
            this.materialListView.Size = new System.Drawing.Size(350, 480);
            this.materialListView.TabIndex = 4;
            this.materialListView.UseCompatibleStateImageBehavior = false;
            this.materialListView.View = System.Windows.Forms.View.Details;
            this.materialListView.SelectedIndexChanged += new System.EventHandler(this.materialListView_SelectedIndexChanged);
            // 
            // Players
            // 
            this.Players.Text = "Players";
            this.Players.Width = 350;
            // 
            // LobbyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.materialListView);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.backButton);
            this.Name = "LobbyForm";
            this.Text = "LobbyForm";
            this.Load += new System.EventHandler(this.LobbyForm_Load);
            this.Leave += new System.EventHandler(this.LobbyForm_Leave);
            this.ResumeLayout(false);

        }

        



        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button startButton;
        private MaterialSkin.Controls.MaterialListView materialListView;
        public System.Windows.Forms.ColumnHeader Players;
    }
}