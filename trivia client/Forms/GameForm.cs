﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Threading.Tasks;
using System.Windows.Forms;
using trivia_client.request;
using trivia_client.respons;
using System.Runtime.InteropServices;
using System.Media;

namespace trivia_client.Forms
{
    public partial class GameForm : Form
    {
        static Mutex m = new Mutex();
        System.Timers.Timer timer = new System.Timers.Timer();
        private MainForm main;
        private int time;
        private int Anser;
        public bool gameOver;
        private bool canAnser;
        private bool admin;
        int corectAnser;
        private Thread myThread;
        private Thread doGame;
        private SoundPlayer sound_player = new SoundPlayer(Properties.Resources.gameMusic);
        public GameForm(MainForm main, int time, bool admin)
        {
            InitializeComponent();
            this.main = main;
            this.time = time;
            this.gameOver = false;
            this.corectAnser = 0;
            this.admin = admin;

            this.AButton.firstColor = MainForm.ChangeColorBrightness(Color.Red, 0.3f);
            this.AButton.secondColor = MainForm.ChangeColorBrightness(Color.Red, -0.3f);
            this.AButton.text = "A";

            this.BButton.firstColor = MainForm.ChangeColorBrightness(Color.Blue, 0.3f);
            this.BButton.secondColor = MainForm.ChangeColorBrightness(Color.Blue, -0.3f);
            this.BButton.text = "B";

            this.CButton.firstColor = MainForm.ChangeColorBrightness(Color.Green, 0.4f);
            this.CButton.secondColor = MainForm.ChangeColorBrightness(Color.Green, -0.2f);
            this.CButton.text = "C";

            this.DButton.firstColor = MainForm.ChangeColorBrightness(Color.Yellow, 0.2f);
            this.DButton.secondColor = MainForm.ChangeColorBrightness(Color.Yellow, -2.5f);
            this.DButton.text = "D";

            this.backButton.BackColor = Color.White;
        }

        private void GameForm_Shown(object sender, System.EventArgs e)
        {
            doGame = new Thread(() =>
            {
                myThread = new Thread(DoBarUpdates);
                myThread.IsBackground = true;
                GetQuestion();
                this.Invoke((MethodInvoker)(() => this.Refresh()));
                while (!gameOver)
                {
                    if (myThread.IsAlive)
                    {
                        myThread.Abort();
                    }
                    myThread = new Thread(DoBarUpdates);
                    myThread.IsBackground = true;
                    this.progressBar.Invoke((MethodInvoker)(() =>
                    {
                        progressBar.Value = 0;
                        progressBar.Maximum = time * 2;
                    }));


                    int start = DateTime.Now.Second;
                    myThread.Start();


                    while (Anser == 0 && DateTime.Now.Second - start < time) { }

                    if (Anser != 0)
                    {
                        SubmitAnswer();
                        switch (corectAnser)
                        {
                            case 1:
                                this.BButton.Invoke((MethodInvoker)(() =>
                                {
                                    this.AButton.Visible = true;
                                }));
                                break;
                            case 2:
                                this.BButton.Invoke((MethodInvoker)(() =>
                                {
                                    this.BButton.Visible = true;
                                }));
                                break;
                            case 3:
                                this.BButton.Invoke((MethodInvoker)(() =>
                                {
                                    this.CButton.Visible = true;
                                }));
                                break;
                            case 4:
                                this.BButton.Invoke((MethodInvoker)(() =>
                                {
                                    this.DButton.Visible = true;
                                }));
                                break;
                            default:
                                break;
                        }
                        this.Invoke((MethodInvoker)(() => this.Refresh()));
                    }
                    Thread.Sleep(1000);
                    GetQuestion();
                    this.Invoke((MethodInvoker)(() => this.Refresh()));
                }
                GetGameResults();
                sound_player.Stop();
                if(myThread.IsAlive) myThread.Abort();
                this.main.click();
                this.main.OpenForm(new StatisticsForm(), this);
                
            });
            doGame.IsBackground = true;
            doGame.Start();
        }

        private void GetGameResults()
        {
            GetGameResultsRequest getGameResultsRequest = new GetGameResultsRequest();

            while (true)
            {
                string respons = MainForm.soc.SocketSendReceive(getGameResultsRequest);
                if ((char)Codes.getGameResult_respons == respons[0])
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    GetGameResultsResponse getGameResultsResponse = JsonConvert.DeserializeObject<GetGameResultsResponse>(respons);

                    string output = "";
                    Pair<int,int>[] results = new Pair<int, int>[getGameResultsResponse.results.Count];
                    int points;

                    for (int i = 0; i < getGameResultsResponse.results.Count; i++)
                    {
                        if(getGameResultsResponse.results[i].wrongAnswerCount == 0)
                            points = Convert.ToInt32( (getGameResultsResponse.results[i].correctAnswerCount / 0.75)
                                * (30 - getGameResultsResponse.results[i].averageAnswerTime));
                        else
                            points = Convert.ToInt32((getGameResultsResponse.results[i].correctAnswerCount / getGameResultsResponse.results[i].wrongAnswerCount)
                               * (30 - getGameResultsResponse.results[i].averageAnswerTime));

                        Console.WriteLine(getGameResultsResponse.results[i].username + " " + points);

                        results[i] = (new Pair<int, int>(points, i));
                    }
                    Array.Sort(results, new PairComparer());
                    for (int i = results.Length - 1 ; i >= 0 ; i--)
                    {
                        output += (results.Length - i).ToString() + ": " + getGameResultsResponse.results[results[i].Second].username + "\n"
                                 + "avg time: " + getGameResultsResponse.results[results[i].Second].averageAnswerTime.ToString() + " "
                                 + ", right: "  + getGameResultsResponse.results[results[i].Second].correctAnswerCount.ToString() + " "
                                 + ", wrong: "  + getGameResultsResponse.results[results[i].Second].wrongAnswerCount.ToString() + "\n";
                    }
                        
                    MessageBox.Show(output, "Trivia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;

                }
            }
        }

        private void DoBarUpdates()
        {
            for (int i = 0; i < progressBar.Maximum; i++)
            {
                progressBar.Invoke((MethodInvoker)(() => {

                    progressBar.Value += 1;
                    progressBar.Refresh();
                }));

                Thread.Sleep(1000 / (progressBar.Maximum / time));
            }
        }



        private void SubmitAnswer()
        {
            SubmitAnswerRequest submitAnswerRequest = new SubmitAnswerRequest()
            {
                answerId = Anser
            };

            this.AButton.Invoke((MethodInvoker)(() => this.AButton.Visible = false));
            this.BButton.Invoke((MethodInvoker)(() => this.BButton.Visible = false));
            this.CButton.Invoke((MethodInvoker)(() => this.CButton.Visible = false));
            this.DButton.Invoke((MethodInvoker)(() => this.DButton.Visible = false));

            string respons = MainForm.soc.SocketSendReceive(submitAnswerRequest);

            if ((char)Codes.submitAnswer_respons == respons[0])
            {
                respons = respons.Substring(5, respons.Length - 5);
                SubmitAnswerResponse submitAnswerResponse = JsonConvert.DeserializeObject<SubmitAnswerResponse>(respons);

                corectAnser = submitAnswerResponse.answer;
            }
            else
            {
                respons = respons.Substring(5, respons.Length - 5);
                ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            sound_player.Stop();
            if (myThread != null && myThread.IsAlive) myThread.Abort();
            if(doGame != null && doGame.IsAlive) doGame.Abort();
            LeaveGameRequest leaveGameRequest = new LeaveGameRequest();
            string respons = MainForm.soc.SocketSendReceive(leaveGameRequest);
            if ((char)Codes.leaveGame_respons == respons[0])
            {
                this.main.click();
                if(admin)
                    this.main.OpenForm(new CreateForm(this.main), this);
                else
                    this.main.OpenForm(new JoinForm(this.main), this);
            }

            
        }

        private void AButton_Click(object sender, EventArgs e)
        {
            if (canAnser)
            {
                this.Anser = 1;
                canAnser = false;
            }
        }

        private void changeColor_Load(object sender, EventArgs e)
        {
        }
        private void GetQuestion()
        {
            while (!this.IsHandleCreated) { }

            GetQuestionRequest getQuestionRequest = new GetQuestionRequest();
            while (true)
            {
                string respons = MainForm.soc.SocketSendReceive(getQuestionRequest);
                if ((char)Codes.getQuestion_respons == respons[0])
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    GetQuestionResponse getQuestionResponse = JsonConvert.DeserializeObject<GetQuestionResponse>(respons);

                    Anser = 0;

                    this.QuestionLable.Invoke((MethodInvoker)(() => this.QuestionLable.Text = getQuestionResponse.question));

                    this.AButton.Invoke((MethodInvoker)(() => this.AButton.Visible = true));
                    this.BButton.Invoke((MethodInvoker)(() => this.BButton.Visible = true));
                    this.CButton.Invoke((MethodInvoker)(() => this.CButton.Visible = true));
                    this.DButton.Invoke((MethodInvoker)(() => this.DButton.Visible = true));

                    string answer1 = getQuestionResponse.answers[0], answer2 = getQuestionResponse.answers[1], answer3 = getQuestionResponse.answers[2], answer4 = getQuestionResponse.answers[3];
                    this.AButton.Invoke((MethodInvoker)(() => this.AButton.text = answer1));
                    this.BButton.Invoke((MethodInvoker)(() => this.BButton.text = answer2));
                    this.CButton.Invoke((MethodInvoker)(() => this.CButton.text = answer3));
                    this.DButton.Invoke((MethodInvoker)(() => this.DButton.text = answer4));


                    canAnser = true;
                    break;

                }
                else
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                    if (errorRespons.message == "game over")
                    {
                        gameOver = true;
                        break;
                    }
                }
            }

        }

        private void BButton_Click(object sender, EventArgs e)
        {
            if (canAnser)
            {
                this.Anser = 2;
                canAnser = false;
            }
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            if (canAnser)
            {
                this.Anser = 3;
                canAnser = false;
            }
        }

        private void DButton_Click(object sender, EventArgs e)
        {
            if (canAnser)
            {
                this.Anser = 4;
                canAnser = false;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GameForm_Load(object sender, EventArgs e)
        {
            sound_player.PlayLooping();
        }

        private void GameForm_Leave(object sender, EventArgs e)
        {
            sound_player.Stop();
        }
    }
}