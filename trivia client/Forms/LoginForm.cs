﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using trivia_client.request;
using trivia_client.respons;

namespace trivia_client.Forms
{
    public partial class LoginForm : Form
    {
        private MainForm main;
        public LoginForm(MainForm main)
        {
            this.main = main;
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            // DO LOGIN REQUEST
            this.main.click();
            string respons;
            if (this.main.isLoggedIn)
            {
                LogoutRequest request = new LogoutRequest();
                respons = MainForm.soc.SocketSendReceive(request);
            }
            else
            {
                LoginRequest request = new LoginRequest()
                {
                    username = this.LoginUsernameTextBox.Text,
                    password = this.LoginPasswordTextBox.Text,
                };
                respons = MainForm.soc.SocketSendReceive(request);
            }
            if (respons == "waiting for server to come up...")
                loginInfoLable.Text = respons;
            else if ((char)Codes.login_respons == respons[0])
            {
                main.username = this.LoginUsernameTextBox.Text;
                loginInfoLable.Text = "Successful Login";
                this.main.isLoggedIn = true;
                this.loginButton.Text = "Logout";
            }
            else if ((char)Codes.logout_respons == respons[0])
            {
                main.username = "";
                loginButton.Text = "Login";
                this.main.isLoggedIn = false;
                this.loginInfoLable.Text = "Successgul logout";
            }
            else
            {
                respons = respons.Substring(5, respons.Length - 5);
                ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                if (errorRespons.message == "You must login first.")
                {
                    main.username = "";
                    loginButton.Text = "Login";
                    this.main.isLoggedIn = false;
                    this.loginInfoLable.Text = "Unsuccessgul logout";
                }
                else    
                    loginInfoLable.Text = errorRespons.message;
                
            }

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            GraphicsPath GraphPath = new GraphicsPath();
            int radius = 250;
            float r2 = radius / 2f;
            float w2 = 0;
            Rectangle Rect = new Rectangle(0, 0, this.Width, this.Height);

            GraphPath.AddLine(Rect.X + w2, Rect.Y + w2, Rect.X, Rect.Y);
            GraphPath.AddArc(Rect.X + Rect.Width - radius - w2, Rect.Y + w2, radius, radius, 270, 90);
            GraphPath.AddLine(Rect.X + Rect.Width - w2, Rect.Y + Rect.Height, Rect.X + Rect.Width, Rect.Y + Rect.Height - w2);
            GraphPath.AddLine(Rect.X + w2, Rect.Y - w2 + Rect.Height, Rect.X - w2, radius);
            GraphPath.AddLine(Rect.X + w2, Rect.Y + Rect.Height + r2 + w2 + Rect.Y, Rect.X + w2, Rect.Y + Rect.Y);
            this.Region = new Region(GraphPath);
        }
        private void LoginForm_Load(object sender, EventArgs e)
        {
            
            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor, 0.7f);

            foreach (Control btns in this.Controls)
            {
                if (btns.GetType() == typeof(Button))
                {
                    Button btn = (Button)btns;
                    btn.BackColor = MainForm.PrimaryColor;
                    btn.ForeColor = Color.White;
                    btn.FlatAppearance.BorderColor = MainForm.SecondaryColor;
                }
            }
            LoginLable.ForeColor = MainForm.SecondaryColor;
            SignupLable.ForeColor = MainForm.SecondaryColor;
            if (this.main.isLoggedIn)
                loginButton.Text = "Logout";
            else 
                loginButton.Text = "Login";
        }

        private void signupButton_Click(object sender, EventArgs e)
        {
            this.main.click();
            SignupRequest signupRequest = new SignupRequest()
            {
                username = this.signupUsernameTextBox.Text,
                password = this.signupPasswordTextBox.Text,
                email = this.signupEmailTextBox.Text
            };
            int goodRespons = 102;
            string respons = MainForm.soc.SocketSendReceive(signupRequest);
            if ((char)goodRespons == respons[0])
            {
                loginInfoLable.Text = "successful sign up";
            }
            else
            {
                respons = respons.Substring(5, respons.Length - 5);
                ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                loginInfoLable.Text = errorRespons.message;
            }
        }

        private void EmailLable_Click(object sender, EventArgs e)
        {

        }

        private void SignupLable_Click(object sender, EventArgs e)
        {

        }

        private void signupEmailTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void signupPasswordTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void signupUsernameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void LoginUsernameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoginPasswordTextBox_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
