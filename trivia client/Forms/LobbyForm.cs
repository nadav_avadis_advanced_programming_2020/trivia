﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using MaterialSkin;
using trivia_client.request;
using trivia_client.respons;
using Newtonsoft.Json;
using System.Threading;
using System.Media;

namespace trivia_client.Forms
{
    [DefaultEvent("Click")]
    public partial class LobbyForm : Form
    {
        MaterialSkinManager skinManager;
        private SoundPlayer sound_player = new SoundPlayer(Properties.Resources.lobbyMusic);
        private List<string> playersList;
        private MainForm main;
        public Type creator;
        private int id;
        private Thread t;

        private CancellationTokenSource ts = new CancellationTokenSource();
        private CancellationToken ct;

        private int time;
        public LobbyForm(MainForm main, Type creator, int id,int time)
        {
            InitializeComponent();
            this.main = main;
            this.creator = creator;    
            this.startButton.Visible = creator == typeof(CreateForm);
            playersList = new List<string>();
            skinManager = MaterialSkinManager.Instance;
            skinManager.Theme = MaterialSkinManager.Themes.DARK;
            this.id = id;
            this.time = time;

             ct = this.ts.Token;
            t = new Thread(RefreshPlayers);
            t.IsBackground = true;
            t.Start();
            
        }

        private void RefreshPlayers()
        {
            while(!this.IsHandleCreated){ } // wait untill Handler is created

            while(true)
            {
                string respons = "";
                respons = MainForm.soc.SocketSendReceive(new GetRoomStateRequest());
                
                if ((char)Codes.getRoomState_respons == respons[0])
                {
                    //remove headers
                    respons = respons.Substring(5, respons.Length - 5); 

                    GetRoomStateResponse getRoomStateResponse = JsonConvert.DeserializeObject<GetRoomStateResponse>(respons);

                    // if the room is closed , go back to main menu
                    if (getRoomStateResponse.status == (char)Codes.leaveRoom_respons)
                    {
                       this.main.OpenForm(new JoinForm(this.main), this);
                        break;
                    }

                    if(getRoomStateResponse.status == (char)Codes.startGame_respons)
                    {
                        if(this.creator == typeof(JoinForm))
                            this.main.OpenForm(new GameForm(this.main, this.time,false), this);
                        else
                            this.main.OpenForm(new GameForm(this.main, this.time, true), this);
                        break;

                    }

                    // clear the list
                    this.materialListView.Invoke((MethodInvoker)(() => materialListView.Items.Clear()));

                    // put the user in the list
                    for (var i = 0; i < getRoomStateResponse.Players.Count; i++)
                        this.materialListView.Invoke((MethodInvoker)(() => materialListView.Items.Add(new ListViewItem(getRoomStateResponse.Players[i]))));    
                }
                
                Thread.Sleep(3000); // wait 3 seocnds , for no over-loading the server
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.main.click();

            this.t.Abort();

            if (creator == typeof(CreateForm))
            {
                CloseRoomRequest closeRoomRequest = new CloseRoomRequest();
                string respons = MainForm.soc.SocketSendReceive(closeRoomRequest);
                if ((char)Codes.closeRoom_respons == respons[0])
                {
                    this.main.OpenForm(new CreateForm(this.main), this);
                    
                }
                else
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                }
            }
            else
            {
                LeaveRoomRrequest leaveRoomRrequest = new LeaveRoomRrequest();
                string respons = MainForm.soc.SocketSendReceive(leaveRoomRrequest);
                if ((char)Codes.leaveRoom_respons == respons[0])
                {
                    this.main.OpenForm(new CreateForm(this.main), this);
                    
                }
                else
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                }
            }
            
        }

        private int GetPlayerIndex(string name)
        {
            int count = 0;
            foreach (string player in playersList)
            {
                if (player == name)
                    return count;
                count++;
            }
            return -1;
        }
        private void materialListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (materialListView.SelectedItems.Count > 0)
            {
                ListViewItem item = materialListView.SelectedItems[0];
                int index = GetPlayerIndex(item.Text);

                // do somethig ?
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            this.main.click();
            this.t.Abort();

           
            string respons = MainForm.soc.SocketSendReceive(new StartGameRequest());
            if ((char)Codes.startGame_respons == respons[0])
            {
                if (this.creator == typeof(JoinForm))
                    this.main.OpenForm(new GameForm(this.main, this.time, false), this);
                else
                    this.main.OpenForm(new GameForm(this.main, this.time, true), this);
            }
            else
            {
                respons = respons.Substring(5, respons.Length - 5);
                ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
            }

        }

        private void LobbyForm_Load(object sender, EventArgs e)
        {
            sound_player.PlayLooping();
        }

        private void LobbyForm_HandleCreated(object sender, System.EventArgs e)
        {
            
        }

        private void LobbyForm_Leave(object sender, EventArgs e)
        {
            sound_player.Stop();
        }
    }
}
