﻿namespace trivia_client.Forms
{
    partial class ConnectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IPlabel = new System.Windows.Forms.Label();
            this.IPtextBox = new System.Windows.Forms.TextBox();
            this.connectButton = new trivia_client.ColorButton();
            this.changeColor = new trivia_client.ChangeColor();
            this.SuspendLayout();
            // 
            // IPlabel
            // 
            this.IPlabel.AutoSize = true;
            this.IPlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.IPlabel.Location = new System.Drawing.Point(48, 65);
            this.IPlabel.Name = "IPlabel";
            this.IPlabel.Size = new System.Drawing.Size(55, 31);
            this.IPlabel.TabIndex = 8;
            this.IPlabel.Text = "IP :";
            this.IPlabel.Click += new System.EventHandler(this.IPlabel_Click);
            // 
            // IPtextBox
            // 
            this.IPtextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IPtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.IPtextBox.Location = new System.Drawing.Point(123, 65);
            this.IPtextBox.Multiline = true;
            this.IPtextBox.Name = "IPtextBox";
            this.IPtextBox.Size = new System.Drawing.Size(213, 31);
            this.IPtextBox.TabIndex = 7;
            this.IPtextBox.TextChanged += new System.EventHandler(this.IPtextBox_TextChanged);
            // 
            // connectButton
            // 
            this.connectButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connectButton.Angle = 0F;
            this.connectButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.connectButton.Location = new System.Drawing.Point(54, 214);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(282, 81);
            this.connectButton.TabIndex = 6;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // changeColor
            // 
            this.changeColor.Angle = 93F;
            this.changeColor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changeColor.Location = new System.Drawing.Point(0, 0);
            this.changeColor.Name = "changeColor";
            this.changeColor.Size = new System.Drawing.Size(384, 361);
            this.changeColor.TabIndex = 0;
            this.changeColor.Load += new System.EventHandler(this.changeColor_Load);
            // 
            // ConnectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.IPlabel);
            this.Controls.Add(this.IPtextBox);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.changeColor);
            this.Name = "ConnectForm";
            this.Text = "ConnectForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ChangeColor changeColor;
        private System.Windows.Forms.Label IPlabel;
        private System.Windows.Forms.TextBox IPtextBox;
        private ColorButton connectButton;
    }
}