﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using trivia_client.request;
using trivia_client.respons;

namespace trivia_client.Forms
{
    public partial class StatisticsForm : Form
    {
        public StatisticsForm()
        {
            InitializeComponent();
            
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            GraphicsPath GraphPath = new GraphicsPath();
            int radius = 250;
            float r2 = radius / 2f;
            float w2 = 0;
            Rectangle Rect = new Rectangle(0, 0, this.Width, this.Height);

            GraphPath.AddLine(Rect.X + w2, Rect.Y + w2, Rect.X, Rect.Y);
            GraphPath.AddArc(Rect.X + Rect.Width - radius - w2, Rect.Y + w2, radius, radius, 270, 90);
            GraphPath.AddLine(Rect.X + Rect.Width - w2, Rect.Y + Rect.Height, Rect.X + Rect.Width, Rect.Y + Rect.Height - w2);
            GraphPath.AddLine(Rect.X + w2, Rect.Y - w2 + Rect.Height, Rect.X - w2, radius);
            GraphPath.AddLine(Rect.X + w2, Rect.Y + Rect.Height + r2 + w2 + Rect.Y, Rect.X + w2, Rect.Y + Rect.Y);
            this.Region = new Region(GraphPath);
        }

        private void StatisticsForm_Load(object sender, EventArgs e)
        {
            this.BackColor = MainForm.ChangeColorBrightness(MainForm.PrimaryColor, 0.7f);
            GetStatisticsRequest getStatisticsRequest = new GetStatisticsRequest();

            string respons = MainForm.soc.SocketSendReceive(getStatisticsRequest);
            if (respons == "waiting for server to come up...") { }
            else if ((char)Codes.getPersonalStats_respons == respons[0])
            {
                respons = respons.Substring(5, respons.Length - 5);
                GetStatisticsResponse getStatisticsResponse = JsonConvert.DeserializeObject<GetStatisticsResponse>(respons);
                this.label6.Text = "Your total points is: " + getStatisticsResponse.points.ToString() + "\n";
                this.label6.Text += "Your total correct answers is: " + getStatisticsResponse.correctAnswers.ToString() + "\n";
                this.label6.Text += "Your total answers is: " + getStatisticsResponse.numOfTotalAnswers.ToString() + "\n";
                this.label6.Text += "Your average answer time is: " + getStatisticsResponse.averageAnswerTime.ToString() + "\n";
                this.label6.Text += "Number of played games: " + getStatisticsResponse.numberOfPlayedGames.ToString() + "\n";
            }
            else
            {
                respons = respons.Substring(5, respons.Length - 5);
                ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
            }
        }

        private void tabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                GetStatisticsRequest getStatisticsRequest = new GetStatisticsRequest();

                string respons = MainForm.soc.SocketSendReceive(getStatisticsRequest);
                if ((char)Codes.getPersonalStats_respons == respons[0])
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    GetStatisticsResponse getStatisticsResponse = JsonConvert.DeserializeObject<GetStatisticsResponse>(respons);
                    this.label6.Text = getStatisticsResponse.ToString();
                }
                else
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                }
            }
            else
            {
                GetHighestScoreStatisticsRequest getHighestScoreStatisticsRequest = new GetHighestScoreStatisticsRequest();

                string respons = MainForm.soc.SocketSendReceive(getHighestScoreStatisticsRequest);
                if ((char)Codes.getHighScore_respons == respons[0])
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    GetHighestScoreStatisticsRespons getHighestScoreStatisticsRespons = JsonConvert.DeserializeObject<GetHighestScoreStatisticsRespons>(respons);
                    for (int i = 0; i < getHighestScoreStatisticsRespons.highScores.Count; i++)
                    {
                        if(i == 0)
                        {
                            this.label1.Text = getHighestScoreStatisticsRespons.highScores[i].ToString();
                        }
                        else if(i == 1)
                        {
                            this.label2.Text = getHighestScoreStatisticsRespons.highScores[1].ToString();
                        }
                        else if(i == 2)
                        {
                            this.label3.Text = getHighestScoreStatisticsRespons.highScores[2].ToString();

                        }
                        else if (i == 3)
                        {
                            this.label4.Text = getHighestScoreStatisticsRespons.highScores[3].ToString();

                        }
                        else 
                        {
                            this.label5.Text = getHighestScoreStatisticsRespons.highScores[4].ToString();

                        }
                    }
                }
                else
                {
                    respons = respons.Substring(5, respons.Length - 5);
                    ErrorRespons errorRespons = JsonConvert.DeserializeObject<ErrorRespons>(respons);
                }
            }

        }

        private void PersonalTab_Click(object sender, EventArgs e)
        {

        }
    }
}
