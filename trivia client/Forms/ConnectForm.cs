﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trivia_client.Forms
{
    public partial class ConnectForm : Form
    {
        public string ip = "";
        public ConnectForm()
        {
            InitializeComponent();
            this.connectButton.text = "Connect";
            this.connectButton.TextSize = 20;
            this.IPtextBox.Text = MainForm.soc.GetServer();

            this.changeColor.firstColor = new Color();
            this.changeColor.secondColor = new Color();
            this.changeColor.firstColor = Color.FromArgb(51, 51, 76);
            this.changeColor.secondColor = Color.FromArgb(0, 150, 136);
            this.changeColor.wh = 1;
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            IPAddress address;
            if (IPAddress.TryParse(this.IPtextBox.Text, out address))
            {
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) // we have IPv4
                {
                    ip = this.IPtextBox.Text;
                    this.Close();
                }
                else
                    MessageBox.Show("This isnt a vaild ip address.",
                                "Trivia",
                                MessageBoxButtons.OK);
            }
        }

        private void IPtextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void IPlabel_Click(object sender, EventArgs e)
        {

        }

        private void changeColor_Load(object sender, EventArgs e)
        {

        }
    }
}
