﻿using System.Drawing;
using System.Windows.Forms;

namespace trivia_client.Forms
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backButton = new System.Windows.Forms.Button();
            this.QuestionLable = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BarTimer = new System.Windows.Forms.Timer(this.components);
            this.CButton = new trivia_client.ColorButton();
            this.DButton = new trivia_client.ColorButton();
            this.BButton = new trivia_client.ColorButton();
            this.AButton = new trivia_client.ColorButton();
            this.changeColor = new trivia_client.ChangeColor();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.backButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Location = new System.Drawing.Point(50, 513);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(132, 42);
            this.backButton.TabIndex = 20;
            this.backButton.Text = "Leave";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // QuestionLable
            // 
            this.QuestionLable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.QuestionLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.QuestionLable.Location = new System.Drawing.Point(0, 10);
            this.QuestionLable.Name = "QuestionLable";
            this.QuestionLable.Size = new System.Drawing.Size(985, 100);
            this.QuestionLable.TabIndex = 0;
            this.QuestionLable.Text = "Question";
            this.QuestionLable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(50, 130);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(900, 20);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 15;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.QuestionLable);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(985, 120);
            this.panel1.TabIndex = 16;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // CButton
            // 
            this.CButton.Angle = 312F;
            this.CButton.BackColor = System.Drawing.Color.Transparent;
            this.CButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CButton.Location = new System.Drawing.Point(50, 363);
            this.CButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(400, 100);
            this.CButton.TabIndex = 24;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // DButton
            // 
            this.DButton.Angle = 312F;
            this.DButton.BackColor = System.Drawing.Color.Transparent;
            this.DButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DButton.Location = new System.Drawing.Point(550, 363);
            this.DButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DButton.Name = "DButton";
            this.DButton.Size = new System.Drawing.Size(400, 100);
            this.DButton.TabIndex = 23;
            this.DButton.Click += new System.EventHandler(this.DButton_Click);
            // 
            // BButton
            // 
            this.BButton.Angle = 312F;
            this.BButton.BackColor = System.Drawing.Color.Transparent;
            this.BButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.BButton.Location = new System.Drawing.Point(550, 206);
            this.BButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BButton.Name = "BButton";
            this.BButton.Size = new System.Drawing.Size(400, 100);
            this.BButton.TabIndex = 22;
            this.BButton.Click += new System.EventHandler(this.BButton_Click);
            // 
            // AButton
            // 
            this.AButton.Angle = 312F;
            this.AButton.BackColor = System.Drawing.Color.Transparent;
            this.AButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.AButton.Location = new System.Drawing.Point(50, 206);
            this.AButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.AButton.Name = "AButton";
            this.AButton.Size = new System.Drawing.Size(400, 100);
            this.AButton.TabIndex = 21;
            this.AButton.Click += new System.EventHandler(this.AButton_Click);
            // 
            // changeColor
            // 
            this.changeColor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.changeColor.Angle = 277F;
            this.changeColor.Location = new System.Drawing.Point(-117, -52);
            this.changeColor.Name = "changeColor";
            this.changeColor.Size = new System.Drawing.Size(1102, 619);
            this.changeColor.TabIndex = 0;
            this.changeColor.Load += new System.EventHandler(this.changeColor_Load);
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.CButton);
            this.Controls.Add(this.DButton);
            this.Controls.Add(this.BButton);
            this.Controls.Add(this.AButton);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.changeColor);
            this.Name = "GameForm";
            this.Text = "GameForm";
            this.Load += new System.EventHandler(this.GameForm_Load);
            this.Shown += new System.EventHandler(this.GameForm_Shown);
            this.Leave += new System.EventHandler(this.GameForm_Leave);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        #endregion

        private ChangeColor changeColor;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label QuestionLable;
        private System.Windows.Forms.Panel panel1;
        private ColorButton AButton;
        private ColorButton BButton;
        private ColorButton DButton;
        private ColorButton CButton;
        private Timer BarTimer;
        public ProgressBar progressBar;
    }
}