﻿namespace trivia_client.Forms
{
    partial class JoinForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.JoinRoomLable = new System.Windows.Forms.Label();
            this.materialListView = new MaterialSkin.Controls.MaterialListView();
            this.Room_Name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Max_Players = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Number_of_qusetions = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Time_per_question = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.refresh = new System.Windows.Forms.Button();
            this.joinLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // JoinRoomLable
            // 
            this.JoinRoomLable.AutoSize = true;
            this.JoinRoomLable.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.JoinRoomLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.JoinRoomLable.Location = new System.Drawing.Point(60, 45);
            this.JoinRoomLable.Name = "JoinRoomLable";
            this.JoinRoomLable.Size = new System.Drawing.Size(143, 31);
            this.JoinRoomLable.TabIndex = 24;
            this.JoinRoomLable.Text = "Join Room";
            this.JoinRoomLable.Click += new System.EventHandler(this.JoinRoomLable_Click);
            // 
            // materialListView
            // 
            this.materialListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.materialListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Room_Name,
            this.Max_Players,
            this.Number_of_qusetions,
            this.Time_per_question});
            this.materialListView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.materialListView.Depth = 0;
            this.materialListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.materialListView.FullRowSelect = true;
            this.materialListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.materialListView.HideSelection = false;
            this.materialListView.Location = new System.Drawing.Point(192, 79);
            this.materialListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialListView.MouseState = MaterialSkin.MouseState.OUT;
            this.materialListView.Name = "materialListView";
            this.materialListView.OwnerDraw = true;
            this.materialListView.Size = new System.Drawing.Size(572, 190);
            this.materialListView.TabIndex = 25;
            this.materialListView.UseCompatibleStateImageBehavior = false;
            this.materialListView.View = System.Windows.Forms.View.Details;
            this.materialListView.SelectedIndexChanged += new System.EventHandler(this.materialListView_SelectedIndexChanged);
            // 
            // Room_Name
            // 
            this.Room_Name.Text = "Room Name";
            this.Room_Name.Width = 150;
            // 
            // Max_Players
            // 
            this.Max_Players.Text = "Max Players";
            this.Max_Players.Width = 105;
            // 
            // Number_of_qusetions
            // 
            this.Number_of_qusetions.Text = "Number of questions";
            this.Number_of_qusetions.Width = 160;
            // 
            // Time_per_question
            // 
            this.Time_per_question.Text = "Time per question";
            this.Time_per_question.Width = 140;
            // 
            // refresh
            // 
            this.refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refresh.FlatAppearance.BorderSize = 0;
            this.refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refresh.Location = new System.Drawing.Point(66, 232);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(106, 37);
            this.refresh.TabIndex = 26;
            this.refresh.Text = "refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // joinLabel
            // 
            this.joinLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.joinLabel.AutoSize = true;
            this.joinLabel.Location = new System.Drawing.Point(0, 437);
            this.joinLabel.Name = "joinLabel";
            this.joinLabel.Size = new System.Drawing.Size(0, 13);
            this.joinLabel.TabIndex = 27;
            // 
            // JoinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.joinLabel);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.materialListView);
            this.Controls.Add(this.JoinRoomLable);
            this.Name = "JoinForm";
            this.Text = "JoinForm";
            this.Load += new System.EventHandler(this.JoinForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label JoinRoomLable;
        private MaterialSkin.Controls.MaterialListView materialListView;
        private System.Windows.Forms.ColumnHeader Room_Name;
        private System.Windows.Forms.ColumnHeader Max_Players;
        private System.Windows.Forms.ColumnHeader Number_of_qusetions;
        private System.Windows.Forms.ColumnHeader Time_per_question;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.Label joinLabel;
    }
}