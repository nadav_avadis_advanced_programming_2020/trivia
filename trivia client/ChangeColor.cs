﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace trivia_client
{
    public partial class ChangeColor : UserControl
    {
        public int wh = 20;
        private Timer timer =  new Timer();
        private float ang;
        public Color firstColor  = Color.Blue;
        public Color secondColor = Color.Orange;
        public ChangeColor()
        {
            InitializeComponent();
            DoubleBuffered = true;
            timer.Interval = 60;
            timer.Start();
            timer.Tick += (s, e) => { Angle = Angle % 360 + 1; };
            firstColor = MainForm.PrimaryColor;

            int index = new Random().Next(MainForm.colorList.Count);
            while (MainForm.tempIndex == index)
            {
                index = new Random().Next(MainForm.colorList.Count);
            }
            MainForm.tempIndex = index;
            String color = MainForm.colorList[index];

            secondColor =  ColorTranslator.FromHtml(color);
        }

        public float Angle
        {
            get { return ang; }
            set { ang = value; Invalidate(); }
        }

        protected override void OnPaint(PaintEventArgs e)
        {

            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            GraphicsPath gp = new GraphicsPath();

            gp.AddArc(new Rectangle(0,0,wh,wh),180,90);
            gp.AddArc(new Rectangle(Width - wh,0,wh,wh),-90, 90);
            gp.AddArc(new Rectangle(Width - wh , Height - wh, wh ,wh),0,90);
            gp.AddArc(new Rectangle(0, Height - wh, wh, wh),90, 90);

            e.Graphics.FillPath(new LinearGradientBrush(ClientRectangle, firstColor, secondColor, ang), gp);
            base.OnPaint(e);
            SendToBack();
        }

        private void ChangeColor_Load(object sender, EventArgs e)
        {

        }
    }
}
