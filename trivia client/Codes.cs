﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client
{
    public static class Codes
    {
        const  int no_code = -0001;
        //erorr
        const  int erorr = 0000;

        //requests : 

        //login 
        public const  int login_request = 1;
        public const int signup_request = 2;
        public const int logout_request = 3;

        //menu
        public const  int createRoom_request = 4;
        public const  int joinRoom_request = 5;
        public const  int getPersonalStats_request = 6;
        public const  int getPlayersInRoom_request = 7;
        public const  int getRooms_request = 8;
        public const  int signout_request = 9;
        public const int getHighScore_request = 10;

        //room 
        public const int closeRoom_request = 11;
        public const int startGame_request = 12;
        public const int getRoomState_request = 13;
        public const int leaveRoom_request = 14;

        //game 
        public const int leaveGame_request = 15;
        public const int submitAnswer_request = 16;
        public const int getQuestion_request = 17;
        public const int getGameResult_request = 18;


        //extra
        public const int getOlinePlyers_request = 19;

        // respons :

        // login
        public const int login_respons = 101;
        public const  int signup_respons = 102;
        public const  int logout_respons = 103;

        // menu
        public const int createRoom_respons = 104;
        public const  int joinRoom_respons = 105;
        public const  int getPersonalStats_respons = 106;
        public const  int getPlayersInRoom_respons = 107;
        public const  int getRooms_respons = 108;
        public const  int signout_respons = 109;
        public const  int getHighScore_respons = 110;

        //room 
        public const int closeRoom_respons = 111;
        public const int startGame_respons = 112;
        public const int getRoomState_respons = 113;
        public const int leaveRoom_respons = 114;

        //game 
        public const int leaveGame_respons = 115;
        public const int submitAnswer_respons = 116;
        public const int getQuestion_respons = 117;
        public const int getGameResult_respons = 118;

        //extra
        public const int getOlinePlyers_respons = 119;


    }
}
