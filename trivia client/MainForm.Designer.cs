﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace trivia_client
{
    partial class MainForm
    {
        public static Color PrimaryColor { get; set; }
        public static Color SecondaryColor { get; set; }

        public static List<string> colorList = new List<string>()
        {
            "#3f51b5","#009688", "#ff5722","#607D8B","#FF9800","#9C27B0","#2196F3",
            "#EA676C","#E41A4A","#5978BB","#018790","#0E3441","#00b0AD","#721D47",
            "#EA4833","#EF937E","#F37521","#A12059","#126881","#8BC240","#364D5B",
            "#C7DC5B","#0094BC","#E4126B","#43B76E","#7BCFE9","#B71C46"
        };

        public static Color ChangeColorBrightness(Color color , double correntionFactor)
        {
            double R = color.R , G = color.G , B = color.B;

            // if  correntionFactor < 0 , darker color.
            if (correntionFactor < 0)
            {
                correntionFactor = 1 + correntionFactor;
                R *= correntionFactor;
                G *= correntionFactor;
                B *= correntionFactor;
            }
            // if  correntionFactor > 0 , lighten color.
            else
            {
                R = (255 - R) * correntionFactor + R;
                G = (255 - G) * correntionFactor + G;
                B = (255 - B) * correntionFactor + B;
            }

            return Color.FromArgb(color.A, (byte)R, (byte)G, (byte)B);
        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.sidePanel = new System.Windows.Forms.Panel();
            this.onlineButton = new System.Windows.Forms.Button();
            this.topLeftPanel = new System.Windows.Forms.Panel();
            this.labelTopLeft = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.Button();
            this.StatisticsButton = new System.Windows.Forms.Button();
            this.joinRoomButton = new System.Windows.Forms.Button();
            this.createRoomButton = new System.Windows.Forms.Button();
            this.menuLoginButton = new System.Windows.Forms.Button();
            this.topPanel = new trivia_client.GradientPanel();
            this.TopLable = new System.Windows.Forms.Label();
            this.MainPanel = new trivia_client.GradientPanel();
            this.FullPanel = new System.Windows.Forms.Panel();
            this.sidePanel.SuspendLayout();
            this.topLeftPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sidePanel
            // 
            this.sidePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.sidePanel.Controls.Add(this.onlineButton);
            this.sidePanel.Controls.Add(this.topLeftPanel);
            this.sidePanel.Controls.Add(this.exitButton);
            this.sidePanel.Controls.Add(this.StatisticsButton);
            this.sidePanel.Controls.Add(this.joinRoomButton);
            this.sidePanel.Controls.Add(this.createRoomButton);
            this.sidePanel.Controls.Add(this.menuLoginButton);
            this.sidePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sidePanel.Location = new System.Drawing.Point(0, 0);
            this.sidePanel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.sidePanel.Name = "sidePanel";
            this.sidePanel.Size = new System.Drawing.Size(200, 561);
            this.sidePanel.TabIndex = 1;
            // 
            // onlineButton
            // 
            this.onlineButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.onlineButton.FlatAppearance.BorderSize = 0;
            this.onlineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.onlineButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.onlineButton.Image = global::trivia_client.Properties.Resources.world_grid;
            this.onlineButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.onlineButton.Location = new System.Drawing.Point(0, 365);
            this.onlineButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.onlineButton.Name = "onlineButton";
            this.onlineButton.Size = new System.Drawing.Size(200, 70);
            this.onlineButton.TabIndex = 5;
            this.onlineButton.Text = "       online plyers";
            this.onlineButton.UseVisualStyleBackColor = true;
            this.onlineButton.Click += new System.EventHandler(this.onlineButton_Click);
            // 
            // topLeftPanel
            // 
            this.topLeftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(58)))));
            this.topLeftPanel.Controls.Add(this.labelTopLeft);
            this.topLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.topLeftPanel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.topLeftPanel.Name = "topLeftPanel";
            this.topLeftPanel.Size = new System.Drawing.Size(200, 75);
            this.topLeftPanel.TabIndex = 3;
            // 
            // labelTopLeft
            // 
            this.labelTopLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTopLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelTopLeft.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelTopLeft.Location = new System.Drawing.Point(0, 0);
            this.labelTopLeft.Name = "labelTopLeft";
            this.labelTopLeft.Size = new System.Drawing.Size(200, 75);
            this.labelTopLeft.TabIndex = 0;
            this.labelTopLeft.Text = "Trivia";
            this.labelTopLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTopLeft.Click += new System.EventHandler(this.labelTopLeft_Click);
            // 
            // exitButton
            // 
            this.exitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitButton.FlatAppearance.BorderSize = 0;
            this.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.exitButton.Image = global::trivia_client.Properties.Resources.exit;
            this.exitButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.exitButton.Location = new System.Drawing.Point(0, 435);
            this.exitButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(200, 70);
            this.exitButton.TabIndex = 4;
            this.exitButton.Text = "   Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // StatisticsButton
            // 
            this.StatisticsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StatisticsButton.FlatAppearance.BorderSize = 0;
            this.StatisticsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatisticsButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.StatisticsButton.Image = global::trivia_client.Properties.Resources.statistics;
            this.StatisticsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StatisticsButton.Location = new System.Drawing.Point(2, 295);
            this.StatisticsButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.StatisticsButton.Name = "StatisticsButton";
            this.StatisticsButton.Size = new System.Drawing.Size(200, 70);
            this.StatisticsButton.TabIndex = 3;
            this.StatisticsButton.Text = "   Statistics";
            this.StatisticsButton.UseVisualStyleBackColor = true;
            this.StatisticsButton.Click += new System.EventHandler(this.StatisticsButton_Click);
            // 
            // joinRoomButton
            // 
            this.joinRoomButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.joinRoomButton.FlatAppearance.BorderSize = 0;
            this.joinRoomButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.joinRoomButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.joinRoomButton.Image = global::trivia_client.Properties.Resources.join;
            this.joinRoomButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.joinRoomButton.Location = new System.Drawing.Point(0, 225);
            this.joinRoomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.joinRoomButton.Name = "joinRoomButton";
            this.joinRoomButton.Size = new System.Drawing.Size(200, 70);
            this.joinRoomButton.TabIndex = 2;
            this.joinRoomButton.Text = "   JoinRoom";
            this.joinRoomButton.UseVisualStyleBackColor = true;
            this.joinRoomButton.Click += new System.EventHandler(this.joinRoomButton_Click);
            // 
            // createRoomButton
            // 
            this.createRoomButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.createRoomButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createRoomButton.FlatAppearance.BorderSize = 0;
            this.createRoomButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createRoomButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.createRoomButton.Image = global::trivia_client.Properties.Resources.create;
            this.createRoomButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.createRoomButton.Location = new System.Drawing.Point(2, 155);
            this.createRoomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.createRoomButton.Name = "createRoomButton";
            this.createRoomButton.Size = new System.Drawing.Size(200, 70);
            this.createRoomButton.TabIndex = 1;
            this.createRoomButton.Text = "   CreateRoom";
            this.createRoomButton.UseVisualStyleBackColor = false;
            this.createRoomButton.Click += new System.EventHandler(this.createRoomButton_Click);
            // 
            // menuLoginButton
            // 
            this.menuLoginButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuLoginButton.FlatAppearance.BorderSize = 0;
            this.menuLoginButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menuLoginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.menuLoginButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.menuLoginButton.Image = global::trivia_client.Properties.Resources.login1;
            this.menuLoginButton.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.menuLoginButton.Location = new System.Drawing.Point(0, 75);
            this.menuLoginButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.menuLoginButton.Name = "menuLoginButton";
            this.menuLoginButton.Size = new System.Drawing.Size(200, 70);
            this.menuLoginButton.TabIndex = 0;
            this.menuLoginButton.Text = "   Login";
            this.menuLoginButton.UseVisualStyleBackColor = true;
            this.menuLoginButton.Click += new System.EventHandler(this.menuLoginButton_Click);
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.topPanel.Controls.Add(this.TopLable);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(200, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.topPanel.Name = "topPanel";
            this.topPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.topPanel.Size = new System.Drawing.Size(784, 75);
            this.topPanel.TabIndex = 2;
            // 
            // TopLable
            // 
            this.TopLable.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TopLable.AutoSize = true;
            this.TopLable.BackColor = System.Drawing.Color.Transparent;
            this.TopLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.TopLable.ForeColor = System.Drawing.Color.Gainsboro;
            this.TopLable.Location = new System.Drawing.Point(361, 25);
            this.TopLable.Name = "TopLable";
            this.TopLable.Size = new System.Drawing.Size(104, 26);
            this.TopLable.TabIndex = 0;
            this.TopLable.Text = "Welcome";
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.FullPanel);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Padding = new System.Windows.Forms.Padding(200, 75, 0, 0);
            this.MainPanel.Size = new System.Drawing.Size(984, 561);
            this.MainPanel.TabIndex = 0;
            this.MainPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.MainPanel_Paint);
            // 
            // FullPanel
            // 
            this.FullPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FullPanel.Location = new System.Drawing.Point(0, 0);
            this.FullPanel.Name = "FullPanel";
            this.FullPanel.Size = new System.Drawing.Size(985, 565);
            this.FullPanel.TabIndex = 0;
            this.FullPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.FullPanel_Paint);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.sidePanel);
            this.Controls.Add(this.MainPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.sidePanel.ResumeLayout(false);
            this.topLeftPanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.MainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

     





        #endregion

        private GradientPanel MainPanel;
        private Panel sidePanel;
        private Button menuLoginButton;
        private GradientPanel topPanel;
        private Button createRoomButton;
        private Button joinRoomButton;
        private Button StatisticsButton;
        private Button exitButton;
        private Panel topLeftPanel;
        private Label TopLable;
        private Label labelTopLeft;
        private Panel FullPanel;
        private Button onlineButton;
    }
}