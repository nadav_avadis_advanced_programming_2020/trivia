﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace trivia_client
{
    [DefaultEvent("Click")] 
    public partial class ColorButton : UserControl
    {
        public float TextSize = 15;
        public string text = "";
        public int wh = 50;
        private Timer timer = new Timer();
        private float ang;
        public Color firstColor = Color.Blue;
        public Color secondColor = Color.Orange;
        public ColorButton()
        {
            InitializeComponent();
            DoubleBuffered = true;
            timer.Interval = 60;
            timer.Tick += (s, e) => { Angle = Angle % 360 + 10; };
            timer.Stop();
        }


        public float Angle
        {
            get { return ang; }
            set { ang = value;  Invalidate(); }
        }

        protected override void OnPaint(PaintEventArgs e)
        {

            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            GraphicsPath gp = new GraphicsPath();

            gp.AddArc(new Rectangle(0, 0, wh, wh), 180, 90);
            gp.AddArc(new Rectangle(Width - wh, 0, wh, wh), -90, 90);
            gp.AddArc(new Rectangle(Width - wh, Height - wh, wh, wh), 0, 90);
            gp.AddArc(new Rectangle(0, Height - wh, wh, wh), 90, 90);

            e.Graphics.FillPath(new LinearGradientBrush(ClientRectangle, firstColor, secondColor, ang), gp);
            this.Region = new Region(gp);
            base.OnPaint(e);

            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            Font font = new System.Drawing.Font("Microsoft Sans Serif", this.TextSize , System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            e.Graphics.DrawString(text, font, new LinearGradientBrush(ClientRectangle, Color.Black, Color.Black, 90), ClientRectangle, sf);

        }

        private void ColorButton_Load(object sender, EventArgs e)
        {
            
        }

        private void ColorButton_MouseHover(object sender, EventArgs e)
        {
            timer.Start();
        }

        private void ColorButton_MouseLeave(object sender, EventArgs e)
        {
            timer.Stop();
        }
    }
}
