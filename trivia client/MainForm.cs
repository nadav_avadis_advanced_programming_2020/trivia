﻿using NAudio.Wave;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using trivia_client.Forms;
using trivia_client.request;
using trivia_client.respons;

namespace trivia_client
{
    public partial class MainForm : Form
    {

        private Button curr_button;
        public static int tempIndex;
        public string username;
        private  Form activeForm;
        private Random random = new Random();
        public static GetSocket soc;
        public static List<Room> roomList;
        public bool isLoggedIn = false;
        public MainForm()
        {
            InitializeComponent();
            this.ResizeRedraw = true;
            soc = new GetSocket("127.0.0.1", 2904, this);
            
            
        }
    

        public Label GetTopLeftLabel()
        {
            return this.labelTopLeft;
        }

        public Color SelectThereColor()
        {
            int index = this.random.Next(colorList.Count);
            while (tempIndex == index)
            {
                index = this.random.Next(colorList.Count);
            }
            tempIndex = index;
            String color = colorList[index];

            return ColorTranslator.FromHtml(color);
        }

        private void ActiveButton(object btnSender)
        {
            if (btnSender != null)
            {
                if (curr_button != (Button)btnSender)
                {
                    DisableButton();
                    Color color = SelectThereColor();
                    curr_button = (Button)btnSender;
                    curr_button.BackColor = color;
                    curr_button.ForeColor = Color.White;
                    curr_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));

                    topPanel.BackColor = color;

                    topLeftPanel.BackColor = ChangeColorBrightness(color, -0.3);

                    MainForm.PrimaryColor = color;
                    MainForm.SecondaryColor = ChangeColorBrightness(color, -0.3); ;

                    this.BackColor = MainForm.SecondaryColor;
                    this.topPanel.RightColor = MainForm.PrimaryColor;
                    this.MainPanel.RightColor = MainForm.PrimaryColor;
                    this.topPanel.LeftColor = MainForm.SecondaryColor;
                    this.MainPanel.LeftColor = MainForm.SecondaryColor;
                }
            }
        }

        private void DisableButton()
        {
            foreach (Control previousBtn in sidePanel.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = Color.FromArgb(51, 51, 76);
                    previousBtn.ForeColor = Color.Gainsboro;
                    previousBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
                }
            }
        }
        public void OpenForm(Form form, object  sender)
        {
            bool isToMenuButton;
            
            while(!this.IsHandleCreated)
            { }

            if (activeForm != null)
            {
                this.activeForm.Invoke((MethodInvoker)(() => activeForm.Close()));
            }

            if (sender.GetType() == typeof(Button))
            {
                ActiveButton(sender);
                isToMenuButton = true;
            }
            else
            {
                isToMenuButton = IsToMenuButton(sender as Form, form);

                if (sender.GetType() == typeof(LobbyForm) && form.GetType() != typeof(GameForm))
                {
                    if ((sender as LobbyForm).creator == typeof(CreateForm))
                        form = new CreateForm(this);
                    else if ((sender as LobbyForm).creator == typeof(JoinForm))
                        form = new JoinForm(this);

                }
            }
            form.TopLevel = false;
            form.Dock = DockStyle.Fill;

            form.FormBorderStyle = FormBorderStyle.None;

            

            if (isToMenuButton)
            {
                
                this.FullPanel.Invoke((MethodInvoker)(() => FullPanel.Visible = false));
                this.topPanel.Invoke((MethodInvoker)(() => topPanel.Visible = true));
                this.sidePanel.Invoke((MethodInvoker)(() => sidePanel.Visible = true));
                this.topLeftPanel.Invoke((MethodInvoker)(() => topLeftPanel.Visible = true));

                this.FullPanel.Invoke((MethodInvoker)(() => FullPanel.SendToBack()));
                this.TopLable.Invoke((MethodInvoker)(() => TopLable.Text = form.Name));

                this.MainPanel.Invoke((MethodInvoker)(() => this.MainPanel.Controls.Add(form)));
                this.MainPanel.Invoke((MethodInvoker)(() => this.MainPanel.Tag = form));

                if (form.GetType() == typeof(StatisticsForm) && sender.GetType() == typeof(GameForm) && (sender as GameForm).gameOver)
                    ActiveButton(StatisticsButton);
            }
            else
            {
                this.FullPanel.Invoke((MethodInvoker)(() => FullPanel.Visible = true));
                this.topPanel.Invoke((MethodInvoker)(() => topPanel.Visible = false));
                this.sidePanel.Invoke((MethodInvoker)(() => sidePanel.Visible = false));
                this.topLeftPanel.Invoke((MethodInvoker)(() => topLeftPanel.Visible = false));  

                this.FullPanel.Invoke((MethodInvoker)(() => FullPanel.BringToFront()));

                this.FullPanel.Invoke((MethodInvoker)(() => this.FullPanel.Controls.Add(form)));
                this.FullPanel.Invoke((MethodInvoker)(() => this.FullPanel.Tag = form));
            }

            form.BringToFront();
            if (form.InvokeRequired)
                form.Invoke(new MethodInvoker(form.Show));
            else
                form.Show();
            
            activeForm = form;
        }
        private bool IsToMenuButton(Form form, Form toForm)
        {
            return form.GetType() == typeof(LobbyForm) && toForm.GetType() != typeof(GameForm) ||
                form.GetType() == typeof(GameForm) ||
                form.GetType() == typeof(MainForm);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Bitmap bmp = Properties.Resources.login1;
            bmp.MakeTransparent(Color.White);
            menuLoginButton.Image = bmp;

            bmp = Properties.Resources.create;
            bmp.MakeTransparent(Color.White);
            createRoomButton.Image = bmp;

            bmp = Properties.Resources.join;
            bmp.MakeTransparent(Color.White);
            joinRoomButton.Image = bmp;

            bmp = Properties.Resources.statistics;
            bmp.MakeTransparent(Color.White);
            StatisticsButton.Image = bmp;

            bmp = Properties.Resources.exit;
            bmp.MakeTransparent(Color.White);
            exitButton.Image = bmp;

            this.labelTopLeft.Text = "Connecting to server...";
            this.labelTopLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            soc.ConnectSocket();
        }
        public void click()
        {
            SoundPlayer sound_player;
            switch (this.random.Next(0, 7))
            {
                case 0:
                    sound_player = new SoundPlayer(Properties.Resources.Mouse_Click_00);
                    break;
                case 1:
                    sound_player = new SoundPlayer(Properties.Resources.Mouse_Click_01);
                    break;
                case 2:
                    sound_player = new SoundPlayer(Properties.Resources.Mouse_Click_02);
                    break;
                case 3:
                    sound_player = new SoundPlayer(Properties.Resources.Mouse_Click_03);
                    break;
                case 4:
                    sound_player = new SoundPlayer(Properties.Resources.Mouse_Click_04);
                    break;
                case 5:
                    sound_player = new SoundPlayer(Properties.Resources.Mouse_Click_05);
                    break;

                default:
                    sound_player = new SoundPlayer(Properties.Resources.Mouse_Click_06);
                    break;
            }

            sound_player.Play();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            click();
            OpenForm(new ExitForm(this), sender);
        }

        private void createRoomButton_Click(object sender, EventArgs e)
        {
            click();
            OpenForm(new CreateForm(this), sender);
        }

        private void joinRoomButton_Click(object sender, EventArgs e)
        {
            click();
            OpenForm(new JoinForm(this), sender);
        }

        private void StatisticsButton_Click(object sender, EventArgs e)
        {
            click();
            OpenForm(new StatisticsForm(), sender);
        }

        private void menuLoginButton_Click(object sender, EventArgs e)
        {
            click();
            OpenForm(new LoginForm(this), sender);
        }

        private void MainPanel_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void FullPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelTopLeft_Click(object sender, EventArgs e)
        {
            ConnectForm form = new ConnectForm();
            var result = form.ShowDialog();
            if(form.ip != "")
            {
                soc.close();
                soc = new GetSocket(form.ip, 2904, this);
                soc.ConnectSocket();
            }
                
        }

      

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are you sure want to exit?",
                                "Trivia",
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Information) == DialogResult.OK)
                {

                    soc.close();
                    Environment.Exit(0);
                }
                else
                    //e.Cancel = true; // to don't close form is user change his mind
            }
            */

        }

        private void onlineButton_Click(object sender, EventArgs e)
        {
            click();
            OpenForm(new OnlinePlyersForm(this), sender);
        }
    }
}
