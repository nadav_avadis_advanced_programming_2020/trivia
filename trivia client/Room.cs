﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trivia_client
{
    public class Room
    {
        public int ID { get; set; }
        public string name { get; set; }
        public int maxPlayers { get; set; }
        public int numberOfQuestion { get; set; }
        public int timePerQuestion { get; set; }

        public Room(int id , string name, int maxPlayers, int numberOfQestions , int TimePerQuestion)
        {
            this.name = name;
            this.ID = id;
            this.numberOfQuestion = numberOfQestions;
            this.timePerQuestion = TimePerQuestion;
            this.maxPlayers = maxPlayers;
        }
    }
}
