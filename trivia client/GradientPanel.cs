﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace trivia_client
{
    class GradientPanel : Panel
    {
        public Color RightColor;
        public Color LeftColor;

        public GradientPanel() : base()
        {
            
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            LinearGradientBrush lgb=  new LinearGradientBrush(this.ClientRectangle, this.LeftColor, this.RightColor, 180F);
            
            Graphics g = e.Graphics;
            g.FillRectangle(lgb, this.ClientRectangle);
            
            base.OnPaint(e);
        }
    }
}
