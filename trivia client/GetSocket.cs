﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using trivia_client.request;
using System.Threading;
using System.Windows.Forms;

namespace trivia_client
{
    public class GetSocket
    {
        private Socket socket;
        private string server;
        private int port;
        private MainForm main;

        public GetSocket(string server, int port, MainForm main)
        {
            this.server = server;
            this.port = port;
            this.socket = null;
            this.main = main;
        }

        public string GetServer()
        {
            return this.server;
        }
        public void ConnectSocket()
        {
            Thread t = new Thread(ConnectTheSocket);
            t.IsBackground = true;
            t.Start();
        }
        private void ConnectTheSocket()
        {
            Socket sck = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(this.server), this.port);
            
            while(!sck.Connected)
            {
                try
                {
                    sck.Connect(endPoint);
                    this.main.GetTopLeftLabel().Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
                    if(this.main.GetTopLeftLabel().InvokeRequired)
                        this.main.GetTopLeftLabel().Invoke((MethodInvoker)(() => this.main.GetTopLeftLabel().Text = "Trivia"));
                }
                catch (Exception)
                {

                }
            }

            this.socket = sck;
        }
        public void close()
        {
            if(socket != null && socket.Connected)
                this.socket.Close();
            this.main.GetTopLeftLabel().Text = "Connecting to server...";
            this.main.GetTopLeftLabel().Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            ConnectSocket();
        }
        private string SocketSendReceive(string msg, int code)
        {
            if (socket == null || !socket.Connected)
                return "waiting for server to come up...";
            int len = msg.Length;
            byte[] codeBuffer = new byte[1];
            codeBuffer[0] = (byte)code;
            byte[] lenBuffer = BitConverter.GetBytes(len);
            byte[] jsonBuffer = Encoding.Default.GetBytes(msg);

            var myList = new List<byte>();
            myList.AddRange(codeBuffer);
            myList.AddRange(lenBuffer);
            myList.AddRange(jsonBuffer);

            byte[] msgBuffer = myList.ToArray();
            byte[] buffer = new byte[4096];
            int rec = 0;
            try
            {
                socket.Send(msgBuffer, 0, msgBuffer.Length, 0);
                rec = socket.Receive(buffer, 0, buffer.Length, 0);
            }
            catch (Exception)
            {
                MainForm.soc.close();
                return "waiting for server to come up...";
            }

            Array.Resize(ref buffer, rec);

            return Encoding.Default.GetString(buffer);
        }
        
        public string SocketSendReceive(LeaveGameRequest leaveGameRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(leaveGameRequest), Codes.leaveGame_request);
        }
        public string SocketSendReceive(GetGameResultsRequest getGameResultsRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getGameResultsRequest), Codes.getGameResult_request);
        }
        public string SocketSendReceive(SubmitAnswerRequest submitAnswerRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(submitAnswerRequest), Codes.submitAnswer_request);
        }
        public string SocketSendReceive(GetQuestionRequest getQuestionRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getQuestionRequest), Codes.getQuestion_request);
        }
        public string SocketSendReceive(GetOnlinePlyersRequest getOnlinePlyersRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getOnlinePlyersRequest), Codes.getOlinePlyers_request);
        }
        public string SocketSendReceive(LogoutRequest logoutRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(logoutRequest), Codes.logout_request);
        }
        public string SocketSendReceive(StartGameRequest startRoomRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(startRoomRequest), Codes.startGame_request);
        }
        public string SocketSendReceive(CloseRoomRequest closeRoomRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(closeRoomRequest), Codes.closeRoom_request);
        }
        public string SocketSendReceive(LeaveRoomRrequest leaveRoomRrequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(leaveRoomRrequest), Codes.leaveRoom_request);
        }
        public string SocketSendReceive(GetRoomStateRequest getRoomStateRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getRoomStateRequest), Codes.getRoomState_request);
        }
        public string SocketSendReceive(SignupRequest signupRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(signupRequest), Codes.signup_request);
        }
        public string SocketSendReceive(LoginRequest loginRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(loginRequest), Codes.login_request);
        }
        public string SocketSendReceive(CreateRoomRequest createRoomRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(createRoomRequest), Codes.createRoom_request);
        }
        public string SocketSendReceive(GetHighestScoreStatisticsRequest getHighestScoreStatisticsRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getHighestScoreStatisticsRequest), Codes.getHighScore_request);
        }
        public string SocketSendReceive(GetRoomsRequest getRoomsRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getRoomsRequest), Codes.getRooms_request);
        }
        public string SocketSendReceive(BackFromRoom backFromRoom)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(backFromRoom), Codes.signout_request);
        }
        public string SocketSendReceive(JoinRoomRequest joinRoomRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(joinRoomRequest), Codes.joinRoom_request);
        }
        public string SocketSendReceive(GetPlayersInRoomsRequest getPlayersInRoomsRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getPlayersInRoomsRequest), Codes.getPlayersInRoom_request);
        }
        public string SocketSendReceive(GetStatisticsRequest getStatisticsRequest)
        {
            return SocketSendReceive(JsonConvert.SerializeObject(getStatisticsRequest), Codes.getPersonalStats_request);
        }
    }
}
